## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| research/optcont/papers/conv-a-paper/main.pdf |
| work/alg/curr/MTH0111-syllabus.tex |
| misc/not_mine/Math765/assignment3.tex |
| work/alg/curr/1701-lec/review-ch1-0111-also.pdf |
| misc/not_mine/Math765/assignment1.tex |
| work/projects/conf-gen/abs_accept_letter.tex |
| misc/not_mine/Math765/assignment2.tex |
| work/projects/ugr/fa13/laite/antideriv-step.tex |
| research/optcont/papers/isp1fd/main.tex |
| ugrad/fp/tex/report.tex |
| work/projects/conf-gen/mealtickets/mealtickets-xelatex.tex |
| work/projects/conf-gen/accept_letter.tex |
| work/projects/conf-gen/mealtickets/mealtickets-xelatex-nonumber.tex |
| work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-6.tex |
| research/chaos/feigenbaum/jl/doc/logmap-conv.tex |
| research/optcont/papers/nd-paper/main.tex |



### New Tests
File/Test | Result
----------|----
work/Calc3/test2-sp19.tex | true
work/DiffEq/quiz/quiz5-sp19.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
misc/progcode/Onboarding.md.tex | false



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



