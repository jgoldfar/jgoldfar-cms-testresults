## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/syl/syllabus.pdf | true | *false*
work/Calc1/syl/calendar.pdf | true | *false*
research/seminar/minilec/picards-thm_seminar.tex | true | *false*

### New Tests
File/Test | New
----------|----
work/PutnamPrepOther/review-2018B5.inc.tex | false
research/paramid/LV-Diffusion/main.tex | true
work/PutnamPrepOther/review-2018B5.tex | true
work/PutnamPrepOther/review-2018B3.inc.tex | false
work/PutnamPrepOther/review-2018A4.inc.tex | false
work/PutnamPrepOther/review-2018B2.tex | true
work/PutnamPrepOther/review-2018A4.tex | true
work/PutnamPrepOther/review-2018A6.inc.tex | false
work/PutnamPrepOther/review-2018A5.inc.tex | false
work/PutnamPrepOther/review-2018B2.inc.tex | false
work/PutnamPrepOther/tpl.inc.tex | false
work/PutnamPrepOther/review-2018B6.tex | true
work/PutnamPrepOther/review-2018A2.tex | true
work/PutnamPrepOther/review-2018A3.tex | true
work/PutnamPrepOther/review-2018B1.inc.tex | false
work/PutnamPrepOther/tpl.tex | true
work/PutnamPrepOther/review-2018B3.tex | true
work/PutnamPrepOther/review-2018A5.tex | true
work/PutnamPrepOther/review-2018A1.tex | true
work/projects/blog-drafts/neural-network-universal-approximation.tex | true
work/PutnamPrepOther/review-2018B6.inc.tex | false
work/PutnamPrepOther/review-2018A1.inc.tex | false
work/PutnamPrepOther/review-2018A2.inc.tex | false
work/PutnamPrepOther/review-2018A3.inc.tex | false
work/PutnamPrepOther/review-2018A6.tex | true
work/PutnamPrepOther/review-2018B4.tex | true
work/PutnamPrepOther/review-2018B4.inc.tex | false
work/PutnamPrepOther/review-2018B1.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/projects/blog-drafts/continuous-paper-integration.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/nd-paper | true | *false*
research/fda-pde/ml-ufldl | false | *true*
research/seminar/day-to-day-FOSS | true | *false*
work/Calc1/syl | true | *false*

### New Tests
File/Test | New
----------|----
work/PutnamPrepOther | true
research/paramid/LV-Diffusion | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/paramid/LV-Diffusion/README | true
work/PutnamPrepOther/LICENSE | false
research/paramid/LV-Diffusion/LICENSE | true
research/numpde/FastMarchingMethod/LICENSE | true
research/numpde/FastMarchingMethod/README | true
work/PutnamPrepOther/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




