## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/projects/ugr/fa13/laite/antideriv-step.tex | true | *false*
work/MAC/recruiting.tex | false | *true*

### New Tests
File/Test | New
----------|----
research/optcont/papers/nd-paper/disc-paper-parts/tpl.tex | false
research/optcont/papers/nd-sphere-paper/common/preamble.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/discretization-interp.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/tpl.tex | false
research/optcont/papers/nd-sphere-paper/common/energy-est-int1.tex | false
research/talks/jmm_19/preamble-dir.tex | false
research/talks/jmm_19/intro.tex | false
research/talks/jmm_19/diff.tex | false
research/optcont/papers/nd-sphere-paper/common/cont-sobolev-energy-estimate-2.tex | false
research/optcont/papers/nd-sphere-paper/common/assumptions.tex | false
research/optcont/papers/nd-sphere-paper/common/energy-est-u.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/defn-macros.tex | false
research/talks/jmm_19/main.tex | true
research/talks/jmm_19/preamble-cmds.tex | false
research/optcont/papers/nd-sphere-paper/common/defn-macros.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/defn-macros.tex | false
research/talks/jmm_19/abstract-indep.tex | true
research/optcont/papers/MISP-general/NonlinearParabolicPaper.tex | true
research/talks/jmm_19/preamble.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/discretization-interp.tex | false
research/optcont/papers/nd-sphere-paper/common/functional-well-defined.tex | false
research/optcont/papers/nd-sphere-paper/common/parts-tpl.tex | false
research/talks/jmm_19/abstract.tex | false
research/optcont/papers/nd-sphere-paper/common/cont-existence-galerkin.tex | false
research/talks/jmm_19/preamble-cmds-other.tex | false
work/projects/blog-drafts/generalized-jensens.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-classical.tex | false
research/talks/jmm_19/figures/tex/with-domain.tex | false
research/talks/jmm_19/figures/tex/isp-with-domain.tex | false
research/optcont/papers/nd-sphere-paper/main.inc.tex | false
research/optcont/papers/nd-sphere-paper/common/cmds.tex | false
research/optcont/papers/nd-sphere-paper/main.tex | true
research/talks/jmm_19/diff-statement.tex | false
research/optcont/papers/nd-sphere-paper/common/transformation-of-domain.tex | false
research/talks/jmm_19/figures/tex/before-domain.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-main.tex | false
research/talks/jmm_19/apps-diff.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-formulation.tex | false
research/talks/jmm_19/pgf-include.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/discretization-defns.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/tpl.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-general-v1.tex | false
work/projects/blog-drafts/compactness-of-the-trace-map.tex | true
research/optcont/papers/nd-sphere-paper/paper-parts/discretization-defns.tex | false
research/optcont/papers/nd-sphere-paper/common/cont-sobolev-energy-estimate.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/defn-macros.tex | false
research/optcont/papers/nd-sphere-paper/common/formulation-exterior.tex | false
research/talks/jmm_19/apps.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/discretization-state-vec.tex | false
research/optcont/papers/nd-sphere-paper/common/nd-spherical-coords.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/discretization-defns.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-multiphase.tex | false
research/optcont/papers/nd-sphere-paper/future.tex | true
research/optcont/papers/nd-sphere-paper/common/v2-estimate-noncylindrical.tex | false
research/optcont/papers/nd-sphere-paper/common/weak-soln-derivation.tex | false
research/talks/jmm_19/disc.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/discretization-interp.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/nd-paper | true | *false*

### New Tests
File/Test | New
----------|----
research/optcont/papers/MISP-general | true
research/optcont/papers/nd-sphere-paper | true
research/talks/jmm_19 | false
work/projects/ugr/sp13 | true
work/projects/ugr/sp12 | true
work/projects/ugr/fa18 | true
work/projects/ugr/docs | true
work/projects/ugr/16-17 | true
research/optcont/papers/nd-sphere-paper/common | true
research/optcont/papers/nd-paper/semidisc-paper-parts | true
research/optcont/papers/nd-paper/disc-paper-parts | true
research/optcont/papers/nd-sphere-paper/paper-parts | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/papers/nd-paper/paper-parts | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




