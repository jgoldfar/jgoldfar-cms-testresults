## PDF Files Compile

File/Test | Old | New
----------|-----|----
misc/resume/courses.pdf | true | *false*

### New Tests
File/Test | New
----------|----
research/talks/ISP_Summary/diff-parts/gradient-mainres-s-1.tex | false
grad/dissertation/talk/conv-parts/energy-est-2-p-1-s.tex | false
grad/dissertation/talk/conv-parts/energy-est-1-cons-1-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-2-cons.tex | false
research/talks/ISP_Summary/conv-parts/Pn-Qn-maps-props-2-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-2-p-1-s-eq.tex | false
research/talks/ISP_Summary/diff-parts/preamble-defn-macros.tex | false
research/talks/ISP_Summary/diff-parts/heur-coeff-def-func.tex | false
grad/dissertation/talk/diff-parts/gradient-mainres-adjointprob.tex | false
grad/dissertation/talk/conv-parts/main-res-2-conv-3-s.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/c1-interpolation.tex | false
grad/dissertation/talk/diff-parts/gradient-mainres-assumptions.tex | false
grad/dissertation/talk/conv-parts/interpolation-equivalence-2-s.tex | false
research/talks/ISP_Summary/conv-parts/main-res-2-conv-2-s.tex | false
grad/dissertation/talk/conv-parts/Pn-Qn-maps-props-2-s.tex | false
grad/dissertation/talk/diff-parts/sot-only.tex | false
grad/dissertation/talk/diff-parts/gradient-defn-main.tex | false
grad/dissertation/talk/conv-parts/defn-macros.tex | false
research/talks/ISP_Summary/diff-parts/more-weak-solution-defn.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/smooth-truncation.tex | false
research/talks/ISP_Summary/conv-parts/interpolation-equivalence-2-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-2-cons-s.tex | false
grad/dissertation/talk/diff-parts/more-weak-solution-defn-change.tex | false
grad/dissertation/talk/conv-parts/energy-est-2-cons.tex | false
grad/dissertation/talk/diff-parts/more-weak-solution-defn.tex | false
research/talks/ISP_Summary/conv-parts/Pn-Qn-maps-props-1-s.tex | false
research/talks/ISP_Summary/diff-parts/more-weak-solution-defn-eqn.tex | false
research/talks/ISP_Summary/diff-parts/notation-l2omega.tex | false
grad/dissertation/talk/conv-parts/energy-est-2-cons-s.tex | false
research/talks/ISP_Summary/diff-parts/sot-only.tex | false
research/talks/ISP_Summary/diff-parts/gradient-mainres-assumptions.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/winfty-bdyconstraint-general.tex | false
grad/dissertation/talk/conv-parts/energy-est-2-p-1-s-eq.tex | false
research/talks/ISP_Summary/conv-parts/main-res-1-s.tex | false
grad/dissertation/talk/diff-parts/adjoint-exists-s.tex | false
research/talks/ISP_Summary/conv-parts/defn-macros.tex | false
grad/dissertation/talk/conv-parts/energy-est-2-cons-2-s.tex | false
research/talks/ISP_Summary/diff-parts/adjoint-exists-s.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/b2ell-v1.tex | false
grad/dissertation/talk/diff-parts/more-weak-solution-defn-eqn.tex | false
grad/dissertation/talk/conv-parts/Pn-Qn-maps-props-1-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-2-s.tex | false
research/talks/ISP_Summary/diff-parts/gradient-mainres-adjointprob.tex | false
grad/dissertation/talk/diff-parts/notation-l2omega.tex | false
work/Calc1/cmds.tex | false
research/talks/ISP_Summary/conv-parts/main-res-2-conv-3-s.tex | false
grad/dissertation/talk/conv-parts/main-res-2-conv-2-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-2-p-1-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-1-cons-1-s.tex | false
grad/dissertation/talk/diff-parts/gradient-mainres-s-1.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-2-cons-2-s.tex | false
research/talks/ISP_Summary/conv-parts/main-res-2-conv-1-s.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/winfty-estimate.tex | false
grad/dissertation/talk/conv-parts/energy-est-1-cons-2-s.tex | false
research/talks/ISP_Summary/diff-parts/more-weak-solution-defn-change.tex | false
grad/dissertation/talk/conv-parts/main-res-1-s.tex | false
research/talks/ISP_Summary/diff-parts/j-well-defined-result.tex | false
grad/dissertation/talk/conv-parts/energy-est-2-s.tex | false
grad/dissertation/talk/diff-parts/heur-coeff-def-func.tex | false
grad/dissertation/talk/diff-parts/j-well-defined-result.tex | false
grad/dissertation/talk/conv-parts/main-res-2-conv-1-s.tex | false
research/talks/ISP_Summary/conv-parts/energy-est-1-cons-2-s.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/laplace-interpolation.tex | false
research/optcont/papers/conv-paper/a-ptwise-interp-parts/kriging.tex | false
research/talks/ISP_Summary/diff-parts/gradient-defn-main.tex | false
grad/dissertation/talk/diff-parts/preamble-defn-macros.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
misc/resume | true | *false*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




