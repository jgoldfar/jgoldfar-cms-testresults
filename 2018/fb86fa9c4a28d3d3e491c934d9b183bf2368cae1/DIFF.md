## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/resume/cv.pdf | true | *false* |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/MCode-Constrained/docs.tex | true | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/nd-paper | false | *true* |
| work/Calc1/syl | true | *false* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




