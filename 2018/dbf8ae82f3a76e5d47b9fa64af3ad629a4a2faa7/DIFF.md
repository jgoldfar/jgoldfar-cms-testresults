## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/optcont/MISPCode/MISPTools/example/TikhonovSamarskiiResults.tex | false | *true*

### New Tests
File/Test | New
----------|----
research/optcont/MISPCode/MISPTools/example/StateVectorErrorContourPlot.tex | false
research/optcont/MISPCode/MISPTools/example/NxNtRatioL2NormPlot.tex | false
research/optcont/ODE/ode-opt-extended.pdf | true
research/optcont/ODE/ode-opt-full.pdf | true
research/optcont/MISPCode/MISPTools/example/ApproxErrorEpsilonDependencePlot.tex | false
research/optcont/MISPCode/MISPTools/example/StateVectorContourPlot.tex | false
research/optcont/ODE/paper-linfinity.pdf | true
research/optcont/MISPCode/MISPTools/example/NtNxFunctionalConvergencePlot.tex | false
research/optcont/ODE/ode-opt.pdf | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/ODE/ode-opt-extended.tex | false
research/optcont/ODE/ode-opt.tex | false
research/optcont/ODE/ode-opt-full.tex | false
research/optcont/ODE/paper-linfinity.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




