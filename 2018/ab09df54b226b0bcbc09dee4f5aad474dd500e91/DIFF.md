## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/resume/cv.pdf | true | *false* |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/searcde16/diff-parts/more-weak-solution-defn.tex | false | 
| research/talks/searcde16/diff-parts/preamble-defn-macros.tex | false | 
| research/talks/searcde16/diff-parts/more-weak-solution-defn-change.tex | false | 
| research/talks/searcde16/diff-parts/notation-l2omega.tex | false | 
| research/talks/searcde16/diff-parts/more-weak-solution-defn-eqn.tex | false | 
| research/talks/searcde16/paper-parts/preamble-cmds.tex | false | 
| research/talks/searcde16/diff-parts/adjoint-exists-s.tex | false | 
| research/talks/searcde16/paper-parts/preamble-cmds-other.tex | false | 
| research/talks/searcde16/paper-parts/preamble-defn-macros.tex | false | 
| research/talks/searcde16/diff-parts/j-well-defined-result.tex | false | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/conv-paper/disc-paper-parts | false | *true* |
| research/optcont/papers/conv-paper | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| misc/resume | true | 
| research/optcont/papers/diff-paper-comp | true | 
| ugrad/fp_prop_09 | true | 
| research/talks/searcde16 | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| work/projects/MakeUnit/README | true | 
| work/projects/MakeUnit/LICENSE | true | 



## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




