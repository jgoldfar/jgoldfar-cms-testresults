## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/Calc1/labs/lab19-intro.inc.tex | false
work/Calc1/labs/lab02.tex | true
work/Calc1/labs/lab19-qs.inc.tex | false
work/Calc1/labs/lab19.tex | true
work/Calc1/labs/lab01.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Calc1/labs/lab1.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




