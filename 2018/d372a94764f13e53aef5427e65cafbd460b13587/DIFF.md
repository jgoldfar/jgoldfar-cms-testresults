## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/notes/main.tex | false | *true*
work/Calc1/notes/2.5/main.tex | false | *true*

### New Tests
File/Test | New
----------|----
work/Calc1/assess/quiz2a.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/projects/ugr-old/docs/ode/extremal_problems-inc.tex | false
research/optcont/JCode/ISP/test/exportOutput/testhead.tex | false
work/misc-old/PhysicsHeatEquationSoln.tex | true
work/Models-old/ex3.tex | true
work/misc-old/refletter/reference-letter-tpl.tex | true
research/optcont/MISPCode/examplePaper/section_3.tex | false
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input_v3.tex | false
work/misc-old/series-tests.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input.tex | false
research/optcont/JCode/ISP/test/exportOutput/test.tex | false
work/Models-old/lab-2.tex | true
work/misc-old/tex-tutorial.tex | true
work/tutoring-old/3x3-arithmetic.tex | true
work/projects/ugr-old/sp12/nmertins/diff-conv-categorize-input.tex | false
work/tutoring-old/zachary/zachary_100112.tex | true
work/projects/ugr-old/sp13/malik/ode-opt-extended.tex | true
work/projects/ugr-old/fa13/talk-laite.tex | true
work/tutoring-old/susanna-mathmeth1-ec.tex | true
work/projects/ugr-old/fa13/talk-linney.tex | true
research/optcont/MISPCode/examplePaper/section_4.tex | false
work/tutoring-old/zachary/zachary_021212.tex | true
work/misc-old/refletter/mnguyen.tex | true
work/misc-old/refletter/dmccormick.tex | true
work/projects/ugr-old/fa13/talk-schluter.tex | true
research/optcont/JCode/ISP/num/osc2015-03-09/collected.tex | false
work/tutoring-old/zachary/zachary_240414.tex | true
research/optcont/JCode/ISP/num/collected.tex | false
work/misc-old/refletter/ecosgrove.tex | true
work/projects/ugr-old/sp13/bpoggi/paper-linfinity.tex | true
work/tutoring-old/koceski-12.3.5-ode.tex | true
work/projects/ugr-old/fa18/jgluck-init.tex | true
work/ProbStat-old/midterm1-question.tex | true
work/misc-old/fit-resignation.tex | true
work/projects/ugr-old/sp13/malik/j-lip-cond.tex | true
work/tutoring-old/zachary/zachary_071212.tex | true
work/projects/ugr-old/fa12/problem-statement.tex | true
work/Calc2-old/Su10/pfd.tex | true
work/projects/ugr-old/sp12/nmertins/4.13.2012-fit-talk.tex | true
research/optcont/JCode/ISP/num/tex/PDEdata.tex | false
work/tutoring-old/zachary/zachary_200414.tex | true
research/optcont/JCode/ISP/doc/figures/CodeDiagram.tex | false
work/projects/ugr-old/sp12/nmertins/4.16.2012-erau-talk-r2.tex | true
work/projects/ugr-old/sp12/nmertins/4.16.2012-erau-talk.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input_v2-shading.tex | false
work/tutoring-old/zachary/zachary_310314.tex | true
work/Models-old/ex2.tex | true
work/Models-old/ex1.tex | true
research/optcont/MISPCode/examplePaper/section_2.tex | false
work/misc-old/mth4101-fa15-midterm-2.tex | true
research/optcont/JCode/ISP/num/tex/PDEsolnMult.tex | false
research/optcont/MISPCode/examplePaper/section_5.tex | false
research/optcont/JCode/ISP/test/exportOutput/plotPDEsoln.tex | false
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input_v2.tex | false
work/projects/ugr-old/sp13/malik/paper-linfinity.tex | true
work/projects/ugr-old/sp13/bpoggi/extremal_problems.tex | true
research/optcont/MISPCode/examplePaper/paper_ISP_JCAM_2017.tex | false
work/projects/ugr-old/sp13/malik/proj_statement.tex | false
work/projects/ugr-old/16-17/jbarrett-lift.tex | true
work/ProbStat-old/midterm1.tex | true
research/optcont/JCode/ISP/test/exportOutput/testsoln.tex | true
work/tutoring-old/zachary/zachary_220412.tex | true
work/misc-old/refletter/jbarrett.tex | true
research/optcont/JCode/ISP/num/tex/ISPdata.tex | false
work/projects/ugr-old/sp13/bpoggi/dual-spaces.tex | true
research/optcont/MISPCode/examplePaper/section_1.tex | false
work/projects/ugr-old/fa13/talk-poggi.tex | true
work/projects/ugr-old/sp13/malik/ode-opt-pre.tex | false
research/optcont/JCode/ISP/test/exportOutput/optTestConv.tex | false
work/Models-old/ch4.1.tex | true
work/projects/ugr-old/sp13/malik/least-squares.tex | true
work/Models-old/test1.tex | true
work/projects/ugr-old/sp12/nmertins/diff-conv-categorize.tex | true
research/optcont/JCode/ISP/num/tex/ISPmult.tex | false
work/projects/ugr-old/sp13/malik/ode-opt-full.tex | true
work/projects/ugr-old/sp13/malik/ode-opt.tex | true
work/projects/ugr-old/sp13/malik/work.tex | false
work/projects/ugr-old/sp12/nmertins/2012-poster-copy.tex | true
work/tutoring-old/cassnchels.tex | true
research/optcont/JCode/ISP/num/tex/PDEsoln.tex | false
work/projects/ugr-old/fa13/laite/antideriv-step.tex | true
work/projects/ugr-old/sp13/malik/work_expanded.tex | true
work/projects/ugr-old/sp13/malik/proj_statement_extended.tex | true
work/Models-old/test2.tex | true
work/tutoring-old/jason_10-20.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/misc-notes | false | *true*

### New Tests
File/Test | New
----------|----
work/misc/refletter | true
work/siam-chapter/summer_school/2013 | true
work/projects/abdulla/NSF-2015 | true
work/projects/typing | true
work/GrpProj/Calc3/g2.inc | true
work/projects/ugr/docs/ode | true
work/GrpProj/Calc2 | true
work/siam-chapter/ayr13 | true
work/projects/ugr/sp12/nmertins | true
work/Models | true
work/siam-chapter/summer_school/2013/yrnotes | true
work/siam-chapter/ayr13/reading-group | true
work/tutoring | true
work/projects/ugr/sp13/malik | true
work/MAC | true
work/misc | true
work/matlab-workshop/13.8.31-combined | true
work/projects/abdulla | true
work/training/latex/BlankProj | true
work/projects/ugr/fa13/laite | true
work/GrpProj/Calc1 | true
work/projects/typing/fulton-siam-2016 | true
work/ProbStat | true
work/projects/dsct-laserday | true
work/common | true
work/matlab-workshop/feedback | true
work/projects/typing/jmandelkern | true
work/training/julia | true
work/GrpProj/Calc3/g3.inc | true
work/projects/ugr/sp13/bpoggi | true
work/DiffEq/quiz | true
work/training/maxima | true
work/projects/useR | true
work/projects/ugr/fa12 | true
work/NumericalMethodsBook | true
work/matlab-workshop/materials/Advanced/NPDE | true
work/projects/ugr/fa13 | true
work/Calc3/quizzes | true
work/siam-chapter/ayr12 | true
work/DiffEq/proj | true
work/training/latex | true
work/DiffEq/hw-gr | true
work/MAC/GraphicalIdentity | true
work/GrpProj/Calc3 | true
work/training/VCS | true
work/siam-chapter/summer_school | true
work/tutoring/zachary | true
work/GrpProj/Calc3/g4.inc | true
work/siam-chapter/summer_school/notes | true
work/siam-chapter | true
work/GrpProj | true
work/GrpProj/DiffEq | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Calc2-old/Su10 | false



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/optcont/JCode/ISP/README | true
work/Calc2/LICENSE | false
research/optcont/JCode/PGFPlotsProcess/README | true
research/optcont/JCode/SourceDeps/README | true
work/tutoring/README | false
research/optcont/JCode/PGFPlotsProcess/LICENSE | true
research/optcont/JCode/HotCode/LICENSE | true
work/tutoring/LICENSE | false
work/misc/LICENSE | false
work/Models/README | false
research/optcont/JCode/ISP/LICENSE | true
work/Calc2/README | false
research/optcont/JCode/HotCode/README | true
work/ProbStat/LICENSE | false
research/optcont/JCode/ISP/doc/LICENSE | true
research/optcont/JCode/SourceDeps/LICENSE | true
work/misc/README | false
work/ProbStat/README | false
work/Models/LICENSE | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/JCode/PGFPlotsProcess.jl/README | false
research/optcont/JCode/ISP.jl/README | false
research/optcont/JCode/ISP.jl/LICENSE | false
research/optcont/JCode/SourceDeps.jl/README | false
research/optcont/JCode/HotCode.jl/LICENSE | false
research/optcont/JCode/SourceDeps.jl/LICENSE | false
research/optcont/JCode/PGFPlotsProcess.jl/LICENSE | false
research/optcont/JCode/HotCode.jl/README | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/Models/mathematica/lab-2.m | false
work/Models/mathematica/lab-1.m | false
work/Models/matlab/stefan2.m | true
work/Models/project-1.nb | false
work/Models/matlab/stefan_u.m | true
research/optcont/JCode/ISP/num/ezplot3.m | false
work/tutoring/undergrad_research_mamie_sp11/dataimport.m | false
work/Calc2/Sp12/polarplots.nb | true
work/tutoring/kosak-zhora-1.m | false
work/tutoring/ramya.m | false
work/Models/mathematica/project-1.m | false
work/Models/matlab/project1_model.m | true
work/Models/matlab/stefan.m | true
work/Models/matlab/project1_plotmap.m | true
work/Models/lab-2.nb | false
work/Models/lab-1.nb | false
work/tutoring/susanna-1.m | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Models-old/matlab/stefan2.m | true
work/tutoring-old/ramya.m | false
work/Calc2-old/Sp12/polarplots.nb | true
work/tutoring-old/undergrad_research_mamie_sp11/dataimport.m | false
work/tutoring-old/kosak-zhora-1.m | false
work/Models-old/mathematica/project-1.m | false
work/Models-old/matlab/project1_model.m | true
work/tutoring-old/susanna-1.m | true
work/Models-old/project-1.nb | false
work/Models-old/lab-2.nb | false
work/Models-old/matlab/project1_plotmap.m | true
work/Models-old/mathematica/lab-1.m | false
work/Models-old/matlab/stefan.m | true
work/Models-old/matlab/stefan_u.m | true
work/Models-old/lab-1.nb | false
work/Models-old/mathematica/lab-2.m | false



