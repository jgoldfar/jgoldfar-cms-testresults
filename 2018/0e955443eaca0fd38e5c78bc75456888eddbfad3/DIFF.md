## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/Calc1/notes/5.1/main.tex | true
work/Calc1/notes/2.8/main.tex | true
work/Calc1/notes/main.tex | true
work/Calc1/notes/4.2/main.inc.tex | false
work/Calc1/notes/3.3/main.inc.tex | false
work/Calc1/notes/5.2/main.inc.tex | false
work/Calc1/notes/2.6/main.tex | true
work/Calc1/notes/2.5/main.inc.tex | false
work/Calc1/notes/2.3/main.inc.tex | false
work/Calc1/notes/5.3/main.tex | true
work/Calc1/notes/2.7/main.tex | true
work/Calc1/notes/3.1/main.tex | true
work/Calc1/notes/2.6/main.inc.tex | false
work/Calc1/notes/3.2/main.tex | true
work/Calc1/notes/5.4/main.tex | true
work/Calc1/notes/4.5/main.inc.tex | false
work/Calc1/notes/4.1/main.tex | true
work/Calc1/notes/3.9/main.tex | true
work/Calc1/notes/4.2/main.tex | true
work/Calc1/notes/4.8/main.tex | true
work/Calc1/notes/3.3/main.tex | true
work/Calc1/notes/2.2/main.tex | true
work/Calc1/notes/3.5/main.tex | true
work/Calc1/notes/2.8/main.inc.tex | false
work/Calc1/notes/3.11/main.inc.tex | false
work/Calc1/notes/4.7/main.tex | true
work/Calc1/notes/4.4/main.inc.tex | false
work/Calc1/notes/3.4/main.tex | true
work/Calc1/notes/2.3/main.tex | true
work/Calc1/notes/4.8/main.inc.tex | false
work/Calc1/notes/3.1/main.inc.tex | false
work/Calc1/notes/3.9/main.inc.tex | false
work/Calc1/notes/3.6/main.tex | true
work/Calc1/notes/2.2/main.inc.tex | false
work/Calc1/notes/5.2/main.tex | true
work/Calc1/notes/5.3/main.inc.tex | false
work/Calc1/notes/main.inc.tex | false
work/Calc1/notes/4.7/main.inc.tex | false
work/Calc1/notes/2.5/main.tex | true
work/Calc1/notes/3.6/main.inc.tex | false
work/Calc1/notes/4.4/main.tex | true
work/Calc1/notes/4.5/main.tex | true
work/Calc1/notes/5.1/main.inc.tex | false
work/Calc1/notes/5.4/main.inc.tex | false
work/Calc1/notes/4.1/main.inc.tex | false
work/Calc1/notes/4.3/main.inc.tex | false
work/Calc1/notes/3.2/main.inc.tex | false
work/Calc1/notes/3.11/main.tex | true
work/Calc1/notes/4.9/main.inc.tex | false
work/Calc1/notes/3.7/main.tex | true
work/Calc1/notes/3.5/main.inc.tex | false
work/Calc1/notes/2.7/main.inc.tex | false
work/Calc1/notes/3.4/main.inc.tex | false
work/Calc1/notes/4.9/main.tex | true
work/Calc1/notes/3.7/main.inc.tex | false
work/Calc1/notes/4.3/main.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/MISPCode | true | *false*

### New Tests
File/Test | New
----------|----
work/Calc1/notes | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




