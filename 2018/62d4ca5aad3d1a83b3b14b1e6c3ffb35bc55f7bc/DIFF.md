## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/ProbStat/midterm1-question.tex | false | *true*
work/alg/curr/1701-lec/week6.tex | false | *true*
work/alg/curr/1701-lec/all.tex | false | *true*
work/alg/curr/1701-lec/week8.tex | false | *true*
work/Calc1/Fa09/Lab15_selectedsolns.tex | false | *true*
work/Calc1/Sp10/realpowers.tex | false | *true*
work/Calc1/labs_extended/lab02.tex | false | *true*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/siam-chapter/ayr12/minutes-10.19.12.tex | true
work/siam-chapter/summer_school/2013/calendar-7day.tex | true
work/projects/reu-admin/notes/npde/report/fdiff-slides.tex | true
work/projects/reu-admin/notes/npde/report/old-pres.tex | true
work/projects/reu-admin/notes/dsct/DSCT-Lecture4-print.tex | true
work/projects/reu-admin/notes/isp/abdulla-5.16.2014-ISP-4.tex | true
work/projects/reu-admin/notes/tex/template-full.tex | true
work/projects/typing/ua-pot/doc3.tex | true
work/training/latex/main.tex | false
work/projects/ugr/sp13/malik/work_expanded.tex | true
work/tutoring/cassnchels.tex | true
work/projects/reu-admin/notes/npde/report/6.13.2014-v2.tex | true
work/projects/ugr/sp13/bpoggi/dual-spaces.tex | true
work/projects/typing/ua-pot/doc2.tex | true
work/projects/ugr/sp13/malik/ode-opt-full.tex | false
work/projects/reu-admin/notes/isp/abdulla-5.12.2014-ISP-2-online.tex | true
work/projects/ugr/sp12/nmertins/diff-conv-categorize.tex | true
work/siam-chapter/ayr13/reading-group.tex | true
work/siam-chapter/summer_school/2013/calendar-5day.tex | true
work/siam-chapter/ayr13/flyer-genmeeting.tex | true
work/projects/ugr/fa13/laite/antideriv-step.tex | true
work/projects/typing/fulton_FL_2015.tex | true
work/siam-chapter/ayr12/flyer-talk-13.03.22-v1.tex | true
work/tutoring/zachary/zachary_220412.tex | true
work/projects/typing/nadiyah/nadiyah_equations.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-1-ua.tex | true
work/projects/reu-admin/notes/dsct/DSCT-Lecture4.tex | true
work/projects/reu-admin/notes/dsct/DSCT-Lecture1.tex | true
work/projects/reu-admin/notes/npde/report/6.13.2014.tex | true
work/siam-chapter/ayr12/minutes-9.6.12.tex | true
work/siam-chapter/ayr13/flyer-talk-11.01.13.tex | true
work/tutoring/zachary/zachary_071212.tex | true
work/projects/typing/jmandelkern/bessel_p.tex | true
work/training/julia/main.pdf | true
work/projects/ugr/sp13/bpoggi/paper-linfinity.tex | false
work/projects/placement/tips-and-tricks.tex | true
work/projects/ugr/fa13/talk-schluter.tex | true
work/projects/reu-admin/notes/isp/abdulla-5.12.2014-ISP-2.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-1-loc.tex | true
work/projects/reu-admin/notes/isp/report/g1-6.13.2014.tex | true
work/projects/typing/fulton_dec16.tex | true
work/projects/reu-admin/notes/isp/report/g1-current.tex | true
work/tutoring/3x3-arithmetic.tex | true
work/siam-chapter/ayr12/flyer-talk-13.03.22.tex | true
work/projects/typing/nadiyah/nadiyah_final.tex | false
work/projects/useR/useR.tex | true
work/siam-chapter/summer_school/2013/yrnotes/intro-indep.tex | true
work/siam-chapter/summer_school/2013/flyer-tex.tex | true
work/siam-chapter/ayr13/reading-group/sir-10.24.13.tex | true
work/projects/ugr/sp13/malik/paper-linfinity.tex | true
work/projects/typing/nadiyah/nadiyah_thesis_prop.tex | true
work/tutoring/zachary/zachary_021212.tex | true
work/training/latex/template.tex | true
work/projects/ugr/fa13/talk-poggi.tex | true
work/projects/typing/fulton-siam-2016/main.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-6.tex | true
work/siam-chapter/ayr13/reading-group/reading-group-future.tex | true
work/siam-chapter/minutes-template.tex | true
work/projects/typing/fulton-french-lines.tex | true
work/projects/ugr/16-17/jbarrett-lift.tex | true
work/projects/reu-admin/notes/tex/template-minimal.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-5.tex | true
work/siam-chapter/summer_school/2013/certificates-1.tex | true
work/projects/reu-admin/notes/dsct/DSCT-Lecture3.tex | true
work/projects/ugr/fa12/problem-statement.tex | true
work/tutoring/zachary/zachary_200414.tex | true
work/projects/reu-admin/notes/isp/report/first-energy-est-conv.tex | true
work/projects/ugr/sp12/nmertins/4.16.2012-erau-talk-r2.tex | true
work/projects/typing/nadiyah/nadiyah_thesis.tex | true
work/siam-chapter/ayr12/flyer-talk-13.3.29.tex | true
work/projects/typing/fit-thesis.tex | true
work/projects/ugr/sp12/nmertins/4.13.2012-fit-talk.tex | false
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-2.tex | true
work/projects/reu-admin/notes/isp/abdulla-4.25.2014-ISP-1.tex | true
work/projects/ugr/fa13/talk-laite.tex | true
work/training/latex/template-simple.tex | true
work/projects/reu-admin/notes/npde/report/current-brief.tex | true
work/projects/ugr/sp13/malik/proj_statement_extended.tex | true
work/siam-chapter/officer-duties.tex | true
work/projects/ugr/sp12/nmertins/2012-poster-copy.tex | true
work/projects/ugr/sp12/nmertins/interfaces_schematic.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-3.tex | true
work/siam-chapter/ayr12/flyer-talk-13.03.22-v2.tex | true
work/projects/ugr/sp13/malik/j-lip-cond.tex | true
work/tutoring/zachary/zachary_240414.tex | true
work/projects/reu-admin/notes/npde/report/fdiff.tex | true
work/training/maxima/main.tex | true
work/projects/ugr/sp13/malik/ode-opt.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-4.tex | true
work/projects/typing/fulton-aims2012-paper.tex | true
work/projects/mac-site/job-announce.tex | true
work/siam-chapter/summer_school/notes/intro-indep.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-3-ua.tex | true
work/siam-chapter/summer_school/2013/yrnotes/final-projects.tex | true
work/training/VCS/main.tex | true
work/projects/reu-admin/notes/isp/lae-b0c0.tex | true
work/siam-chapter/summer_school/2013/yrnotes/outro-indep.tex | true
work/projects/reu-admin/notes/dsct/DSCT-Lecture4-print-loc.tex | true
work/projects/reu-admin/notes/dsct/block_figures_1.tex | true
work/siam-chapter/summer_school/2013/yrnotes/main.tex | false
work/projects/typing/FL_history.tex | true
work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-1.tex | true
work/siam-chapter/summer_school/notes/all.tex | false
work/siam-chapter/ayr12/flyer-talk-12.4.23.tex | true
work/projects/reu-admin/notes/npde/jgoldfar-4.14.2014-emdiben-1.tex | true
work/tutoring/jason_10-20.tex | true
work/projects/reu-admin/notes/isp/abdulla-5.14.2014-ISP-3.tex | true
work/tutoring/zachary/zachary_100112.tex | true
work/projects/reu-admin/notes/isp/report/first-energy-est.tex | true
work/siam-chapter/ayr12/flyer-talk-13.2.22.tex | true
work/tutoring/koceski-12.3.5-ode.tex | true
work/siam-chapter/trobbin.tex | true
work/training/latex/BlankProj/main.tex | true
work/projects/reu-admin/notes/dsct/DSCT-Lecture2.tex | true
work/projects/mac-site/GSA-announce.tex | true
work/projects/typing/duane/beamer_tpl.tex | true
work/tutoring/zachary/zachary_310314.tex | true
work/siam-chapter/ayr13/reading-group-flyer.tex | true
work/projects/typing/ua-pot/doc4.tex | true
work/siam-chapter/ayr12/flyer-talk-13.3.29-fitlogo.tex | true
work/siam-chapter/summer_school/full-calendar-tpl-5day.tex | true
work/projects/reu-admin/notes/npde/report/current-slowdiff.tex | true
work/projects/placement/webwork/ntq.tex | true
work/training/latex/template-refs.tex | false
work/projects/ugr/fa13/talk-linney.tex | true
work/siam-chapter/summer_school/flyer-template.tex | true
work/projects/ugr/sp13/malik/ode-opt-extended.tex | true
work/siam-chapter/ayr13/reading-group/sir-10.10.13.tex | true
work/projects/reu-admin/notes/isp/report/g2-current.tex | true
work/siam-chapter/ayr12/flyer-talk-13.2.1.tex | true
work/projects/reu-admin/notes/isp/report/g2-6.13.2014.tex | true
work/siam-chapter/flyer-template-fitlogo.tex | true
work/projects/ugr/sp13/malik/least-squares.tex | true
work/projects/reu-admin/notes/tex/example-paper-first.tex | true
work/projects/reu-admin/notes/tex/template-min.tex | true
work/siam-chapter/ayr13/genmeeting.tex | true
work/projects/typing/ua-pot/doc1.tex | true
work/projects/reu-admin/notes/isp/report/current-slides.tex | true
work/tutoring/susanna-mathmeth1-ec.tex | true
work/projects/reu-admin/notes/isp/report/main-presentation.tex | true
work/siam-chapter/flyer-template.tex | true
work/siam-chapter/summer_school/full-calendar-tpl-7day.tex | true
work/projects/ugr/sp13/bpoggi/extremal_problems.tex | true
work/projects/ugr/sp12/nmertins/4.16.2012-erau-talk.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/papers/diff-paper-comp | true
grad/mth6230 | true
work/projects/typing | true
work/Calc2/Su10 | true
work/alg/curr/1701-lec | true
grad/appstat | true
research/talks/searcde16 | true
grad/numerical | true
research/optcont/code-docs/alt-scheme | true
research/chaos/feigenbaum/jl/doc | true
research/chaos/parameter-values/abdulla2013 | true
ugrad/bvp | true
ugrad/dynmet2 | true
research/seminar | true
grad/introa/hw | true
research/talks/ams_sec_sep17 | true
work/MAC | true
research/optcont/papers/diff-paper | true
ugrad/poa | true
work/misc | true
research/optcont/papers/isp1fd | true
research/optcont/papers/isp-constrained | true
work/training/latex/BlankProj | true
work/alg/review/poly | true
research/optcont/papers/nd-paper | false
work/alg/review/cart | true
work/projects/ugr/fa13/laite | true
research/optcont/papers/conv-a-paper/common | true
grad/dissertation/other/comp/comp-q-inc | true
grad/algebra | true
ugrad/fluids_lab | true
ugrad/physoc | true
work/training/maxima | true
grad/apdes/su11 | true
work/projects/useR | true
work/acalc/1603 | true
work/alg/review/1701-sec | true
work/projects/ugr/fa12 | true
grad/algebra/extra | true
grad/stochastic | true
ugrad/meteorology | true
work/acalc/calc-sec | true
grad/seminar/optcont | true
work/matlab-workshop/materials/Advanced/NPDE | true
grad/mathstat/hw | true
work/projects/ugr/fa13 | true
work/Calc3/quizzes | true
work/siam-chapter/ayr12 | true
work/DiffEq/hw-gr | true
grad/odestheory | true
research/misc/fa14-npde-course | true
ugrad/civ_1 | true
work/AlgebraReadingGroup | true
research/talks/siam_an12/figures | true
ugrad/atmenv | true
research/talks/siam_seas14 | true
work/misc/refletter | true
work/acalc/1702 | true
work/siam-chapter/summer_school/2013 | true
work/projects/abdulla/NSF-2015 | true
research/optcont/papers/nd-paper/paper-parts | true
research/optcont/papers/conv-paper/disc-paper-parts | true
work/Models | true
grad/seminar/isp | true
work/acalc/1702/notes | true
research/ndce/reu-paper | true
work/siam-chapter/ayr13/reading-group | true
work/tutoring | true
research/talks/jmm_15a | true
research/talks/nsf_grfp | true
work/GrpProj/Calc3/2 | true
misc/julia/CMSTestTest | true
work/APDE/advanced | true
ugrad/models | true
work/matlab-workshop/13.8.31-combined | true
work/projects/abdulla | true
grad/mth6300 | true
research/seminar/minilec | true
notes/independent/papers/solonnikov64 | true
research/optcont/papers/conv-a-paper | false
work/projects/dsct-laserday | true
work/projects/ugr/sp13/bpoggi | true
research/seminar/parts | true
work/alg/review/trig | true
grad/functional | true
work/Calc1/syl | true
ugrad/fp_prop_09 | true
work/alg/review/rev | true
work/alg/curr/0111-lec | true
research/ndce/ndce_paper/sections | true
research/talks/research_summary | true
ugrad/mesomet | true
work/training/VCS | true
work/tutoring/zachary | true
research/seminar/13.9.20_seminar | true
notes/independent/book/lsu | true
research/talks/jmm_16 | true
research/optcont/papers/MISP-diff | true
research/optcont/papers/conv-paper/semidisc-paper-parts | true
work/Calc1/Sp10 | true
grad/apdes/sp13 | true
research/seminar/bvp_of_math_phys | true
research/talks/siam_pd13 | true
grad/pdes-sp13 | true
work/projects/ugr/docs/ode | true
research/optcont/papers/nd-paper/common | true
work/Calc1/labs_extended | true
notes | true
notes/independent/book/besov | true
notes/redo | true
ugrad/fluids | true
ugrad/models/final | true
grad/dissertation | true
research/talks/jmm_15 | true
work/acalc/1702/perweek | true
research/chaos | true
grad/mathmethods | true
notes/independent/book | true
research/ndce/ndce_paper/figures/tex | true
work/APDE/hw | true
research/chaos/parameter-values | true
grad/linear | true
research/optcont/misc-notes | true
research/optcont/MCode-Constrained | false
grad/mth6300/abdulla_typed/YMC-2012 | true
grad/mathstat | true
ugrad/misc | true
notes/independent | true
work/projects/typing/fulton-siam-2016 | true
notes/finished | true
work/common | true
work/matlab-workshop/feedback | true
grad/introa | true
work/training/julia | true
research/talks/jmm_15b | true
misc/env/tex-include/Templates | true
grad/sobolevspaces/hw-revised | true
research/optcont/code-docs/misc | true
research/ndce/codes/gen-python | true
ugrad/hydro | true
ugrad/dynmet | true
research/misc/proof-secs | true
ugrad/remote | true
work/NumericalMethodsBook | true
grad/numpde | true
work/Calc1/assess | true
notes/complex | true
work/siam-chapter/summer_school/notes | true
work/alg/curr/1012-lec | true
notes/independent/papers/emdiben | true
ugrad/civ_2 | true
research/optcont/papers/conv-paper | false
research/seminar/13.9.20_seminar/sections | true
research/ndce/codes/cnps | true
grad/complex | true
work/alg/review | true
research/seminar/14.09.09_seminar | true
misc/julia/CMSTest/ex/TeXMakefile | true
work/GrpProj/Calc3/3 | true
research/seminar/FA_inf_calc | true
work/Calc1/labs | true
work/siam-chapter/ayr13 | true
work/GrpProj/Calc3/4 | true
work/projects/ugr/sp12/nmertins | true
research/ndce/ndce_paper | true
work/siam-chapter/summer_school/2013/yrnotes | true
ugrad/atmpolllab | true
research/numpde | true
research/seminar/ISP_convergence_talk | true
research/misc | true
work/alg/review/exp-log | true
work/Calc1/Fa09 | true
ugrad/synmet1 | true
research/talks/jmm_17 | true
notes/independent/papers/oleinik_60 | true
work/projects/ugr/sp13/malik | true
work/alg | true
ugrad/envsatsys | true
misc/env/tex-include/Templates/More | true
misc/resume | true
research/optcont/papers/conv-a-paper/paper-parts | true
grad/realvariables | true
notes/independent/book/holton | true
work/alg/planning | true
grad/seminar/dsct | true
misc/julia/CMSTest | true
research/talks/siam_an12 | true
notes/independent/book/goldman | true
work/ProbStat | true
work/APDE/hw/exercises | true
grad/mth6300/abdulla_typed/AIMS-2012 | true
grad/pdes | true
research/ndce | true
work/DiffEq/quiz | true
research/optcont/papers/conv-paper/common | true
research/ndce/codes/emdiben | true
work/alg/curr/1011-lec | true
work/GrpProj/Calc3/1 | true
work/alg/review/function | true
research/seminar/13.9.20_seminar/figures/tex | true
work/alg/review/0111-sec | true
research/optcont/papers/diff-c-paper | true
work/DiffEq/proj | true
work/training/latex | true
misc/julia/SPSBase/doc | true
research/optcont/papers/gen-constrained | true
work/siam-chapter/summer_school | true
ugrad/thermo | true
work/acalc/1603/notes | true
research/optcont/code-docs | true
misc/not_mine | true
notes/independent/papers | true
misc/maths | true
work/siam-chapter | true
research/optcont/ODE | true
research/talks/siam_pd17/talk | false
work/alg/review/linsys | true
work/acalc/extra | true
work/GrpProj/DiffEq | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




