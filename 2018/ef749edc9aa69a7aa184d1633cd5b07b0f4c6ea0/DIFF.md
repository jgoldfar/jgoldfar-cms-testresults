## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/resume/cv.pdf | false | *true* |
| work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-6.tex | true | *false* |
| work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-5.tex | true | *false* |

### New Tests
| File/Test | New |
|-----------------|
| work/misc/refletter/ecosgrove.tex | true | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/MCode-Constrained | true | *false* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




