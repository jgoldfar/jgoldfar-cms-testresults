## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/misc/refletter/mnguyen.tex | true
research/optcont/MISPCode/MISPTools/example/TikhonovSamarskiiResults.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
grad/dissertation/talk/paper-parts/paper-beginning-abdulla-controlset.tex | false
research/talks/ISP_Summary/paper-parts/paper-beginning-abdulla-func.tex | false
research/optcont/papers/MISP-diff/verif/TSE-Gamma.tex | false
research/talks/ISP_Summary/paper-parts/paper-beginning-abdulla-controlset.tex | false
grad/dissertation/talk/paper-parts/paper-beginning-abdulla-func.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
work/misc/refletter | true | *false*
research/optcont/MCode-Constrained | false | *true*

### New Tests
File/Test | New
----------|----
misc/resume/cv-secs | false



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




