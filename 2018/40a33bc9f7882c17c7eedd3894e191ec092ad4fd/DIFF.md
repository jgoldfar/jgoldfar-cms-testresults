## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/projects.tex | true | *false*

### New Tests
File/Test | New
----------|----
work/Calc1/labs_extended/preamble.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




