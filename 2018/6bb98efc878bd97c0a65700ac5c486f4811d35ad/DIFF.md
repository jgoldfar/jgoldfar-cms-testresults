## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/optcont/misc-notes/general-mapping-construction.tex | false | *true*

### No New Tests




## Run Check Commands

File/Test | Old | New
----------|-----|----
work/misc/refletter | false | *true*
misc/resume/cv-secs | false | *true*
research/optcont/misc-notes | false | *true*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




