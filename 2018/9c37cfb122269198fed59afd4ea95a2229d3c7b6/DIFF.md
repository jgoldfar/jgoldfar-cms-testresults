## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/notes/5.1/main.tex | true | *false*
work/Calc1/notes/main.tex | true | *false*

### New Tests
File/Test | New
----------|----
work/Calc1/assess/testf1.tex | true
work/Calc1/assess/testf2.tex | true
misc/julia/SPSBase/doc/logo-standalone.tex | true
work/Calc1/notes/5.5/main.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Calc1/assess/testf.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/fda-pde/ml-ufldl | true | *false*
research/optcont/papers/conv-paper | true | *false*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/projects/blog-drafts/LICENSE | true
work/projects/blog-drafts/README | true
misc/julia/MetaRowEchelon/LICENSE | true
misc/julia/MetaRowEchelon/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
misc/maths/weierstrass-poly.m | false | *true*
grad/numerical/codes/quad_mc_3d.m | false | *true*
grad/numerical/codes/quad_mc_2d.m | false | *true*

### New Tests
File/Test | New
----------|----
research/numpde/numbvp/BVP.jl/test/mlcompare.m | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/numpde/numbvp/BVP.jl/test/mlcompare1.m | false



