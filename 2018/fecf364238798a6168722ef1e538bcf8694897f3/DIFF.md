## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/fda-pde/ml-ufldl/ufldl.pdf | false | *true*

### New Tests
File/Test | New
----------|----
work/MunkresSolutions/TopologyMunkresSolns.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/MunkresSolutions/README | true
work/MunkresSolutions/LICENSE | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/projects/ugr/fa13/laite/lcg_rand.m | false
work/projects/ugr/fa13/laite/jesse.m | false
work/projects/ugr/fa13/laite/jesse_initial_value.m | false



