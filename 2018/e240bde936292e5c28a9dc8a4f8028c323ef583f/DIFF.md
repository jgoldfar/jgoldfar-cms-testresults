## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| grad/apdes/su11/test1.pdf | true | 
| grad/apdes/su11/final-solns.pdf | true | 
| grad/apdes/su11/presentation_notes.pdf | true | 
| grad/apdes/su11/final.pdf | true | 
| grad/apdes/su11/hw1.pdf | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| grad/apdes/su11/final-solns.tex | true |
| research/optcont/MCode-Constrained/+model_fns/prob4.tex | false |
| research/optcont/MCode-Constrained/+model_fns/prob5.tex | false |
| grad/apdes/su11/final.tex | true |
| research/optcont/MCode-Constrained/+model_fns/prob3.tex | false |
| grad/apdes/su11/hw1.tex | true |
| grad/apdes/su11/test1.tex | true |
| research/optcont/MCode-Constrained/+model_fns/prob6.tex | false |
| research/optcont/MCode-Constrained/+model_fns/prob2.tex | false |
| research/optcont/MCode-Constrained/+model_fns/prob1.tex | false |
| grad/apdes/su11/presentation_notes.tex | true |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| grad/apdes/su11/hw1.nb | false | *true* |

### No New Tests




