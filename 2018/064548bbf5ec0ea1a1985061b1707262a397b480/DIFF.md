## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/resume/cv.pdf | false | *true* |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/optcont/MCode-Constrained/test/report1-FuncQuad.tex | false |
| research/optcont/MCode-Constrained/report1.tex | false |
| research/optcont/papers/diff-c-paper/resultStatement.tex | false |
| research/optcont/papers/diff-c-paper/variationalFormulation.tex | false |
| research/optcont/MCode-Constrained/test/report1-heatSolveConverges.tex | false |
| research/optcont/papers/diff-c-paper/notation.tex | false |
| research/optcont/papers/diff-c-paper/prelims.tex | false |
| research/optcont/MCode-Constrained/docs.tex | true |
| research/optcont/papers/diff-c-paper/differentiability-proof.tex | false |
| research/optcont/papers/diff-c-paper/heuristicDerivation.tex | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/nd-paper | true | *false* |
| work/Calc1/syl | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




