## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/fda-pde/ml-ufldl/ufldl.pdf | true | *false*
research/optcont/papers/MISP-general/NonlinearParabolicPaper.pdf | true | *false*
research/optcont/papers/MISP-diff/main.pdf | true | *false*

### No New Tests




## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/committeeService | true
research/optcont/papers/conv-paper/verif | true
work/committeeService/cEarl | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/committeeService/LICENSE | true
work/committeeService/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




