## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/optcont/misc-notes/general-mapping-construction.tex | false | *true*

### New Tests
File/Test | New
----------|----
research/optcont/papers/conv-a-paper/main.pdf | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/talks/searcde16/diff-parts/more-weak-solution-defn-change.tex | false
research/optcont/papers/conv-a-paper/main-OCAM.pdf | false
research/talks/searcde16/diff-parts/j-well-defined-result.tex | false
research/optcont/papers/conv-a-paper/main-paper.pdf | false
research/talks/searcde16/paper-parts/preamble-cmds-other.tex | false
research/talks/searcde16/paper-parts/preamble-defn-macros.tex | false
research/talks/searcde16/diff-parts/preamble-defn-macros.tex | false
research/optcont/papers/conv-a-paper/Pn-Qn-Besov.pdf | false
research/talks/searcde16/diff-parts/adjoint-exists-s.tex | false
research/talks/searcde16/diff-parts/more-weak-solution-defn-eqn.tex | false
research/optcont/papers/conv-a-paper/main-disc.pdf | false
research/talks/searcde16/diff-parts/more-weak-solution-defn.tex | false
research/talks/searcde16/diff-parts/notation-l2omega.tex | false
research/talks/searcde16/paper-parts/preamble-cmds.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




