## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/notes/5.1/main.tex | false | *true*
work/Calc1/notes/main.tex | false | *true*

### No New Tests




## Run Check Commands

File/Test | Old | New
----------|-----|----
research/fda-pde/ml-ufldl | false | *true*
research/optcont/papers/conv-paper | false | *true*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




