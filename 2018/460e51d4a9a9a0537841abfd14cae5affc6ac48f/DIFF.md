## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/projects/dsct-laserday/LaserDayFall2015.tex | true | *false*

### New Tests
File/Test | New
----------|----
grad/dissertation/proposal/res-num/collected-indep.tex | false
grad/dissertation/proposal/parts/heuristic-fullstate-deriv.tex | false
work/SCUDEMPractice/problems.tex | true
work/projects/blog-drafts/scheduling-by-optimization.tex | true
research/ndce/codes/gen-python/doc/images/barenblatt-profile.tex | false
grad/dissertation/proposal/parts/heuristic-state-deriv.tex | false
work/projects/blog-drafts/on-the-structure-of-superstable-logistic-orbits.tex | true
research/ndce/codes/gen-python/doc/diff_schemes.tex | false
grad/dissertation/proposal/parts/weaksoln-deriv.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/ndce/codes/gen-python/diff_schemes.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/seminar/15.09.02 | true
research/talks/ky_aug2011 | true
work/Calc3/exam3 | true
misc/juliet | true
work/APDE/codes | true
research/ndce/codes/gen-python/doc | false
work/grants/EDT2017 | true
work/SCUDEMPractice | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/ndce/codes/gen-python | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
research/seminar/day-to-day-FOSS/README | false | *true*

### New Tests
File/Test | New
----------|----
work/SCUDEMPractice/LICENSE | false
work/SCUDEMPractice/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
grad/dissertation/proposal/res-num/pde/bosc-1.m | false



