## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/optcont/misc-notes/diff-wrt-coeff.tex | false | *true*

### New Tests
File/Test | New
----------|----
work/Calc1/qbank/4.7/main.tex | false
work/Calc1/qbank/2.7/main.tex | false
work/Calc1/qbank/2.2/main.tex | false
work/Calc1/qbank/3.5/main.tex | false
work/Calc1/qbank/3.2/main.tex | false
work/Calc1/qbank/4.4/main.tex | false
work/Calc1/qbank/3.7/main.tex | false
work/Calc1/qbank/5.3/main.tex | false
work/Calc1/qbank/2.5/main.tex | false
work/Calc1/qbank/5.1/main.tex | false
work/Calc1/qbank/3.4/main.tex | false
work/Calc1/qbank/3.1/main.tex | false
work/Calc1/qbank/2.3/main.tex | false
work/Calc1/qbank/3.3/main.tex | false
research/optcont/MCode-Constrained/src/+model_fns/definedProblems.pdf | true
work/Calc1/qbank/3.11/main.tex | false
work/Calc1/qbank/4.8/main.tex | false
work/Calc1/qbank/4.2/main.tex | false
work/Calc1/qbank/4.5/main.tex | false
work/Calc1/qbank/3.6/main.tex | false
work/Calc1/qbank/4.9/main.tex | false
work/Calc1/qbank/4.3/main.tex | false
work/Calc1/qbank/5.2/main.tex | false
work/Calc1/qbank/2.8/main.tex | false
work/Calc1/qbank/2.6/main.tex | false
work/Calc1/qbank/3.9/main.tex | false
work/Calc1/qbank/5.4/main.tex | false
work/Calc1/qbank/4.1/main.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/MCode-Constrained/src/+model_fns/prob4c.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob1.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob3a.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob6.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4a.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob3.tex | false
work/Calc1/qbank/2.3/limitVariety1.tex | false
work/Calc1/qbank/3.6/DiffLogFn1.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4d.tex | false
research/optcont/MCode-Constrained/src/+model_fns/definedProblems.tex | true
work/Calc1/qbank/3.5/InvCosTrigDiff.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4b.tex | false
work/Calc1/qbank/2.2/sketch1.tex | false
work/Calc1/qbank/2.2/graphical1.tex | false
work/Calc1/qbank/3.5/secondDiffImpl1.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob5.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob3c.tex | false
work/Calc1/qbank/2.5/ContDomain1.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob3b.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4.tex | false
work/Calc1/qbank/2.5/IVT1.tex | true
work/Calc1/qbank/3.5/InvTanTrigDiff.tex | false
work/Calc1/qbank/3.7/Example1.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob2.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/MISP-diff | false | *true*

### New Tests
File/Test | New
----------|----
work/Calc2 | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
work/misc/LICENSE | false | *true*
work/ProbStat/README | false | *true*
research/optcont/papers/gen-constrained/LICENSE | false | *true*
research/talks/siam_pd17/LICENSE | false | *true*
work/Calc3/README | false | *true*
work/tutoring/LICENSE | false | *true*
work/Models/README | false | *true*
work/misc/README | false | *true*
work/tutoring/README | false | *true*
work/Calc2/README | false | *true*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/papers/isp-constrained/LICENSE | false
research/optcont/papers/conv-a-paper/LICENSE | false
work/Models/LICENSE | false
work/ProbStat/LICENSE | false
work/Calc3/LICENSE | false
research/optcont/papers/diff-c-paper/LICENSE | false
work/projects/typing/fulton-siam-2016/LICENSE | false
work/Calc2/LICENSE | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




