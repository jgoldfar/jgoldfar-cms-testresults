## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/notes/main.tex | true | *false*
work/Calc1/notes/2.5/main.tex | true | *false*

### New Tests
File/Test | New
----------|----
work/Models-old/lab-2.tex | true
research/optcont/JCode/ISP/num/tex/PDEdata.tex | false
work/projects/ugr-old/16-17/jbarrett-lift.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob3c.tex | false
work/Calc1/qbank/3.5/InvCosTrigDiff.tex | false
work/projects/ugr-old/sp13/malik/ode-opt-pre.tex | false
work/projects/ugr-old/fa13/laite/antideriv-step.tex | true
work/projects/ugr-old/sp13/malik/work_expanded.tex | true
work/tutoring-old/zachary/zachary_200414.tex | true
work/projects/ugr-old/sp13/malik/proj_statement_extended.tex | true
work/Calc1/qbank/2.3/limitVariety1.tex | false
work/misc-old/refletter/mnguyen.tex | true
work/projects/ugr-old/fa13/talk-laite.tex | true
work/Models-old/ex2.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob4d.tex | false
work/Models-old/ex1.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob1.tex | false
work/misc-old/tex-tutorial.tex | true
work/tutoring-old/3x3-arithmetic.tex | true
research/optcont/JCode/ISP/num/collected.tex | false
work/ProbStat-old/midterm1.tex | true
work/projects/ugr-old/fa18/jgluck-init.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob3b.tex | false
research/optcont/JCode/ISP/test/exportOutput/optTestConv.tex | false
research/optcont/JCode/ISP/test/exportOutput/plotPDEsoln.tex | false
research/optcont/misc-notes/student-success.tex | true
work/projects/ugr-old/sp13/malik/ode-opt-full.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob5.tex | false
work/tutoring-old/cassnchels.tex | true
work/projects/ugr-old/docs/ode/extremal_problems-inc.tex | false
work/projects/ugr-old/sp13/malik/ode-opt.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob3a.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob6.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob2.tex | false
work/projects/ugr-old/fa12/problem-statement.tex | true
work/misc-old/refletter/dmccormick.tex | true
research/optcont/JCode/ISP/test/exportOutput/testsoln.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input_v2.tex | false
research/optcont/MCode-Constrained/src/+model_fns/definedProblems.tex | true
work/Calc1/qbank/2.5/ContDomain1.tex | false
work/Models-old/test2.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input_v3.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4b.tex | false
work/Calc2-old/Su10/pfd.tex | true
work/ProbStat-old/midterm1-question.tex | true
work/misc-old/refletter/ecosgrove.tex | true
work/Calc1/qbank/3.5/secondDiffImpl1.tex | false
work/tutoring-old/zachary/zachary_220412.tex | true
work/projects/ugr-old/sp13/malik/work.tex | false
work/projects/ugr-old/sp13/malik/paper-linfinity.tex | true
research/optcont/MISPCode/examplePaper/section_2.tex | false
research/optcont/misc-notes/diff-wrt-coeff.tex | false
work/misc-old/mth4101-fa15-midterm-2.tex | true
work/misc-old/series-tests.tex | true
work/Models-old/ch4.1.tex | true
work/tutoring-old/susanna-mathmeth1-ec.tex | true
work/projects/ugr-old/sp13/malik/least-squares.tex | true
research/optcont/JCode/ISP/num/tex/PDEsolnMult.tex | false
research/optcont/JCode/ISP/doc/figures/CodeDiagram.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4.tex | false
work/Calc1/qbank/3.5/InvTanTrigDiff.tex | false
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input.tex | false
research/optcont/JCode/ISP/num/tex/PDEsoln.tex | false
research/optcont/MCode-Constrained/src/+model_fns/prob4a.tex | false
work/projects/ugr-old/sp12/nmertins/4.16.2012-erau-talk-r2.tex | true
work/projects/ugr-old/fa13/talk-linney.tex | true
work/Models-old/test1.tex | true
research/optcont/JCode/ISP/test/exportOutput/test.tex | false
work/MAC/GraphicalIdentity/Calc2.tex | true
work/Calc1/qbank/3.6/DiffLogFn1.tex | false
research/optcont/MISPCode/examplePaper/section_4.tex | false
work/misc-old/fit-resignation.tex | true
work/Calc1/qbank/3.7/Example1.tex | false
work/Calc1/labs/lab04.tex | true
work/tutoring-old/jason_10-20.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic.tex | true
work/projects/ugr-old/sp13/bpoggi/extremal_problems.tex | true
research/optcont/JCode/ISP/test/exportOutput/testhead.tex | false
work/misc-old/PhysicsHeatEquationSoln.tex | true
work/projects/ugr-old/fa13/talk-schluter.tex | true
research/optcont/MISPCode/examplePaper/paper_ISP_JCAM_2017.tex | false
work/projects/ugr-old/sp13/bpoggi/paper-linfinity.tex | true
work/misc-old/refletter/jbarrett.tex | true
work/projects/ugr-old/sp13/malik/proj_statement.tex | false
work/Models-old/ex3.tex | true
research/optcont/JCode/ISP/num/tex/ISPdata.tex | false
work/projects/ugr-old/sp12/nmertins/4.13.2012-fit-talk.tex | true
work/projects/ugr-old/sp13/bpoggi/dual-spaces.tex | true
work/projects/ugr-old/sp12/nmertins/diff-conv-categorize.tex | true
research/optcont/JCode/ISP/num/tex/ISPmult.tex | false
work/projects/ugr-old/sp12/nmertins/diff-conv-categorize-input.tex | false
work/projects/ugr-old/sp12/nmertins/4.16.2012-erau-talk.tex | true
research/optcont/JCode/ISP/doc/figures/PrintDiag.tex | true
work/projects/ugr-old/sp12/nmertins/interfaces_schematic_input_v2-shading.tex | false
work/projects/ugr/fa18/jgluck-init.tex | true
work/projects/ugr-old/sp12/nmertins/2012-poster-copy.tex | true
work/projects/ugr-old/sp13/malik/j-lip-cond.tex | true
work/Calc1/qbank/2.2/sketch1.tex | false
work/Calc1/qbank/2.5/IVT1.tex | true
work/tutoring-old/zachary/zachary_071212.tex | true
research/optcont/JCode/ISP/num/osc2015-03-09/collected.tex | false
work/misc-old/refletter/reference-letter-tpl.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob3.tex | false
research/optcont/MISPCode/examplePaper/section_5.tex | false
research/optcont/MISPCode/examplePaper/section_1.tex | false
work/tutoring-old/zachary/zachary_100112.tex | true
research/optcont/MCode-Constrained/src/+model_fns/prob4c.tex | false
work/projects/ugr-old/fa13/talk-poggi.tex | true
work/tutoring-old/zachary/zachary_240414.tex | true
work/projects/ugr-old/sp13/malik/ode-opt-extended.tex | true
work/Calc1/glossary.pdf | true
work/MAC/GraphicalIdentity/Calc1.tex | true
research/optcont/MISPCode/examplePaper/section_3.tex | false
work/Calc1/qbank/2.2/graphical1.tex | false
work/tutoring-old/zachary/zachary_310314.tex | true
work/tutoring-old/koceski-12.3.5-ode.tex | true
work/tutoring-old/zachary/zachary_021212.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Calc1/notes/3.11/main.inc.tex | false
work/Calc1/notes/4.9/main.inc.tex | false
work/Calc1/notes/2.7/main.inc.tex | false
work/Calc1/notes/4.8/main.inc.tex | false
work/Calc1/notes/3.1/main.inc.tex | false
work/Calc1/notes/2.2/main.inc.tex | false
work/Calc1/notes/main.inc.tex | false
work/Calc1/notes/4.5/main.inc.tex | false
work/Calc1/notes/5.4/main.inc.tex | false
work/Calc1/notes/4.3/main.inc.tex | false
work/Calc1/notes/3.2/main.inc.tex | false
work/Calc1/notes/4.7/main.inc.tex | false
work/Calc1/notes/4.2/main.inc.tex | false
work/Calc1/notes/3.3/main.inc.tex | false
work/Calc1/notes/2.8/main.inc.tex | false
work/Calc1/notes/3.4/main.inc.tex | false
work/Calc1/qbank/quiz1.tex | true
work/Calc1/notes/5.1/main.inc.tex | false
work/Calc1/notes/3.7/main.inc.tex | false
work/Calc1/notes/5.2/main.inc.tex | false
work/Calc1/notes/5.3/main.inc.tex | false
work/Calc1/notes/3.5/main.inc.tex | false
work/Calc1/notes/3.9/main.inc.tex | false
work/Calc1/thms.tex | false
work/Calc1/notes/3.6/main.inc.tex | false
work/Calc1/notes/2.5/main.inc.tex | false
work/Calc1/notes/2.6/main.inc.tex | false
work/Calc1/qbank/quiz5.tex | true
work/Calc1/notes/2.3/main.inc.tex | false
work/Calc1/notes/4.4/main.inc.tex | false
work/Calc1/notes/4.1/main.inc.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/MISP-diff | true | *false*
research/optcont/misc-notes | true | *false*
research/optcont/MCode-Constrained | true | *false*

### New Tests
File/Test | New
----------|----
research/optcont/MISPCode/examplePaper | true
work/Calc2-old/Su10 | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/projects/typing | true
work/GrpProj/Calc2 | true
work/MAC | true
work/misc | true
work/training/latex/BlankProj | true
work/projects/ugr/fa13/laite | true
research/optcont/papers/conv-a-paper/common | true
work/training/maxima | true
work/projects/useR | true
work/projects/ugr/fa12 | true
work/matlab-workshop/materials/Advanced/NPDE | true
work/projects/ugr/fa13 | true
work/Calc3/quizzes | true
work/siam-chapter/ayr12 | true
work/DiffEq/hw-gr | true
work/misc/refletter | true
work/siam-chapter/summer_school/2013 | true
work/projects/abdulla/NSF-2015 | true
work/Models | true
work/siam-chapter/ayr13/reading-group | true
work/tutoring | true
work/matlab-workshop/13.8.31-combined | true
work/projects/abdulla | true
work/projects/dsct-laserday | true
work/projects/ugr/sp13/bpoggi | true
work/GrpProj/Calc3 | true
work/training/VCS | true
work/tutoring/zachary | true
work/GrpProj/Calc3/g2.inc | true
work/projects/ugr/docs/ode | true
work/projects/typing/fulton-siam-2016 | true
work/common | true
work/matlab-workshop/feedback | true
work/projects/typing/jmandelkern | true
work/training/julia | true
work/GrpProj/Calc3/g3.inc | true
work/NumericalMethodsBook | true
work/GrpProj/Calc3/g4.inc | true
work/siam-chapter/summer_school/notes | true
work/GrpProj | true
work/siam-chapter/ayr13 | true
work/projects/ugr/sp12/nmertins | true
work/siam-chapter/summer_school/2013/yrnotes | true
work/projects/ugr/sp13/malik | true
research/optcont/papers/conv-a-paper/paper-parts | true
work/GrpProj/Calc1 | true
work/ProbStat | true
work/DiffEq/quiz | true
work/DiffEq/proj | true
work/training/latex | true
work/siam-chapter/summer_school | true
work/siam-chapter | true
work/GrpProj/DiffEq | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/optcont/JCode/SourceDeps.jl/LICENSE | false
research/optcont/JCode/ISP.jl/README | false
research/optcont/JCode/PGFPlotsProcess.jl/LICENSE | false
work/projects/ugr/README | true
research/optcont/JCode/SourceDeps.jl/README | false
work/projects/ugr/LICENSE | true
research/optcont/JCode/PGFPlotsProcess.jl/README | false
research/optcont/JCode/ISP.jl/LICENSE | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/tutoring-old/undergrad_research_mamie_sp11/dataimport.m | false
work/tutoring-old/kosak-zhora-1.m | false
work/Models-old/matlab/stefan2.m | true
work/Calc2-old/Sp12/polarplots.nb | true
work/tutoring-old/susanna-1.m | true
work/Models-old/lab-2.nb | false
work/Models-old/matlab/stefan.m | true
work/Models-old/lab-1.nb | false
work/Models-old/mathematica/project-1.m | false
work/tutoring-old/ramya.m | false
work/Models-old/project-1.nb | false
work/Models-old/mathematica/lab-2.m | false
work/Models-old/matlab/project1_plotmap.m | true
work/Models-old/matlab/stefan_u.m | true
work/Models-old/matlab/project1_model.m | true
work/Models-old/mathematica/lab-1.m | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Models/mathematica/lab-2.m | false
work/Models/project-1.nb | false
work/Calc2/Sp12/polarplots.nb | true
work/tutoring/ramya.m | false
work/Models/matlab/project1_model.m | true
work/Models/matlab/stefan2.m | true
work/Models/mathematica/project-1.m | false
work/Models/matlab/stefan_u.m | true
work/Models/mathematica/lab-1.m | false
work/Models/lab-1.nb | false
work/tutoring/kosak-zhora-1.m | false
work/Models/matlab/stefan.m | true
work/tutoring/undergrad_research_mamie_sp11/dataimport.m | false
work/Models/lab-2.nb | false
work/Models/matlab/project1_plotmap.m | true
work/tutoring/susanna-1.m | true



