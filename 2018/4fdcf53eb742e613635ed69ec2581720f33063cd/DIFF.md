## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/Calc1/qbank/quiz1.tex | true
research/fda-pde/ml-ufldl/ufldl.pdf | true
research/fda-pde/ml-ufldl/figures/func-sigmoid-tanh.tex | false
work/Calc1/qbank/quiz5.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/MISPCode | false | *true*

### New Tests
File/Test | New
----------|----
research/fda-pde/ml-ufldl | true
work/Calc1/qbank | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/fda-pde/ml-ufldl/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/minFunc_processInputOptions.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/WolfeLineSearch.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/precondDiag.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/lbfgsProd.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/sigmoid.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/taylorModel.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/precondTriuDiag.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/ex1_load_mnist.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/mexAll.m | false
research/fda-pde/ml-ufldl/MLTut/stl/feedfowardRICA.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/linear_regression.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/logistic_regression_vec.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/logisticExample/mylogsumexp.m | false
research/fda-pde/ml-ufldl/MLTut/common/loadMNISTImages.m | false
research/fda-pde/ml-ufldl/MLTut/multilayer_supervised/initialize_weights.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/autoDif/autoHv.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/conjGrad.m | false
research/fda-pde/ml-ufldl/MLTut/rica/bsxfunwrap.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/autoDif/autoTensor.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/rosenbrock.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/ArmijoBacktrack.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/logisticExample/LogisticDiagPrecond.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/mchol.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/polyinterp.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/linear_regression_vec.m | false
research/fda-pde/ml-ufldl/MLTut/common/loadMNISTLabels.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/isLegal.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/minFuncSGD.m | false
research/fda-pde/ml-ufldl/MLTut/rica/softICACost.m | false
research/fda-pde/ml-ufldl/MLTut/stl/stlExercise.m | false
research/fda-pde/ml-ufldl/MLTut/rica/showBases.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/autoDif/autoHess.m | false
research/fda-pde/ml-ufldl/MLTut/common/display_network.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnParamsToStack.m | false
research/fda-pde/ml-ufldl/MLTut/multilayer_supervised/params2stack.m | false
research/fda-pde/ml-ufldl/MLTut/rica/l2rowscaledg.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnConvolve.m | false
research/fda-pde/ml-ufldl/MLTut/rica/runSoftICA.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/lbfgsAdd.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/example_derivativeCheck.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/ex1b_logreg.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/logisticExample/LogisticHv.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/lbfgs.m | false
research/fda-pde/ml-ufldl/MLTut/multilayer_supervised/supervised_dnn_cost.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/ex1c_softmax.m | false
research/fda-pde/ml-ufldl/MLTut/multilayer_supervised/stack2params.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/logistic_regression.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/ex1a_linreg.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnCost.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/logisticExample/LogisticLoss.m | false
research/fda-pde/ml-ufldl/MLTut/rica/zca2.m | false
research/fda-pde/ml-ufldl/MLTut/pca/pca_gen.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnPool.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/logisticExample/example_minFunc_LR.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/softmax_regression_vec.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/grad_check.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/autoDif/derivativeCheck.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/multi_classifier_accuracy.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/minFunc.m | false
research/fda-pde/ml-ufldl/MLTut/multilayer_supervised/run_train.m | false
research/fda-pde/ml-ufldl/MLTut/common/samplePatches.m | false
research/fda-pde/ml-ufldl/MLTut/rica/l2rowscaled.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/autoDif/autoGrad.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/precondTriu.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/example_minFunc.m | false
research/fda-pde/ml-ufldl/MLTut/multilayer_supervised/load_preprocess_mnist.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnTrain.m | false
research/fda-pde/ml-ufldl/MLTut/rica/removeDC.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/autoDif/fastDerivativeCheck.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnInitParams.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/computeNumericalGradient.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/lbfgsUpdate.m | false
research/fda-pde/ml-ufldl/MLTut/cnn/cnnExercise.m | false
research/fda-pde/ml-ufldl/MLTut/ex1/binary_classifier_accuracy.m | false
research/fda-pde/ml-ufldl/MLTut/rica/preprocess.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/mcholinc.m | false
research/fda-pde/ml-ufldl/MLTut/common/minFunc_2012/minFunc/dampedUpdate.m | false



