## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/projects/blog-drafts/continuous-paper-integration.tex | true
work/projects/blog-drafts/smooth-truncation-operators.tex | true
work/projects/blog-drafts/tpl.tex | true
work/MAC/NewGSAWorkshop.slide.tex | true
work/MAC/recruiting.tex | false
work/projects/blog-drafts/estimating-transportation-schedules.tex | true
work/MAC/InfoSession.tex | true
work/projects/blog-drafts/random-function-variates.tex | true
work/MAC/NewGSAWorkshop.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
work/MAC | true | *false*
research/optcont/MISPCode | true | *false*
misc/resume | true | *false*

### New Tests
File/Test | New
----------|----
work/projects/blog-drafts | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




