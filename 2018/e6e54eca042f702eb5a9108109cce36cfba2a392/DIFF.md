## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/talks/jmm_17/diff-paper-parts/more-weak-solution-defn-change.tex | false
research/talks/jmm_17/diff-paper-parts/preamble-defn-macros.tex | false
research/optcont/papers/MISP-general/NonlinearParabolicPaper.pdf | true
research/talks/jmm_17/diff-paper-parts/adjoint-exists-s.tex | false
research/talks/jmm_17/diff-paper-parts/notation-l2omega.tex | false
research/talks/jmm_17/diff-paper-parts/more-weak-solution-defn-eqn.tex | false
research/talks/jmm_17/diff-paper-parts/j-well-defined-result.tex | false
research/talks/jmm_17/diff-paper-parts/more-weak-solution-defn.tex | false
research/talks/jmm_17/diss-paper-parts/preamble-cmds.tex | false
research/talks/jmm_17/diss-paper-parts/preamble-cmds-other.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/papers/nd-sphere-paper/common/assumptions.tex | false
research/talks/jmm_19/figures/tex/isp-with-domain.tex | false
research/optcont/papers/nd-sphere-paper/common/weak-soln-derivation.tex | false
research/optcont/papers/nd-sphere-paper/common/preamble.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/discretization-interp.tex | false
research/talks/jmm_19/preamble-cmds.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-classical.tex | false
research/talks/siam_seas14/03.29.14-talk.tex | true
research/optcont/papers/nd-sphere-paper/common/paper-beginning-general-v1.tex | false
research/optcont/papers/nd-sphere-paper/common/formulation-exterior.tex | false
research/talks/jmm_19/preamble.tex | false
research/optcont/papers/nd-sphere-paper/common/defn-macros.tex | false
research/talks/jmm_19/preamble-cmds-other.tex | false
research/optcont/papers/nd-sphere-paper/common/nd-spherical-coords.tex | false
research/optcont/papers/nd-sphere-paper/common/parts-tpl.tex | false
research/optcont/papers/nd-sphere-paper/common/cont-sobolev-energy-estimate.tex | false
research/talks/jmm_19/disc.tex | false
research/talks/jmm_19/abstract-indep.tex | true
research/talks/jmm_19/figures/tex/with-domain.tex | false
research/optcont/papers/nd-sphere-paper/common/cmds.tex | false
research/optcont/papers/nd-sphere-paper/future.tex | true
research/optcont/papers/MISP-general/NonlinearParabolicPaper.tex | true
research/optcont/papers/nd-sphere-paper/common/functional-well-defined.tex | false
research/talks/jmm_19/abstract.tex | false
research/talks/jmm_19/apps-diff.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/defn-macros.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/discretization-state-vec.tex | false
research/talks/jmm_19/preamble-dir.tex | false
research/optcont/papers/nd-sphere-paper/common/energy-est-u.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/defn-macros.tex | false
research/talks/jmm_19/diff-statement.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-multiphase.tex | false
research/optcont/papers/nd-sphere-paper/common/energy-est-int1.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/defn-macros.tex | false
research/optcont/papers/nd-sphere-paper/common/cont-existence-galerkin.tex | false
research/optcont/papers/nd-sphere-paper/common/transformation-of-domain.tex | false
research/talks/jmm_19/pgf-include.tex | false
research/optcont/papers/nd-sphere-paper/common/v2-estimate-noncylindrical.tex | false
research/optcont/papers/nd-sphere-paper/main.inc.tex | false
research/talks/jmm_19/figures/tex/before-domain.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/discretization-defns.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/tpl.tex | false
research/optcont/papers/nd-sphere-paper/paper-parts/discretization-defns.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/tpl.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/discretization-interp.tex | false
research/talks/jmm_19/intro.tex | false
research/optcont/papers/nd-sphere-paper/common/cont-sobolev-energy-estimate-2.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-main.tex | false
research/optcont/papers/nd-sphere-paper/common/paper-beginning-formulation.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/discretization-defns.tex | false
research/optcont/papers/nd-paper/semidisc-paper-parts/tpl.tex | false
research/talks/jmm_19/diff.tex | false
research/talks/jmm_19/apps.tex | false
research/optcont/papers/nd-paper/disc-paper-parts/discretization-interp.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/nd-paper | false | *true*
research/seminar/day-to-day-FOSS | true | *false*
research/talks/jmm_19 | false | *true*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/talks/siam_seas14 | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/talks/ky_aug2011/README | false
research/optcont/papers/MISP-general/README | true
work/MathEdResearch/PBI/LICENSE | false
research/talks/jmm_19/LICENSE | true
research/talks/siam_seas14/LICENSE | false
research/talks/siam_seas14/README | false
research/optcont/papers/nd-sphere-paper/LICENSE | true
research/talks/siam_an12/LICENSE | false
research/talks/jmm_19/README | true
research/talks/siam_pd13/README | false
research/talks/siam_pd13/LICENSE | false
research/optcont/papers/nd-sphere-paper/README | true
work/MathEdResearch/PBI/README | true
work/projects/abdulla/site/README | true
research/talks/siam_an12/README | false
research/talks/ky_aug2011/LICENSE | false
work/projects/abdulla/site/LICENSE | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




