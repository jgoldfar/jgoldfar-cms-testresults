## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/GrpProj/Calc1/pCh2.PP.14.inc.tex | false
work/GrpProj/Calc1/p4.3.82.inc.tex | false
work/GrpProj/Calc1/p3.7.23.inc.tex | false
work/GrpProj/Calc1/p4.4.86.inc.tex | false
work/GrpProj/Calc1/p4.7.47.inc.tex | false
work/GrpProj/Calc1/p4.7.53.inc.tex | false
work/GrpProj/Calc1/p3.1.74.inc.tex | false
work/GrpProj/Calc1/p2.7.55.inc.tex | false
work/GrpProj/Calc1/p2.7.53.inc.tex | false
work/GrpProj/Calc1/p4.7.80.inc.tex | false
work/GrpProj/Calc1/p4.4.80.inc.tex | false
work/GrpProj/Calc1/p3.2.61.inc.tex | false
work/GrpProj/Calc3/g4.inc/p5.inc.tex | false
work/GrpProj/Calc1/p4.7.71.inc.tex | false
work/GrpProj/Calc1/p2.7.57.inc.tex | false
work/GrpProj/Calc1/p3.3.46.inc.tex | false
work/GrpProj/Calc1/p3.4.98.inc.tex | false
work/GrpProj/Calc1/p4.1.77.inc.tex | false
work/GrpProj/Calc1/pCh2.PP.8.inc.tex | false
work/GrpProj/Calc3/g4.inc/p4.inc.tex | false
work/GrpProj/Calc1/p2.5.64.inc.tex | false
work/GrpProj/Calc1/p4.2.5.inc.tex | false
work/GrpProj/preamble.inc.tex | false
work/GrpProj/Calc1/p4.3.80.inc.tex | false
work/GrpProj/Calc2/pfc644cdbd34d9a4a974e7627338ce726.inc.tex | false
work/GrpProj/Calc1/p2.5.61.inc.tex | false
work/GrpProj/Calc3/g2.inc/p2.inc.tex | false
work/GrpProj/Calc1/p2.4.25.inc.tex | false
work/GrpProj/DiffEq/problems.tex | false
work/GrpProj/Calc1/p4.2.9.inc.tex | false
work/GrpProj/Calc1/p3.7.39.inc.tex | false
work/GrpProj/Calc1/pCh3.PP.11.inc.tex | false
work/GrpProj/Calc1/p3.3.71.inc.tex | false
work/GrpProj/Calc1/p3.5.71.inc.tex | false
work/GrpProj/Calc1/p3.1.72.inc.tex | false
work/GrpProj/Calc1/p3.2.62.inc.tex | false
work/GrpProj/Calc1/p3.7.29.inc.tex | false
work/GrpProj/Calc3/g3.inc/p3.inc.tex | false
work/GrpProj/Calc3/p1.inc.tex | true
work/GrpProj/Calc1/p3.5.46.inc.tex | false
work/GrpProj/Calc1/p2.6.69.inc.tex | false
work/GrpProj/Calc1/p2.6.68.inc.tex | false
work/GrpProj/pTpl.inc.tex | false
work/GrpProj/Calc1/p4.2.21.inc.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/GrpProj/DiffEq/main.tex | true
work/GrpProj/Calc3/2/Project2.tex | true
work/GrpProj/Calc3/1/Project1.tex | true
work/GrpProj/Calc3/4/Project5.tex | true
work/GrpProj/Calc3/4/Project4.tex | true
work/GrpProj/Calc3/3/Project3.tex | true
work/GrpProj/Calc1/SelectedProblemsAndInstructions.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/GrpProj/Calc3/g2.inc | true
work/GrpProj/Calc2 | true
work/GrpProj/Calc3/g3.inc | true
work/GrpProj/Calc3 | true
work/GrpProj/Calc1 | true
work/GrpProj | true
work/GrpProj/Calc3/g4.inc | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/GrpProj/Calc3/2 | true
work/GrpProj/Calc3/3 | true
work/GrpProj/Calc3/4 | true
work/GrpProj/Calc3/1 | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/MAC/GraphicalIdentity/README | false
work/MAC/GraphicalIdentity/LICENSE | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




