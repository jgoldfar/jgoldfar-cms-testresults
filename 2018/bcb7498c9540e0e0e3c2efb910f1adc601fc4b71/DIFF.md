## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/Calc1/assess/quiz2Alt.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/Calc1/assess/test1Solns.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/MISPCode | true | *false*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




