## PDF Files Compile

File/Test | Old | New
----------|-----|----
research/seminar/12.7.23_seminar.tex | true | *false*

### New Tests
File/Test | New
----------|----
research/seminar/day-to-day-FOSS/main.tex | true
misc/env/tex-include/Templates/beamer-noinc.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/MAC/NewGSAWorkshop.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
work/MAC | false | *true*
misc/resume | false | *true*

### New Tests
File/Test | New
----------|----
research/seminar/day-to-day-FOSS | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/seminar/day-to-day-FOSS/README | false
research/seminar/day-to-day-FOSS/LICENSE | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/ISP.jl/DataProvenance/LICENSE | false
research/optcont/ISP.jl/ISPEx/LICENSE | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




