## PDF Files Compile

File/Test | Old | New
----------|-----|----
misc/resume/courses.pdf | true | *false*
research/optcont/misc-notes/general-mapping-construction.tex | true | *false*

### New Tests
File/Test | New
----------|----
research/optcont/ODE/ode-opt-full.tex | false
research/optcont/papers/MISP-diff/verif/TSE-Gamma.tex | false
research/optcont/ODE/paper-linfinity.tex | false
research/talks/ISP_Summary/paper-parts/paper-beginning-abdulla-func.tex | false
research/optcont/ODE/ode-opt-extended.tex | false
research/optcont/ODE/ode-opt.tex | false
research/talks/ISP_Summary/paper-parts/paper-beginning-abdulla-controlset.tex | false
grad/dissertation/talk/paper-parts/paper-beginning-abdulla-func.tex | false
grad/dissertation/talk/paper-parts/paper-beginning-abdulla-controlset.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/ODE/ode-opt.pdf | true
research/optcont/ODE/ode-opt-extended.pdf | true
research/optcont/ODE/ode-opt-full.pdf | true
work/misc/refletter/mnguyen.tex | true
research/optcont/MISPCode/MISPTools/example/TikhonovSamarskiiResults.tex | false
research/optcont/ODE/paper-linfinity.pdf | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/misc-notes | true | *false*
research/optcont/MCode-Constrained | true | *false*
misc/resume | true | *false*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
misc/resume/cv-secs | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




