## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/qbank/5.4/main.tex | false | *true*
work/Calc1/qbank/4.3/main.tex | false | *true*
work/Calc1/qbank/4.1/main.tex | false | *true*
work/Calc1/qbank/3.11/main.tex | false | *true*
work/Calc1/qbank/5.1/main.tex | false | *true*
work/Calc1/qbank/4.8/main.tex | false | *true*
work/Calc1/qbank/2.5/main.tex | false | *true*
work/Calc1/qbank/2.7/main.tex | false | *true*
work/Calc1/qbank/4.9/main.tex | false | *true*
work/Calc1/qbank/3.4/main.tex | false | *true*
work/Calc1/qbank/4.2/main.tex | false | *true*
work/Calc1/qbank/2.3/main.tex | false | *true*
work/Calc1/qbank/2.8/main.tex | false | *true*
work/Calc1/qbank/3.1/main.tex | false | *true*
work/Calc1/qbank/4.5/main.tex | false | *true*
work/Calc1/qbank/4.7/main.tex | false | *true*
work/Calc1/qbank/5.3/main.tex | false | *true*
work/Calc1/qbank/3.5/main.tex | false | *true*
work/Calc1/qbank/3.6/main.tex | false | *true*
work/Calc1/qbank/3.2/main.tex | false | *true*
work/Calc1/qbank/2.6/main.tex | false | *true*
work/Calc1/qbank/3.9/main.tex | false | *true*
work/Calc1/qbank/4.4/main.tex | false | *true*
work/Calc1/qbank/5.2/main.tex | false | *true*
work/Calc1/qbank/3.3/main.tex | false | *true*
work/Calc1/qbank/3.7/main.tex | false | *true*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/fda-pde/ml-ufldl/figures/func-sigmoid-tanh.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




