## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/Calc1/thms.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/JCode/ISP.jl/doc/figures/PrintDiag.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/MISPCode | false | *true*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
research/optcont/JCode/HotCode.jl/LICENSE | true | *false*
research/optcont/JCode/HotCode.jl/README | true | *false*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/JCode/PGFPlotsProcess.jl/README | true
research/optcont/JCode/ISP.jl/README | true
research/optcont/JCode/ISP.jl/LICENSE | true
research/optcont/JCode/SourceDeps.jl/README | true
research/optcont/JCode/SourceDeps.jl/LICENSE | true
research/optcont/JCode/PGFPlotsProcess.jl/LICENSE | true
research/optcont/JCode/ISP.jl/doc/LICENSE | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/JCode/ISP.jl/num/ezplot3.m | false



