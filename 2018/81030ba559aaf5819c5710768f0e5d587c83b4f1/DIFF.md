## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/projects/blog-drafts/generalized-jensens.tex | false | *true*

### New Tests
File/Test | New
----------|----
research/optcont/papers/nd-paper/verif/aijkl-v-deriv.tex | false
research/optcont/papers/nd-paper/verif/utsq-simple.tex | false
research/talks/siam_seas14/03.29.14-talk.tex | true
work/projects/blog-drafts/brief-ift.tex | true
work/projects/blog-drafts/tikz-standalone-preamble.tex | false
work/projects/blog-drafts/bounded-laplacian-implies-all-second-derivatives.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
work/projects/blog-drafts | true | *false*
research/optcont/papers/nd-paper | true | *false*
research/seminar/day-to-day-FOSS | false | *true*
research/optcont/papers/nd-paper/common | true | *false*
research/optcont/papers/nd-paper/semidisc-paper-parts | true | *false*

### New Tests
File/Test | New
----------|----
research/talks/siam_seas14 | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/MathEdResearch/DataDrivenDecisionmaking/LICENSE | false
work/MathEdResearch/DataDrivenDecisionmaking/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




