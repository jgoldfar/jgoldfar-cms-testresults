## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/notes/main.tex | true | *false*
work/Calc1/qbank/2.2/main.tex | false | *true*
work/Calc1/notes/4.4/main.tex | true | *false*

### New Tests
File/Test | New
----------|----
work/Calc1/assess/test2Graphing.tex | true
work/Calc1/assess/quiz3Soln.tex | true
work/Calc1/assess/newtonBonus.tex | true
research/optcont/MISPCode/MISPTools/example/TikhonovSamarskiiResults.pdf | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/MISPCode/MISPExamples/doc/preamble-cmds.tex | false
research/optcont/MISPCode/MISPBase/doc/preamble-cmds.tex | false
research/optcont/MISPCode/MISPBase/doc/statevector-generalgrids.tex | false
research/optcont/MISPCode/MISPExamples/doc/preamble.tex | false
research/optcont/MISPCode/MISPBase/doc/adjoint.tex | false
research/optcont/MISPCode/MISPTools/example/TikhonovSamarskiiResults.tex | false
research/optcont/MISPCode/MISPBase/doc/model-simple.tex | false
research/optcont/MISPCode/MISPExamples/doc/numerics-model-1.tex | false
research/optcont/MISPCode/MISPBase/doc/MOL.tex | false
research/optcont/MISPCode/MISPBase/doc/statevector.tex | false
research/optcont/MISPCode/MISPBase/doc/preamble.tex | false
research/optcont/MISPCode/MISPBase/doc/preamble-defn-macros.tex | false
research/optcont/MISPCode/MISPBase/doc/statevector-jacobian.tex | false
research/optcont/MISPCode/MISPExamples/doc/preamble-defn-macros.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
work/MAC | true | *false*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




