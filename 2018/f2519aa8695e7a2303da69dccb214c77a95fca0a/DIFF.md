## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/MCode-Constrained/test/report1-FuncQuad.tex | false | 
| research/optcont/MCode-Constrained/test/report1-heatSolveConverges.tex | false | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/nd-paper | true | *false* |
| research/optcont/MCode-Constrained | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




