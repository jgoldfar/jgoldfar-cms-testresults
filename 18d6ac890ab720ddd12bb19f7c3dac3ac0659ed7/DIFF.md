## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/quant-bio/schrodinger-no-diffusion/main.tex | false

### No Lost/Removed Tests



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/quant-bio/schrodinger-no-diffusion | true

### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/quant-bio/schrodinger-no-diffusion/LICENSE | false
research/quant-bio/schrodinger-no-diffusion/README | true

### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/optcont/ISP-Ali/src/initial_setup.m | false
research/optcont/ISP-Ali/src/test_Forward.m | false
research/optcont/ISP-Ali/src/test.m | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/ISP-Ali/src/check.m | false
research/optcont/ISP-Ali/src/Backward.m | false



