const baseDir = isempty(ARGS) ? @__DIR__() : popfirst!(ARGS)

# Organize all files corresponding to one build into the same folder
LogTranslations(id) = [
	(string("build-",id,".log"), "build.log"),
	(string(id, ".stdout"), "stdout.log"),
	(string(id, ".stderr"), "stderr.log")
]

function do_organization_v1()
    files = readdir(baseDir)

    ids = map(t->first(splitext(t)), filter(t->endswith(t, ".stderr"), files))

    for id in ids
        mkpath(joinpath(baseDir, id))
        for (src, dest) in LogTranslations(id)
            destpath = joinpath(id, dest)
            if src in files
                mv(src, destpath)
            else
                if destpath in files
                    info(src, " already moved.")
                else
                    warn(src, " missing.")
                end
            end
        end
    end
end
# do_organization_v1()

const ignoredFolders = [".git"]

const repoRoot = isempty(ARGS) ? joinpath(baseDir, "..", "..") : popfirst!(ARGS)

const existingFolders = filter(x->(isdir(x) && !in(x, ignoredFolders)), readdir(baseDir))

uploadedSentinel(rptbasedir, id) = joinpath(rptbasedir, id, "uploaded")

uploadedContent(id) = string(
    "Committed: ",
    readchomp(`hg log --cwd "$(repoRoot)" -r "$(id)" -T '{date|isodate}'`),
    "\nUploaded: ",
    DateTime(now())
    )

function add_sentinels(folders = existingFolders, baseDir = baseDir)
    for folder in folders
        open(uploadedSentinel(baseDir, folder), "w") do st
            print(st, uploadedContent(folder))
        end
    end
end
# add_sentinels()

function addIndex(folders = existingFolders, baseDir = baseDir)
    open(joinpath(baseDir, "INDEX.dat"), "w") do st
        for folder in folders
            println(st, readchomp(`hg log --cwd "$(repoRoot)" -r "$(folder)" -T '{word(0, date|hgdate)} : {node}'`))
        end
    end
end
# addIndex()
