## PDF Files Compile

File/Test | Old | New
----------|-----|----
misc/env/tex-include/Templates/full.tex | false | *true*
grad/linear/hw3.tex | false | *true*
grad/apdes/su11/final.pdf | false | *true*
grad/pdes/exercises.tex | false | *true*
notes/redo/linear.tex | false | *true*
work/Calc2/Su10/pfd.tex | false | *true*
grad/odestheory/midterm1.tex | false | *true*
work/alg/curr/1012-lec/flashcards.tex | false | *true*
notes/redo/stochastic.tex | false | *true*
grad/numerical/hw3.tex | false | *true*
misc/env/tex-include/Templates/flashcards.tex | false | *true*
grad/odestheory/midterm2.tex | false | *true*
notes/redo/mathstat.tex | false | *true*
notes/redo/pde.tex | false | *true*
grad/numerical/test1.tex | false | *true*
grad/linear/test3.tex | false | *true*
grad/linear/hw2.tex | false | *true*
grad/numerical/hw1.tex | false | *true*
research/seminar/10.10.20_seminar.tex | false | *true*
grad/numerical/hw2.tex | false | *true*
grad/odestheory/finaltest.tex | false | *true*
grad/linear/hw1.tex | false | *true*
grad/apdes/su11/final-solns.pdf | false | *true*
grad/mathmethods/ex1.tex | false | *true*
grad/apdes/su11/hw1.pdf | false | *true*
grad/linear/extra.tex | false | *true*
grad/apdes/su11/test1.pdf | false | *true*
research/seminar/10.9.15_seminar.tex | false | *true*
notes/redo/theoryodes.tex | false | *true*
grad/functional/exercises.tex | false | *true*

### New Tests
File/Test | New
----------|----
misc/env/tex-include/Templates/less-minimal-cmds.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




