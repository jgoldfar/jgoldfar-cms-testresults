## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/seminar/SP19-Opt/main.tex | true
research/paramid/LV-Diffusion/scratch.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/nd-paper | true | *false*
research/optcont/papers/nd-paper/common | true | *false*

### New Tests
File/Test | New
----------|----
research/seminar/SP19-Opt | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
research/seminar/SP19-Opt/LICENSE | true
research/seminar/SP19-Opt/README | true



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




