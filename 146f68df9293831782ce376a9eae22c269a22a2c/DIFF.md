## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| research/quant-bio/schrodinger-no-diffusion/main.tex |
| work/projects/reu-admin/notes/npde/report/fdiff-slides.tex |
| work/projects/reu-admin/notes/npde/report/old-pres.tex |
| grad/dissertation/proposal/res-num/collected-indep.tex |
| research/optcont/papers/MISP-diff/orig.pdf |
| research/optcont/ISP-Ali/scratch.tex |



### New Tests
File/Test | Result
----------|----
research/quant-bio/schrodinger-no-diffusion/regularization.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/projects/conf-gen/certificates.tex | false
work/projects/conf-gen/reg-form.tex | false



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/quant-bio/rashad-matlab/singlet_expectation.m | false
research/quant-bio/rashad-matlab/ForwardScratch.m | false
research/quant-bio/rashad-matlab/HFI.m | false

### No Lost/Removed Tests



