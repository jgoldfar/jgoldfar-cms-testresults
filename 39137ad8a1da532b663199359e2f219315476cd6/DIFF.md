## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| work/siam-chapter/ayr12/flyer-talk-13.3.29.tex |
| research/optcont/papers/conv-paper/main-disc.pdf |
| work/alg/planning/MTH1012-syllabus-prelim.tex |
| work/projects/reu-admin/notes/npde/abdulla-NonlinearPDEs-5.tex |
| work/MAC/NewFacWorkshop.slide.tex |
| research/optcont/papers/isp-constrained/presentation.tex |
| research/fda-pde/ml-ufldl/ufldl.pdf |
| research/optcont/papers/conv-paper/main-OCAM.pdf |
| work/MAC/NewFacWorkshop.printout.tex |
| work/acalc/1603/syllabus-abbrv.tex |
| work/siam-chapter/ayr12/flyer-talk-13.03.22.tex |
| work/acalc/1702/syllabus.tex |
| work/siam-chapter/ayr12/flyer-talk-13.03.22-v1.tex |
| work/siam-chapter/ayr12/flyer-talk-13.3.29-fitlogo.tex |
| research/optcont/papers/conv-paper/main-paper.pdf |
| work/acalc/1702/syllabus-abbrv.tex |
| work/alg/curr/1012-lec/limits-intro-1-ex.pdf |
| work/alg/curr/MTH0111-adv-exam.tex |
| research/talks/nsf_grfp/experience.tex |
| research/talks/ky_aug2011/poster.tex |
| grad/stochastic/stochastic_probset2.tex |
| research/seminar/minilec/picards-thm_seminar.tex |
| research/optcont/misc-notes/disc-isp-nondiff.tex |
| work/alg/curr/1701-lec/all.pdf |
| research/talks/nsf_grfp/personal.tex |
| work/MAC/NewGSAWorkshop.slide.tex |
| work/misc/refletter/dmccormick.tex |
| work/MunkresSolutions/TopologyMunkresSolns.tex |
| research/optcont/papers/diff-c-paper/YMCposter.tex |
| work/alg/planning/MTH1011-syllabus-prelim.tex |
| grad/dissertation/proposal/notes.tex |
| work/siam-chapter/ayr12/flyer-talk-13.2.22.tex |
| work/misc/refletter/ecosgrove.tex |
| work/alg/curr/1012-lec/limits-intro-1.pdf |
| work/siam-chapter/summer_school/flyer-template.tex |
| work/acalc/1603/syllabus.tex |
| research/seminar/12.7.23_seminar.tex |
| work/alg/curr/MTH1701-calendar.tex |



### New Tests
File/Test | Result
----------|----
misc/julia/Mollifiers/doc.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/acalc/calc-sec/tex-include/Templates/amsart-master.tex | true
work/acalc/calc-sec/tex-include/Templates/More/include-gen-lengths.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-gen-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/less-minimal-cmds.tex | false
work/acalc/calc-sec/tex-include/Templates/More/archive.tex | false
work/acalc/calc-sec/tex-include/Templates/semi-minimal-cmds.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-gen.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-cmds-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-triv-sweave.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-cmds.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-pkg-gen.tex | false
work/acalc/calc-sec/tex-include/Templates/amsart-subfile.tex | true
work/acalc/calc-sec/tex-include/Templates/amsart.tex | true
work/acalc/calc-sec/tex-include/Templates/tikz-standalone.tex | true
work/alg/review/dirMacros.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-flashcards.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-pkg-gen-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-big-art-all-nohdr.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-pkg-tikz.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-pkg-tikz-knit.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-thm-env-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-thm-env-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/amsart-master-art.tex | true
work/acalc/calc-sec/tex-include/Templates/beamer.tex | true
work/acalc/calc-sec/tex-include/Templates/More/include-pkg-maths.tex | false
work/acalc/calc-sec/tex-include/Templates/amsart-triv-sweave.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-big-all-nohdr.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-gen.tex | false
work/acalc/calc-sec/tex-include/Templates/amsart-triv-noinc.tex | true
work/acalc/calc-sec/tex-include/Templates/beamer-1.tex | true
work/acalc/calc-sec/tex-include/Templates/More/triv-include-thm-env.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-triv-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/beamer-noinc.tex | true
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-big-art.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-pkg-gen-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-gen-sweave.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-flat.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-fancyhdr.tex | false
work/acalc/calc-sec/tex-include/Templates/amsart-triv.tex | true
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-triv.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-pkg-tikz.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-big.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-gen-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/letter.tex | true
work/acalc/calc-sec/tex-include/Templates/More/flashcards-include-gen.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-pkg-tikz-knit.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-pkg-gen.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart-triv-knit.tex | false
work/acalc/calc-sec/tex-include/Templates/full.tex | true
work/acalc/calc-sec/tex-include/Templates/More/triv-include-pkg-gen-flashcards.tex | false
work/acalc/calc-sec/tex-include/Templates/amsart-sage.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-thm-env.tex | false
work/acalc/calc-sec/tex-include/Templates/More/triv-include-gen-knit.tex | false
work/acalc/calc-sec/tex-include/Templates/More/include-pkg-tikz-beamer.tex | false
work/acalc/calc-sec/tex-include/Templates/More/includeme-amsart.tex | false
work/acalc/calc-sec/tex-include/Templates/flashcards.tex | true



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| research/optcont/ISP-Ali |



### New Tests
File/Test | Result
----------|----
misc/julia/FeigenbaumUtil/data | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/acalc/calc-sec/tex-include | true
work/acalc/calc-sec/tex-include/Templates/More | true
work/acalc/calc-sec/tex-include/Templates | true



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/optcont/JCode/WeightedNorms/LICENSE | true
misc/julia/Libui/README | true
misc/julia/SPSRunner/LICENSE | true
research/numpde/FastMarchingMethod/README | true
misc/julia/Silo/README | true
misc/julia/MetaRowEchelon/LICENSE | true
misc/julia/ThisWeekInJulia/README | true
misc/julia/SPSInterface/README | true
work/training/latex/BlankProj/README | true
misc/julia/Libical/LICENSE | true
research/numpde/numbvp/BVP.jl/LICENSE | true
misc/julia/SPSBase/README | true
misc/julia/SiloBuilder/README | true
misc/julia/CouenneBuilder/LICENSE | true
misc/julia/Mollifiers/LICENSE | true
misc/julia/GraphCircuits/LICENSE | true
misc/julia/SPSRunner/README | true
misc/julia/Hypre/LICENSE | true
research/optcont/ISP.jl/README | true
misc/julia/FeigenbaumUtil/README | true
misc/julia/GraphCircuits/README | true
misc/julia/Silo/LICENSE | true
misc/julia/SiloBuilder/LICENSE | true
misc/julia/BonminBuilder/README | true
misc/julia/Librsync/LICENSE | true
misc/julia/Mollifiers/README | true
misc/julia/Quasis/README | true
misc/julia/SimpleGradientDescent/LICENSE | true
research/numpde/numbvp/Sewell.jl/LICENSE | true
misc/julia/XBraid/LICENSE | true
research/optcont/MISPCode/MISPExamples/LICENSE | false
research/optcont/MISPCode/MISPExamples/README | true
misc/julia/GroupTheoreticML/README | true
research/numpde/numbvp/BVP.jl/README | true
misc/julia/CouenneBuilder/README | true
misc/julia/LaTeXTools/README | true
misc/julia/FeigenbaumUtil/LICENSE | true
work/training/latex/BlankProj/LICENSE | true
misc/julia/MetaRowEchelon/README | true
misc/julia/MLforISP/README | true
misc/julia/SPSInterface/LICENSE | true
misc/julia/Termbox/LICENSE | true
misc/julia/igraph/LICENSE | true
misc/julia/ThisWeekInJulia/LICENSE | true
misc/julia/Termbox/README | true
misc/julia/Quasis/LICENSE | true
research/fda-pde/ml-ufldl/MLTut/LICENSE | true
misc/julia/MLforISP/LICENSE | true
misc/julia/igraph/README | true
research/numpde/FastMarchingMethod/LICENSE | true
misc/julia/PackageManagerCLI/LICENSE | true
misc/julia/GroupTheoreticML/LICENSE | true
misc/julia/BonminBuilder/LICENSE | true
research/optcont/MISPCode/MISPTools/README | true
misc/julia/Bitbucket/LICENSE | true
misc/julia/Libical/README | true
misc/julia/LaTeXTools/LICENSE | true
misc/julia/Libui/LICENSE | true
misc/julia/Hypre/README | true
research/optcont/MISPCode/MISPTools/LICENSE | true
misc/julia/SPSBase/LICENSE | true
misc/julia/XBraid/README | true
research/fda-pde/ml-ufldl/MLTut/README | true
research/numpde/numbvp/Sewell.jl/README | true
misc/julia/Librsync/README | true
research/optcont/JCode/WeightedNorms/README | true
misc/julia/SimpleGradientDescent/README | true
misc/julia/PackageManagerCLI/README | true
misc/julia/Bitbucket/README | true

### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



