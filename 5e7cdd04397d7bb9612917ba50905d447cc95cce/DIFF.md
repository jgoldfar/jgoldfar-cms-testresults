## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| work/APDE/hw/hw3.pdf |
| notes/finished/apdes.tex |
| work/APDE/hw/hw5.pdf |
| grad/complex/hw5.tex |
| grad/complex/hw7.tex |
| work/APDE/hw/hw12_4a-yeq.pdf |
| work/APDE/hw/hw6.pdf |
| work/APDE/hw/hw11.pdf |
| work/APDE/hw/hw2.pdf |
| grad/complex/test1.tex |
| work/APDE/hw/hw8.pdf |
| work/APDE/hw/hw7-p4-odes.pdf |
| work/APDE/hw/hw12.pdf |
| work/APDE/hw/hw1.pdf |
| work/APDE/hw/hw6-intbyparts.pdf |
| work/APDE/hw/hw8-3b-part.pdf |
| work/APDE/hw/hw2-s1.5-p6-explanation.pdf |
| grad/complex/hw3.tex |
| work/APDE/hw/hw7.pdf |
| work/APDE/hw/hw10.pdf |
| work/APDE/hw/hw12_9b.pdf |
| grad/complex/test2.tex |
| work/APDE/hw/hw9.pdf |
| grad/complex/hw1.tex |
| work/APDE/hw/hw14.pdf |
| grad/complex/hw2.tex |
| grad/complex/hw4.tex |



### New Tests
File/Test | Result
----------|----
notes/complex/sec6.tex | false
grad/mth6300/func-approx.pdf | false
notes/complex/sec7.tex | false
notes/complex/sec1.tex | false
grad/mth6300/ex1.pdf | true
notes/complex/sec8.tex | false
notes/complex/sec5.tex | false
grad/mth6300/notes.pdf | false
notes/complex/sec3.tex | false
notes/complex/sec2.tex | false
notes/complex/sec4.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
grad/mth6300/ex1.tex | true
grad/mth6300/notes.tex | false
grad/mth6300/func-approx.tex | false



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| work/Calc1/notes |



### No New Tests


### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



