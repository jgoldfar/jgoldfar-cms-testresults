## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
work/projects/reu-site/app | true
work/projects/typing/ua-pot | true

### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/quant-bio/rashad-matlab/singlet_expectation.m | false
research/quant-bio/rashad-matlab/GradientQuality.m | false
research/quant-bio/rashad-matlab/functions/Forward.m | false
research/quant-bio/rashad-matlab/SyntheticData.m | false
research/quant-bio/rashad-matlab/ForwardScratch.m | false
research/quant-bio/rashad-matlab/Regularize.m | false
research/quant-bio/rashad-matlab/functions/Objective.m | false
research/quant-bio/rashad-matlab/algorithms/initialize.m | false
research/quant-bio/rashad-matlab/Main.m | false
research/quant-bio/rashad-matlab/functions/Gradient.m | false
research/quant-bio/rashad-matlab/algorithms/visualize.m | false
research/quant-bio/rashad-matlab/functions/Adjoint.m | false
research/quant-bio/rashad-matlab/algorithms/algorithm.m | false
research/quant-bio/rashad-matlab/untitled.m | false
research/quant-bio/rashad-matlab/testODESolverParams.m | false
research/quant-bio/rashad-matlab/HFI.m | false
research/quant-bio/rashad-matlab/testComplexODE.m | false
research/quant-bio/rashad-matlab/algorithms/stepsize.m | false



