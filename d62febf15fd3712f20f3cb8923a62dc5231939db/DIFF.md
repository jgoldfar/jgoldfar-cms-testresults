## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/acalc/extra/non-standard.tex | true | *false*
notes/independent/papers/emdiben/notes.tex | true | *false*
work/acalc/1702/notes/week04.tex | true | *false*
research/optcont/code-docs/alt-scheme/dirichlet-scheme.tex | true | *false*
work/alg/mth0111-studentcontract.pdf | true | *false*
misc/maths/geom-sum.tex | true | *false*
research/seminar/13.9.20_seminar/thm1-derivation.tex | true | *false*
work/projects/ugr/sp13/malik/work_expanded.tex | true | *false*
research/seminar/14.09.09_seminar/talk.tex | true | *false*
grad/appstat/final.tex | true | *false*
work/projects/ugr/sp13/bpoggi/dual-spaces.tex | true | *false*
work/projects/ugr/sp13/malik/ode-opt-full.tex | true | *false*
misc/maths/ode-varpar.tex | true | *false*
grad/realvariables/hw2.tex | true | *false*
work/acalc/1702/notes/week05.tex | true | *false*
grad/sobolevspaces/hw-revised/hw5.tex | true | *false*
work/acalc/1702/notes/week02.tex | true | *false*
grad/odestheory/midterm1-q2.tex | true | *false*
grad/appstat/hw2.tex | true | *false*
grad/linear/hw3.tex | true | *false*
grad/realvariables/hw5.tex | true | *false*
grad/sobolevspaces/hw-revised/test1.tex | true | *false*
misc/not_mine/ia_tenali/ia-fa12-hw.tex | true | *false*
notes/independent/book/holton/main.tex | true | *false*
work/acalc/1603/calendar.tex | true | *false*
grad/algebra/hw3.tex | true | *false*
grad/odestheory/nl-improper.tex | true | *false*
research/seminar/14.01.27_seminar-part_a.tex | true | *false*
work/alg/mth0111-test-policies.pdf | true | *false*
notes/independent/book/johnson-FEM.tex | true | *false*
grad/sobolevspaces/hw5.tex | true | *false*
work/Models/test2.tex | true | *false*
grad/apdes/su11/final.pdf | true | *false*
work/alg/curr/MTH0111-calendar.tex | true | *false*
notes/algebra.tex | true | *false*
notes/dynmet.tex | true | *false*
research/optcont/code-docs/alt-scheme/robin-scheme.tex | true | *false*
grad/dissertation/other/comp/comp-comm-hours.tex | true | *false*
work/acalc/1702/notes/week13.tex | true | *false*
research/seminar/13.9.20_seminar/fit-talk.tex | true | *false*
grad/pdes/exercises.tex | true | *false*
work/acalc/calendar-test-com.tex | true | *false*
notes/redo/linear.tex | true | *false*
work/tutoring/zachary/zachary_220412.tex | true | *false*
misc/not_mine/intro_analysis_sp12.tex | true | *false*
work/Calc2/Su10/pfd.tex | true | *false*
work/projects/typing/nadiyah/nadiyah_equations.tex | true | *false*
research/optcont/code-docs/alt-scheme/alt-timediscrete-sep.tex | true | *false*
research/seminar/11.9.16_seminar.tex | true | *false*
notes/independent/leibniz_rule.tex | true | *false*
grad/algebra/hw2.tex | true | *false*
grad/odestheory/midterm1.tex | true | *false*
grad/appstat/hw6.tex | true | *false*
grad/complex/hw1.tex | true | *false*
misc/maths/trigidentities.tex | true | *false*
notes/independent/book/rudin-principles_of_math_analysis.tex | true | *false*
notes/independent/papers/abdulla_king_00.tex | true | *false*
grad/sobolevspaces/exercises.tex | true | *false*
grad/sobolevspaces/hw2.tex | true | *false*
work/tutoring/zachary/zachary_071212.tex | true | *false*
grad/complex/hw5.tex | true | *false*
grad/realvariables/final_exam.tex | true | *false*
misc/maths/prop_firstorder.tex | true | *false*
grad/mathmethods/ex6.tex | true | *false*
work/projects/ugr/sp13/bpoggi/paper-linfinity.tex | true | *false*
grad/sobolevspaces/hw-revised/final.tex | true | *false*
grad/realvariables/hw3.tex | true | *false*
grad/mathmethods/ex5.tex | true | *false*
grad/algebra/hw4.tex | true | *false*
misc/maths/fact-est.tex | true | *false*
grad/algebra/hw6.tex | true | *false*
notes/introa.tex | true | *false*
work/acalc/1603/notes/all.tex | true | *false*
grad/dissertation/plans.pdf | true | *false*
notes/numerical.tex | true | *false*
grad/introa/hw/ia_test2.tex | true | *false*
research/seminar/14.09.09_seminar/talk-pictures.tex | true | *false*
misc/maths/math-ed.tex | true | *false*
grad/mathmethods/mth6050-hw8.tex | true | *false*
grad/mathmethods/ec2.tex | true | *false*
grad/appstat/hw7.tex | true | *false*
work/acalc/1702/notes/week10.tex | true | *false*
work/alg/curr/MTH1701-calendar.tex | true | *false*
notes/linear.tex | true | *false*
work/Models/test1.tex | true | *false*
grad/numerical/test3.tex | true | *false*
work/projects/ugr/sp13/malik/paper-linfinity.tex | true | *false*
work/Models/ex2.tex | true | *false*
research/seminar/11.4.25_seminar.tex | true | *false*
work/tutoring/zachary/zachary_021212.tex | true | *false*
notes/redo/stochastic.tex | true | *false*
grad/numerical/hw3.tex | true | *false*
notes/discmath.tex | true | *false*
work/acalc/1702/notes/week03.tex | true | *false*
grad/appstat/hw4.tex | true | *false*
grad/complex/test2.tex | true | *false*
grad/complex/hw2.tex | true | *false*
grad/pdes/test2.tex | true | *false*
grad/introa/final-sp13.tex | true | *false*
notes/independent/zorich-cont-func.tex | true | *false*
research/ndce/diff-scheme-weak-soln.tex | true | *false*
grad/functional/ec2.tex | true | *false*
work/projects/ugr/fa12/problem-statement.tex | true | *false*
work/tutoring/zachary/zachary_200414.tex | true | *false*
grad/appstat/hw8.tex | true | *false*
work/alg/curr/MTH0111-adv-exam.tex | true | *false*
research/seminar/11.10.28_seminar.tex | true | *false*
grad/sobolevspaces/final_corrected.tex | true | *false*
grad/realvariables/hw4.tex | true | *false*
research/seminar/12.2.10_seminar.tex | true | *false*
grad/mathmethods/ex3.tex | true | *false*
grad/odestheory/midterm2.tex | true | *false*
grad/introa/hw/ia2_sol.tex | true | *false*
grad/mathmethods/ex9.tex | true | *false*
research/ndce/self-similar.tex | true | *false*
grad/mth6300/abdulla_typed/Diff_Schemes_Convergence-Abdulla.tex | true | *false*
grad/sobolevspaces/hw1.tex | true | *false*
work/projects/ugr/sp13/malik/proj_statement_extended.tex | true | *false*
work/acalc/1702/notes/all.tex | true | *false*
notes/redo/mathstat.tex | true | *false*
grad/dissertation/other/prev/sp13/13.03.07_meeting.tex | true | *false*
misc/maths/law-maths.tex | true | *false*
research/optcont/misc-notes/minimization-ua.tex | true | *false*
work/Models/ex3.tex | true | *false*
grad/algebra/hw8.tex | true | *false*
work/Models/ex1.tex | true | *false*
research/misc/fa14-npde-course/notes-dbl-deg.pdf | true | *false*
notes/independent/book/friedman-pde-parabolic.tex | true | *false*
work/acalc/1702/notes/week06.tex | true | *false*
work/acalc/1702/notes/week01.tex | true | *false*
notes/redo/pde.tex | true | *false*
grad/numerical/test1.tex | true | *false*
work/acalc/recap.tex | true | *false*
notes/appstat.tex | true | *false*
grad/mth6300/abdulla_typed/Diff_Schemes_Convergence-Abdulla-spacing.tex | true | *false*
grad/mathmethods/ex8.tex | true | *false*
research/seminar/16.02.04_seminar.tex | true | *false*
notes/sobolevspaces.tex | true | *false*
work/projects/ugr/sp13/malik/j-lip-cond.tex | true | *false*
notes/complex/complex.tex | true | *false*
grad/realvariables/hw6.tex | true | *false*
work/alg/mth1701-studentcontract.pdf | true | *false*
notes/independent/papers/oleinik_60/oleinik_60-kirchoff-cont.tex | true | *false*
work/tutoring/zachary/zachary_240414.tex | true | *false*
research/misc/proofs.pdf | true | *false*
work/acalc/extra/limits_and_continuity.tex | true | *false*
work/misc/tex-tutorial.tex | true | *false*
grad/linear/test3.tex | true | *false*
research/seminar/14.02.03_seminar.tex | true | *false*
grad/mathmethods/ex2.tex | true | *false*
work/acalc/1702/calendar.tex | true | *false*
grad/complex/hw7.tex | true | *false*
notes/finished/apdes.tex | true | *false*
grad/algebra/hw12.tex | true | *false*
grad/algebra/extra/ex1.tex | true | *false*
grad/dissertation/other/prev/sp13/13.02.28_meeting.tex | true | *false*
misc/maths/mollif.tex | true | *false*
work/projects/ugr/sp13/malik/ode-opt.tex | true | *false*
research/seminar/10.11.17_seminar.tex | true | *false*
work/acalc/1702/notes/week12.tex | true | *false*
grad/algebra/hw1.tex | true | *false*
grad/linear/hw2.tex | true | *false*
grad/introa/hw/ia3_sol.tex | true | *false*
grad/sobolevspaces/hw-revised/hw3.tex | true | *false*
research/seminar/12.3.16_seminar.tex | true | *false*
work/acalc/1702/notes/week04-05.tex | true | *false*
grad/sobolevspaces/hw-revised/final-orig.tex | true | *false*
work/acalc/1702/notes/week14.tex | true | *false*
grad/introa/hw/ia1_sol.tex | true | *false*
research/optcont/code-docs/alt-scheme/PDE-CYL-sep.tex | true | *false*
notes/independent/papers/oleinik_60/oleinik_60-problem.tex | true | *false*
work/acalc/extra/convex_facts.tex | true | *false*
notes/independent/book/natanson-real_variables.tex | true | *false*
grad/realvariables/midterm1.tex | true | *false*
work/alg/advancement.pdf | true | *false*
grad/odestheory/midterm1-fa12.tex | true | *false*
grad/numerical/hw1.tex | true | *false*
research/optcont/code-docs/misc/neumann-rect-scheme.tex | true | *false*
work/acalc/calc-sec/all.tex | true | *false*
notes/realvariables.tex | true | *false*
research/optcont/code-docs/alt-scheme/neumann-onestep-scheme.tex | true | *false*
grad/mathmethods/background2.tex | true | *false*
notes/mathstat.tex | true | *false*
grad/functional/final.tex | true | *false*
grad/sobolevspaces/test1.tex | true | *false*
work/acalc/1702/notes/week16.tex | true | *false*
grad/functional/ec1.tex | true | *false*
research/seminar/10.10.20_seminar.tex | true | *false*
grad/numerical/hw2.tex | true | *false*
misc/not_mine/mth6330-final-fa17.tex | true | *false*
grad/odestheory/finaltest.tex | true | *false*
grad/mathmethods/background.tex | true | *false*
research/ndce/ndce_paper/fit-talk.tex | true | *false*
research/optcont/code-docs/misc/neumann-rect-soln.tex | true | *false*
grad/linear/hw1.tex | true | *false*
work/tutoring/zachary/zachary_100112.tex | true | *false*
notes/bvp.tex | true | *false*
work/Models/ch4.1.tex | true | *false*
grad/dissertation/other/prev/su12/problem-statement.tex | true | *false*
work/acalc/calendar-hw-com.tex | true | *false*
grad/complex/test1.tex | true | *false*
research/ndce/codes/alg_desc_tpl.tex | true | *false*
grad/mth6300/notes.tex | true | *false*
grad/sobolevspaces/hw4.tex | true | *false*
grad/appstat/hw9.tex | true | *false*
notes/independent/book/evans-sobolev-notes.tex | true | *false*
grad/pdes/test1.tex | true | *false*
notes/independent/misc.tex | true | *false*
notes/functional.tex | true | *false*
grad/apdes/su11/final-solns.pdf | true | *false*
misc/not_mine/mth4101-final-sp18.tex | true | *false*
work/acalc/1702/notes/week09.tex | true | *false*
work/tutoring/zachary/zachary_310314.tex | true | *false*
notes/stochastic.tex | true | *false*
research/seminar/13.9.20_seminar/fit-talk-notes.tex | true | *false*
notes/independent/papers/oleinik_60/oleinik_60.tex | true | *false*
grad/mathmethods/ex11.tex | true | *false*
research/ndce/ndce_paper/thm1-derivation.tex | true | *false*
misc/not_mine/ia_tenali/rudin-c-not-orderable.tex | true | *false*
grad/realvariables/hw9.tex | true | *false*
grad/algebra/hw.tex | true | *false*
grad/mathmethods/ex10.tex | true | *false*
work/alg/mth1701-test-policies.pdf | true | *false*
research/misc/fa14-npde-course/notes.pdf | true | *false*
grad/apdes/sp13/hw1.tex | true | *false*
grad/complex/hw8.tex | true | *false*
grad/mathmethods/ex4.tex | true | *false*
grad/algebra/hw5.tex | true | *false*
work/projects/ugr/sp13/malik/ode-opt-extended.tex | true | *false*
misc/not_mine/ia_tenali/ia-fa12-mt1.tex | true | *false*
grad/realvariables/hw1.tex | true | *false*
grad/algebra/hw11.tex | true | *false*
work/acalc/1702/notes/week07.tex | true | *false*
grad/mathmethods/ex1.tex | true | *false*
grad/algebra/test2.tex | true | *false*
grad/mathmethods/ec1.tex | true | *false*
misc/maths/complete-graph-num-nodes.tex | true | *false*
research/seminar/14.01.27_seminar.tex | true | *false*
grad/complex/hw4.tex | true | *false*
grad/sobolevspaces/hw-revised/hw4.tex | true | *false*
notes/independent/papers/goldman_03.tex | true | *false*
notes/mathmeth.tex | true | *false*
notes/odestheory.tex | true | *false*
grad/mth6300/ex1.tex | true | *false*
grad/apdes/su11/hw1.pdf | true | *false*
notes/lec-notes-tpl.tex | true | *false*
grad/linear/extra.tex | true | *false*
grad/introa/hw/convexball.tex | true | *false*
grad/pdes-sp13/final.tex | true | *false*
research/optcont/code-docs/alt-scheme/neumann-rect-implicit-scheme.tex | true | *false*
grad/complex/hw3.tex | true | *false*
grad/appstat/test1.tex | true | *false*
grad/apdes/su11/test1.pdf | true | *false*
grad/mathmethods/ex7.tex | true | *false*
work/Models/lab-2.tex | true | *false*
grad/complex/hw9.tex | true | *false*
research/seminar/10.9.15_seminar.tex | true | *false*
notes/redo/theoryodes.tex | true | *false*
grad/appstat/tpl.tex | true | *false*
notes/independent/book/herstein-abstract_algebra-exercises.tex | true | *false*
grad/appstat/hw3.tex | true | *false*
grad/functional/exercises.tex | true | *false*
grad/odestheory/finaltest-fa12.tex | true | *false*
grad/algebra/hw9.tex | true | *false*
grad/appstat/hw5.tex | true | *false*
grad/sobolevspaces/hw3.tex | true | *false*
work/acalc/1702/notes/week15.tex | true | *false*
grad/appstat/hw1.tex | true | *false*
grad/realvariables/hw7.tex | true | *false*
grad/sobolevspaces/hw-revised/hw2.tex | true | *false*
work/acalc/1702/notes/week11.tex | true | *false*
notes/models-sp09.tex | true | *false*
notes/inv-prob-sp12.tex | true | *false*
grad/algebra/hw7.tex | true | *false*
grad/complex/hw6.tex | true | *false*
grad/realvariables/hw8.tex | true | *false*
grad/algebra/hw10.tex | true | *false*
work/acalc/1702/notes/week08.tex | true | *false*
grad/mathmethods/ec1b.tex | true | *false*
work/projects/ugr/sp13/bpoggi/extremal_problems.tex | true | *false*

### New Tests
File/Test | New
----------|----
work/MAC/out-of-order.tex | true
work/Calc1/assess/testf.tex | true
research/paramid/tpl.tex | true
work/Calc1/assess/test1Solns.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
misc/env/tex-include/Templates/less-minimal-cmds.tex | false
research/talks/jmm_17/diss-paper-parts/preamble-cmds.tex | false
grad/dissertation/other/prev/sp13/meeting_snippets.tex | false
research/talks/jmm_17/diss-paper-parts/preamble-cmds-other.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
grad/appstat | true | *false*
grad/numerical | true | *false*
research/optcont/code-docs/alt-scheme | true | *false*
research/seminar | true | *false*
grad/introa/hw | true | *false*
grad/algebra | true | *false*
grad/apdes/su11 | true | *false*
work/acalc/1603 | true | *false*
grad/algebra/extra | true | *false*
grad/odestheory | true | *false*
research/misc/fa14-npde-course | true | *false*
work/acalc/1702 | true | *false*
grad/mth6300 | true | *false*
grad/functional | true | *false*
research/ndce/ndce_paper/sections | true | *false*
research/seminar/13.9.20_seminar | true | *false*
grad/apdes/sp13 | true | *false*
grad/pdes-sp13 | true | *false*
notes | true | *false*
notes/redo | true | *false*
grad/mathmethods | true | *false*
grad/linear | true | *false*
research/optcont/misc-notes | true | *false*
notes/finished | true | *false*
grad/introa | true | *false*
grad/sobolevspaces/hw-revised | true | *false*
research/optcont/code-docs/misc | true | *false*
notes/complex | true | *false*
grad/complex | true | *false*
research/seminar/14.09.09_seminar | true | *false*
research/ndce/ndce_paper | true | *false*
notes/independent/papers/oleinik_60 | true | *false*
work/alg | true | *false*
grad/realvariables | true | *false*
grad/pdes | true | *false*
research/ndce | true | *false*
research/optcont/code-docs | true | *false*
misc/not_mine | true | *false*
misc/maths | true | *false*

### New Tests
File/Test | New
----------|----
research/ndce/codes/gen-python | false



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
research/chem/README | false | *true*

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




