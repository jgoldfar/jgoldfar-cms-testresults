## PDF Files Compile

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/projects/abdulla/CalcVarPDEOptimalStefanProblem.tex | true
work/projects/typing/jrivera-duhamel/main.tex | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/talks/jmm_17/diff-paper-parts/more-weak-solution-defn-eqn.tex | false
research/talks/jmm_17/diff-paper-parts/j-well-defined-result.tex | false
research/ndce/codes/gen-python/doc/images/barenblatt-profile.tex | false
research/talks/jmm_17/diff-paper-parts/more-weak-solution-defn-change.tex | false
work/projects/blog-drafts/tikz-standalone-preamble.tex | false
research/optcont/papers/nd-paper/verif/utsq-simple.tex | false
research/talks/jmm_17/diff-paper-parts/more-weak-solution-defn.tex | false
grad/dissertation/proposal/parts/heuristic-fullstate-deriv.tex | false
research/talks/jmm_17/diff-paper-parts/adjoint-exists-s.tex | false
research/talks/jmm_17/diff-paper-parts/notation-l2omega.tex | false
research/optcont/papers/nd-paper/verif/aijkl-v-deriv.tex | false
grad/dissertation/proposal/parts/heuristic-state-deriv.tex | false
grad/dissertation/proposal/parts/weaksoln-deriv.tex | false
research/talks/jmm_17/diff-paper-parts/preamble-defn-macros.tex | false



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/nd-paper | false | *true*
work/projects/abdulla | false | *true*
research/ndce/codes/gen-python/doc | false | *true*
research/optcont/papers/nd-paper/common | false | *true*
grad/dissertation | false | *true*
grad/numpde | false | *true*

### New Tests
File/Test | New
----------|----
work/projects/typing/jrivera-duhamel | true



## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### New Tests
File/Test | New
----------|----
work/projects/typing/jrivera-duhamel/LICENSE | false
work/projects/typing/jrivera-duhamel/README | false



## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




