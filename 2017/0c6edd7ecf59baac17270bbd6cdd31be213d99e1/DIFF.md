## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/APDE/hw/hw4.pdf | true | *false* |
| research/optcont/papers/isp1fd/REU2014_ISP_paper.tex | true | *false* |
| work/APDE/hw/hw13.pdf | true | *false* |
| work/APDE/misc/heat_eq-vs-wave_eq.pdf | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/nd-paper | false | *true* |
| research/optcont/papers/conv-a-paper | false | *true* |
| research/optcont/papers/nd-paper/common | false | *true* |
| research/optcont/papers/conv-a-paper/paper-parts | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| misc/julia/CMSTest | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




