## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| grad/mth6230 | false | 
| work/projects/typing | false | 
| work/Calc2/Su10 | true | 
| work/alg/curr/1701-lec | false | 
| grad/appstat | false | 
| grad/numerical | true | 
| research/optcont/code-docs/alt-scheme | false | 
| research/chaos/feigenbaum/jl/doc | false | 
| research/chaos/parameter-values/abdulla2013 | true | 
| ugrad/bvp | true | 
| ugrad/dynmet2 | false | 
| research/seminar | false | 
| grad/introa/hw | false | 
| research/talks/ams_sec_sep17 | false | 
| work/MAC | false | 
| research/optcont/papers/diff-paper | true | 
| ugrad/poa | false | 
| work/misc | true | 
| research/optcont/papers/isp1fd | false | 
| research/optcont/papers/isp-constrained | false | 
| work/training/latex/BlankProj | true | 
| work/alg/review/poly | false | 
| research/optcont/papers/nd-paper | false | 
| work/alg/review/cart | false | 
| work/projects/ugr/fa13/laite | true | 
| research/optcont/papers/conv-a-paper/common | false | 
| grad/dissertation/other/comp/comp-q-inc | false | 
| grad/algebra | false | 
| ugrad/fluids_lab | false | 
| ugrad/physoc | false | 
| work/training/maxima | true | 
| grad/apdes/su11 | false | 
| work/projects/useR | false | 
| work/acalc/1603 | false | 
| work/alg/review/1701-sec | false | 
| work/projects/ugr/fa12 | false | 
| grad/algebra/extra | false | 
| grad/stochastic | false | 
| misc/julia/SPS/doc | false | 
| ugrad/meteorology | false | 
| work/acalc/calc-sec | true | 
| grad/seminar/optcont | true | 
| work/matlab-workshop/materials/Advanced/NPDE | true | 
| grad/mathstat/hw | false | 
| work/projects/ugr/fa13 | true | 
| work/Calc3/quizzes | false | 
| work/siam-chapter/ayr12 | false | 
| work/DiffEq/hw-gr | true | 
| grad/odestheory | false | 
| research/misc/fa14-npde-course | false | 
| ugrad/civ_1 | false | 
| work/AlgebraReadingGroup | true | 
| research/talks/siam_an12/figures | true | 
| ugrad/atmenv | false | 
| research/talks/siam_seas14 | false | 
| work/misc/refletter | true | 
| work/acalc/1702 | false | 
| work/siam-chapter/summer_school/2013 | false | 
| work/projects/abdulla/NSF-2015 | false | 
| research/optcont/papers/nd-paper/paper-parts | true | 
| research/optcont/papers/conv-paper/disc-paper-parts | false | 
| work/Models | true | 
| grad/seminar/isp | false | 
| work/acalc/1702/notes | true | 
| research/ndce/reu-paper | false | 
| work/siam-chapter/ayr13/reading-group | false | 
| work/tutoring | false | 
| research/talks/jmm_15a | false | 
| research/talks/nsf_grfp | false | 
| work/GrpProj/Calc3/2 | false | 
| misc/julia/CMSTestTest | true | 
| work/APDE/advanced | false | 
| ugrad/models | false | 
| work/matlab-workshop/13.8.31-combined | true | 
| work/projects/abdulla | false | 
| grad/mth6300 | false | 
| research/seminar/minilec | false | 
| notes/independent/papers/solonnikov64 | false | 
| research/optcont/papers/conv-a-paper | false | 
| work/projects/dsct-laserday | false | 
| work/projects/ugr/sp13/bpoggi | false | 
| research/seminar/parts | false | 
| work/alg/review/trig | true | 
| grad/functional | false | 
| work/Calc1/syl | false | 
| work/alg/review/rev | false | 
| work/alg/curr/0111-lec | false | 
| research/ndce/ndce_paper/sections | false | 
| ugrad/mesomet | true | 
| work/training/VCS | true | 
| work/tutoring/zachary | false | 
| research/seminar/13.9.20_seminar | false | 
| notes/independent/book/lsu | false | 
| research/talks/jmm_16 | false | 
| research/optcont/papers/MISP-diff | false | 
| research/optcont/papers/conv-paper/semidisc-paper-parts | false | 
| work/Calc1/Sp10 | false | 
| grad/apdes/sp13 | false | 
| research/seminar/bvp_of_math_phys | true | 
| research/talks/siam_pd13 | false | 
| grad/pdes-sp13 | false | 
| work/projects/ugr/docs/ode | false | 
| research/optcont/papers/nd-paper/common | false | 
| work/Calc1/labs_extended | false | 
| notes | false | 
| notes/independent/book/besov | false | 
| notes/redo | false | 
| ugrad/fluids | false | 
| ugrad/models/final | false | 
| grad/dissertation | false | 
| research/talks/jmm_15 | false | 
| work/acalc/1702/perweek | true | 
| research/chaos | false | 
| grad/mathmethods | false | 
| notes/independent/book | false | 
| research/ndce/ndce_paper/figures/tex | true | 
| work/APDE/hw | false | 
| research/chaos/parameter-values | true | 
| grad/linear | false | 
| research/optcont/misc-notes | false | 
| research/optcont/MCode-Constrained | false | 
| grad/mth6300/abdulla_typed/YMC-2012 | false | 
| grad/mathstat | true | 
| ugrad/misc | false | 
| notes/independent | true | 
| work/projects/typing/fulton-siam-2016 | false | 
| notes/finished | false | 
| work/common | true | 
| work/matlab-workshop/feedback | true | 
| grad/introa | false | 
| work/training/julia | true | 
| research/talks/jmm_15b | false | 
| misc/env/tex-include/Templates | true | 
| grad/sobolevspaces/hw-revised | false | 
| research/optcont/code-docs/misc | false | 
| ugrad/hydro | false | 
| ugrad/dynmet | true | 
| research/misc/proof-secs | true | 
| ugrad/remote | false | 
| work/NumericalMethodsBook | true | 
| grad/numpde | true | 
| work/Calc1/assess | false | 
| notes/complex | true | 
| work/siam-chapter/summer_school/notes | false | 
| work/alg/curr/1012-lec | false | 
| notes/independent/papers/emdiben | false | 
| ugrad/civ_2 | false | 
| research/optcont/papers/conv-paper | false | 
| research/seminar/13.9.20_seminar/sections | false | 
| research/ndce/codes/cnps | false | 
| grad/complex | false | 
| work/alg/review | false | 
| research/seminar/14.09.09_seminar | false | 
| misc/julia/CMSTest/ex/TeXMakefile | true | 
| work/GrpProj/Calc3/3 | false | 
| research/seminar/FA_inf_calc | true | 
| work/Calc1/labs | false | 
| work/siam-chapter/ayr13 | false | 
| work/GrpProj/Calc3/4 | false | 
| work/projects/ugr/sp12/nmertins | true | 
| research/ndce/ndce_paper | false | 
| work/siam-chapter/summer_school/2013/yrnotes | false | 
| ugrad/atmpolllab | false | 
| research/numpde | false | 
| research/misc | true | 
| work/alg/review/exp-log | false | 
| work/Calc1/Fa09 | false | 
| ugrad/synmet1 | true | 
| research/talks/jmm_17 | false | 
| notes/independent/papers/oleinik_60 | false | 
| work/projects/ugr/sp13/malik | false | 
| work/alg | false | 
| ugrad/envsatsys | false | 
| misc/env/tex-include/Templates/More | true | 
| research/optcont/papers/conv-a-paper/paper-parts | false | 
| grad/realvariables | false | 
| notes/independent/book/holton | false | 
| work/alg/planning | false | 
| grad/seminar/dsct | false | 
| misc/julia/CMSTest | true | 
| research/talks/siam_an12 | true | 
| notes/independent/book/goldman | false | 
| work/ProbStat | true | 
| work/APDE/hw/exercises | false | 
| grad/mth6300/abdulla_typed/AIMS-2012 | false | 
| grad/pdes | false | 
| research/ndce | true | 
| work/DiffEq/quiz | true | 
| research/optcont/papers/conv-paper/common | false | 
| research/ndce/codes/emdiben | false | 
| work/alg/curr/1011-lec | false | 
| work/GrpProj/Calc3/1 | true | 
| work/alg/review/function | false | 
| research/seminar/13.9.20_seminar/figures/tex | false | 
| work/alg/review/0111-sec | false | 
| work/DiffEq/proj | true | 
| work/training/latex | false | 
| work/siam-chapter/summer_school | false | 
| ugrad/thermo | true | 
| work/acalc/1603/notes | true | 
| research/optcont/code-docs | false | 
| notes/independent/papers | false | 
| misc/maths | false | 
| work/siam-chapter | false | 
| research/optcont/ODE | false | 
| work/alg/review/linsys | false | 
| work/acalc/extra | false | 
| work/GrpProj/DiffEq | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




