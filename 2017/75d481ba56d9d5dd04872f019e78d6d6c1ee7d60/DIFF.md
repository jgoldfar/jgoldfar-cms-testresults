## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/review/ex-solns.tex | true | *false* |
| work/alg/review/sel-proofs.tex | true | *false* |
| work/APDE/hw/hw4.pdf | true | *false* |
| work/APDE/hw/hw13.pdf | true | *false* |
| work/alg/review/prereq.tex | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/alg/curr/1701-lec | true | *false* |
| work/alg/review | true | *false* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| work/MAC/blog/README | false | *true* |
| work/training/latex/BlankProj/LICENSE | false | *true* |
| work/projects/swiper/README | false | *true* |
| misc/explore/julia/refs/README | false | *true* |
| work/MAC/report/README | false | *true* |
| work/MAC/report/LICENSE | false | *true* |
| research/general-cs/LICENSE | false | *true* |
| work/MAC/blog/LICENSE | false | *true* |
| misc/explore/LICENSE | false | *true* |
| research/general-cs/README | false | *true* |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




