## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/projects/reu-admin/notes/npde/report/fdiff-slides.tex | false | *true* |
| work/projects/reu-admin/notes/npde/report/old-pres.tex | false | *true* |
| research/ndce/codes/emdiben/alg_desc.tex | false | *true* |
| work/alg/curr/1701-lec/all.tex | false | *true* |
| work/APDE/misc/heat_eq-vs-wave_eq.pdf | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




