## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/not_mine/Math765/assignment3.tex | false | *true* |
| misc/not_mine/Math765/assignment2.tex | false | *true* |
| misc/not_mine/Math765/assignment1.tex | false | *true* |
| work/misc/refletter/dmccormick.tex | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




