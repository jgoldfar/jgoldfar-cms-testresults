## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/APDE/hw/hw4.pdf | false | *true* |
| work/alg/curr/1701-lec/all.tex | false | *true* |
| work/APDE/hw/hw13.pdf | false | *true* |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| work/alg/review/ex-solns.tex | false |
| work/alg/review/sel-proofs.tex | false |
| work/alg/review/prereq.tex | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/siam-chapter/ayr12 | false | *true* |
| work/Calc1/syl | true | *false* |
| work/siam-chapter | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




