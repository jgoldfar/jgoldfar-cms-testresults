## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| research/misc/fa14-npde-course/notes.tex | false | *true* |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| work/siam-chapter/ayr12/minutes-10.19.12-actionitems.tex | false |
| work/projects/abdulla/digraphs/digraph1-2km1-1.tex | false |
| work/siam-chapter/ayr12/minutes-9.6.12-actionitems.tex | false |
| work/APDE/hw/hw13-2c-2.tex | false |
| work/siam-chapter/ayr12/minutes-10.19.12-attendance.tex | false |
| work/APDE/hw/hw13-2b-2.tex | false |
| work/APDE/hw/hw13-2b-1.tex | false |
| work/siam-chapter/ayr12/minutes-9.6.12-attendance.tex | false |
| work/APDE/hw/hw13-2c-1.tex | false |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




