## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### New Tests
| File/Test | New |
|-----------------|
| /Users/jgoldfar/Documents/research/chaos/DSCT-paper.tex | true | 
| /Users/jgoldfar/Documents/research/chaos/feigenbaum/jl/doc/logmap-conv.tex | true | 
| /Users/jgoldfar/Documents/work/MAC/seminar-closure.tex | true | 
| /Users/jgoldfar/Documents/notes/stochastic.tex | true | 
| /Users/jgoldfar/Documents/research/chaos/feigenbaum/jl/doc/logmap-polyroot.tex | true | 
| /Users/jgoldfar/Documents/research/chaos/Feigenbaum-Frechet.tex | true | 
| /Users/jgoldfar/Documents/work/MAC/OfficeHours.tex | true | 
| /Users/jgoldfar/Documents/notes/redo/stochastic.tex | true | 
|-----------------|
*****

## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

