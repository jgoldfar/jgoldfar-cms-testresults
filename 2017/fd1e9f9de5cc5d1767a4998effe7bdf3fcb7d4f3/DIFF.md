## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/MISP-diff/main | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/chaos/feigenbaum/jl/doc | false | *true* |
| research/optcont/papers/isp1fd | false | *true* |
| research/optcont/papers/conv-a-paper/common | false | *true* |
| research/misc/fa14-npde-course | false | *true* |
| research/optcont/papers/conv-paper/disc-paper-parts | false | *true* |
| research/optcont/papers/MISP-diff | false | *true* |
| research/optcont/papers/conv-paper/semidisc-paper-parts | false | *true* |
| work/projects/ugr/docs/ode | false | *true* |
| research/talks/jmm_15 | false | *true* |
| research/optcont/code-docs/misc | false | *true* |
| research/talks/jmm_17 | false | *true* |
| research/optcont/papers/conv-paper/common | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




