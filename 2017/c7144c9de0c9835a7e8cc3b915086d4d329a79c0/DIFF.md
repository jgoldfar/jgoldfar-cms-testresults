## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/misc-notes/disc-isp-nondiff.tex | true | 
| misc/not_mine/Math765/assignment2.tex | false | 
| misc/not_mine/Math765/assignment3.tex | false | 
| misc/not_mine/Math765/assignment1.tex | false | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




