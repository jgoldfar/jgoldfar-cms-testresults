## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/resume/cv.pdf | true | *false* |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/research_summary/main.tex | true | 
| research/optcont/papers/gen-constrained/main.tex | true | 
| research/seminar/ISP_convergence_talk/main.tex | false | 
| research/optcont/papers/gen-constrained/presentation.tex | false | 
| research/optcont/papers/gen-constrained/REG-Update.tex | false | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/MCode-Constrained | true | *false* |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/research_summary | false | 
| research/ndce/codes/gen-python | true | 
| research/optcont/papers/gen-constrained | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/research_summary/LICENSE | true | 
| research/optcont/papers/gen-constrained/README | true | 
| research/seminar/ISP_convergence_talk/README | true | 
| research/seminar/ISP_convergence_talk/LICENSE | true | 
| research/optcont/papers/gen-constrained/LICENSE | false | 
| research/talks/research_summary/README | true | 



## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




