## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| misc/resume/cv.pdf | false | *true* |
| research/seminar/ISP_convergence_talk/main.tex | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/siam_pd17/talk/diff-paper-parts/more-weak-solution-defn-eqn.tex | false | 
| research/talks/siam_pd17/talk/diff-paper-parts/more-weak-solution-defn-change.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/energy-est-2-cons-s.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-2-conv-2-s.tex | false | 
| research/talks/siam_pd17/talk/diff-paper-parts/preamble-defn-macros.tex | false | 
| research/talks/siam_pd17/talk/diff-paper-parts/more-weak-solution-defn.tex | false | 
| research/talks/siam_pd17/talk/diff-paper-parts/adjoint-exists-s.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-1-s.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-2-conv-1-s.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-2-conv-3-s.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/energy-est-2-s.tex | false | 
| research/talks/siam_pd17/talk/diff-paper-parts/j-well-defined-result.tex | false | 
| research/seminar/ISP_convergence_talk/disc-paper-parts/energy-est-1-cons-1-s.tex | false | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/seminar/ISP_convergence_talk | false | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




