## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/APDE/hw/hw4.pdf | true | *false* |
| work/APDE/hw/hw13.pdf | true | *false* |
| research/misc/fa14-npde-course/notes.pdf | true | *false* |
| work/APDE/misc/heat_eq-vs-wave_eq.pdf | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/diff-paper | true | *false* |
| research/talks/siam_seas14 | false | *true* |
| work/siam-chapter/summer_school | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




