## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/review/ex-solns.tex | true | *false* |
| work/alg/curr/1012-lec/all.tex | true | *false* |
| work/alg/curr/0111-lec/review.tex | true | *false* |
| work/alg/curr/0111-lec/review-ch1.tex | true | *false* |
| work/APDE/hw/hw4.pdf | false | *true* |
| work/alg/curr/1701-lec/all.tex | true | *false* |
| work/alg/curr/0111-lec/all.tex | true | *false* |
| work/training/maxima/main.tex | false | *true* |
| work/APDE/hw/hw13.pdf | false | *true* |
| work/alg/curr/1701-lec/review-ch1-0111-also.tex | true | *false* |
| work/alg/curr/0111-lec/week3.tex | true | *false* |
| work/alg/curr/0111-lec/review-ch1-1701-also.tex | true | *false* |
| work/alg/curr/1011-lec/all.tex | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/alg/curr/1701-lec | false | *true* |
| work/MAC | false | *true* |
| work/alg/review/poly | false | *true* |
| work/alg/review/cart | false | *true* |
| work/projects/useR | false | *true* |
| work/projects/ugr/fa12 | false | *true* |
| work/Calc3/quizzes | false | *true* |
| research/talks/nsf_grfp | false | *true* |
| work/GrpProj/Calc3/2 | false | *true* |
| work/projects/dsct-laserday | false | *true* |
| work/projects/ugr/sp13/bpoggi | false | *true* |
| work/Calc1/syl | false | *true* |
| work/alg/review/rev | false | *true* |
| ugrad/remote | false | *true* |
| work/Calc1/assess | false | *true* |
| research/ndce/codes/cnps | false | *true* |
| work/alg/review | false | *true* |
| work/alg/review/exp-log | false | *true* |
| work/projects/ugr/sp13/malik | false | *true* |
| work/alg/planning | false | *true* |
| research/ndce/codes/emdiben | false | *true* |
| work/alg/review/function | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




