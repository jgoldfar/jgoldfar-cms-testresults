## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/APDE/hw/hw4.pdf | false | *true* |
| work/APDE/hw/hw13.pdf | false | *true* |
| research/misc/fa14-npde-course/notes.pdf | false | *true* |
| work/APDE/misc/heat_eq-vs-wave_eq.pdf | false | *true* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/diff-paper | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




