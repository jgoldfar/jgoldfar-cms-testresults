## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/Calc3/quizzes/Quiz3withSolution.tex | false | *true* |
| work/APDE/hw/hw4.pdf | false | *true* |
| research/talks/jmm_15a/talk.tex | false | *true* |
| work/training/maxima/main.tex | false | *true* |
| work/APDE/hw/hw13.pdf | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| ugrad/meteorology/Quiz11.tex | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/optcont/code-docs/tests.pdf | true |
| research/optcont/code-docs/fwd-pde.pdf | false |
| research/optcont/code-docs/opt-cont.pdf | true |
| research/optcont/code-docs/model-probs.pdf | false |
| research/optcont/code-docs/fwd-ode.pdf | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/talks/jmm_15a | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




