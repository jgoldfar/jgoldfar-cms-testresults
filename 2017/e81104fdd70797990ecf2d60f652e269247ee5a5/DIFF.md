## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/APDE/hw/hw4.pdf | false | *true* |
| work/alg/curr/1701-lec/all.tex | false | *true* |
| work/training/maxima/main.tex | false | *true* |
| research/ndce/reu-paper/2015-main.tex | false | *true* |
| work/APDE/hw/hw13.pdf | false | *true* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/alg/curr/1012-lec | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




