## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/review/ex-solns.tex | false | *true* |
| work/alg/curr/0111-lec/review.tex | false | *true* |
| work/alg/curr/0111-lec/review-ch1.tex | false | *true* |
| work/alg/curr/1701-lec/all.tex | true | *false* |
| work/alg/curr/0111-lec/all.tex | false | *true* |
| work/alg/curr/1701-lec/review-ch1-0111-also.tex | false | *true* |
| work/alg/curr/0111-lec/week3.tex | false | *true* |
| work/alg/curr/0111-lec/review-ch1-1701-also.tex | false | *true* |
| work/alg/curr/1011-lec/all.tex | false | *true* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




