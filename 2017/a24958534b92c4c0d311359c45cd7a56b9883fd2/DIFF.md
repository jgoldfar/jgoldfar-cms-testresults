## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/acalc/calc-sec/all.tex | false | *true* |
| work/alg/curr/MTH0111-syllabus.tex | false | *true* |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/optcont/JCode/ISP.jl/num/osc2015-03-09/collected.tex | false |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




