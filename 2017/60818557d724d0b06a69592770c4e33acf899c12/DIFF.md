## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/siam_pd17/talk/main.tex | true | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/siam_pd17/talk | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| research/talks/siam_pd17/README | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/siam_pd17/talk/LICENSE | true | 
| work/MAC/LICENSE | true | 
| research/talks/siam_pd17/talk/README | true | 
| work/MAC/studentList/LICENSE | true | 
| work/MAC/studentList/README | true | 
| work/MAC/README | true | 



## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




