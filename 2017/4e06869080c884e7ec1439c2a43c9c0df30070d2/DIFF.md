## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/projects/reu-admin/notes/isp/report/first-energy-est-conv.tex | false | *true* |
| work/projects/conf-gen/mealtickets/mealtickets-xelatex-nonumber.tex | false | *true* |
| work/projects/reu-admin/notes/isp/report/current-slides.tex | false | *true* |
| work/projects/reu-admin/notes/isp/report/main-presentation.tex | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| work/APDE/hw/hw8.pdf | true | 
| work/APDE/hw/hw12_4a-yeq.pdf | true | 
| work/APDE/hw/hw7.pdf | true | 
| work/APDE/hw/hw15.pdf | true | 
| work/APDE/hw/hw10.pdf | true | 
| work/APDE/hw/hw4.pdf | true | 
| work/APDE/hw/hw2.pdf | true | 
| work/APDE/hw/hw3.pdf | true | 
| work/APDE/hw/hw1.pdf | true | 
| work/APDE/hw/hw13.pdf | true | 
| work/APDE/hw/hw12_9b.pdf | true | 
| work/APDE/hw/hw9.pdf | true | 
| work/APDE/hw/hw12.pdf | true | 
| work/APDE/hw/hw8-3b-part.pdf | true | 
| work/APDE/hw/hw2-s1.5-p6-explanation.pdf | true | 
| work/APDE/hw/hw5.pdf | true | 
| work/APDE/hw/hw14.pdf | true | 
| work/APDE/hw/hw6-intbyparts.pdf | true | 
| work/APDE/hw/hw11.pdf | true | 
| work/APDE/hw/hw7-p4-odes.pdf | true | 
| work/APDE/hw/hw6.pdf | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| work/APDE/hw/hw13.tex | false |
| work/APDE/hw/hw11.tex | true |
| work/APDE/hw/hw12_4a-yeq.tex | true |
| work/APDE/hw/hw4.tex | true |
| work/APDE/hw/hw7-p4-odes.tex | true |
| work/APDE/hw/hw7.tex | true |
| work/APDE/hw/hwn.tex | true |
| work/APDE/hw/hw14.tex | true |
| work/APDE/hw/hw1.tex | true |
| work/APDE/hw/hw2-s1.5-p6-explanation.tex | true |
| work/APDE/hw/hw10.tex | true |
| work/APDE/hw/hw5.tex | true |
| work/APDE/hw/hw6.tex | true |
| work/APDE/hw/hw8-3b-part.tex | true |
| work/APDE/hw/hw8.tex | true |
| work/APDE/hw/hw12_9b.tex | true |
| work/APDE/hw/hw3.tex | true |
| work/APDE/hw/hw9.tex | true |
| work/APDE/hw/hw12.tex | true |
| work/APDE/hw/hw15.tex | true |
| work/APDE/hw/hw2.tex | true |
| work/APDE/hw/hw6-intbyparts.tex | true |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




