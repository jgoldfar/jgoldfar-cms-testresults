## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/papers/diff-c-paper/heuristicDerivation.tex | false | 
| research/optcont/papers/diff-c-paper/variationalFormulation.tex | false | 
| research/optcont/papers/diff-c-paper/main.tex | true | 
| research/optcont/papers/diff-c-paper/resultStatement.tex | false | 
| research/optcont/papers/diff-c-paper/notation.tex | false | 
| research/optcont/MCode-Constrained/+model_fns/prob5.tex | false | 
| research/optcont/MCode-Constrained/+model_fns/prob4.tex | false | 
| research/optcont/MCode-Constrained/+model_fns/prob2.tex | false | 
| research/optcont/MCode-Constrained/+model_fns/prob6.tex | false | 
| research/optcont/MCode-Constrained/+model_fns/prob3.tex | false | 
| research/optcont/papers/diff-c-paper/prelims.tex | false | 
| research/optcont/MCode-Constrained/+model_fns/prob1.tex | false | 
| research/optcont/papers/diff-c-paper/differentiability-proof.tex | false | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/optcont/papers/diff-c-paper/Frechet1.tex | true |
| research/optcont/papers/diff-c-paper/EECT-2017_1.tex | true |
| research/optcont/papers/diff-c-paper/main-full.tex | true |
| research/optcont/papers/diff-c-paper/YMCposter.tex | true |
| research/optcont/papers/diff-c-paper/lemmas.tex | true |
| research/optcont/papers/diff-c-paper/main-paper.tex | true |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/MCode-Constrained | true | *false* |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/papers/diff-c-paper | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




