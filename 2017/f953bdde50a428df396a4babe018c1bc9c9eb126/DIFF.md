## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-2-conv-2-s.tex | false |
| research/seminar/ISP_convergence_talk/disc-paper-parts/energy-est-2-cons-s.tex | false |
| research/seminar/ISP_convergence_talk/disc-paper-parts/energy-est-1-cons-1-s.tex | false |
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-1-s.tex | false |
| research/talks/siam_pd17/talk/diff-paper-parts/more-weak-solution-defn.tex | false |
| research/talks/siam_pd17/talk/diff-paper-parts/more-weak-solution-defn-eqn.tex | false |
| research/talks/siam_pd17/talk/diff-paper-parts/preamble-defn-macros.tex | false |
| research/talks/siam_pd17/talk/diff-paper-parts/adjoint-exists-s.tex | false |
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-2-conv-1-s.tex | false |
| research/talks/siam_pd17/talk/diff-paper-parts/more-weak-solution-defn-change.tex | false |
| research/seminar/ISP_convergence_talk/disc-paper-parts/main-res-2-conv-3-s.tex | false |
| research/seminar/ISP_convergence_talk/disc-paper-parts/energy-est-2-s.tex | false |
| research/talks/siam_pd17/talk/diff-paper-parts/j-well-defined-result.tex | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/talks/research_summary | false | *true* |
| research/seminar/ISP_convergence_talk | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| grad/numerical/codes/newt_e4.m | false | *true* |
| grad/numerical/codes/hw4solve.m | false | *true* |
| grad/numerical/codes/power_mth.m | false | *true* |
| grad/numerical/codes/newt.m | false | *true* |
| grad/numerical/codes/interp_quad.m | false | *true* |
| grad/numerical/codes/regulafalsi.m | false | *true* |
| grad/numerical/codes/rutis_lu.m | false | *true* |
| grad/numerical/codes/ap_quad_optimal.m | false | *true* |
| grad/numerical/codes/hw1p1solve.m | false | *true* |
| grad/numerical/codes/interp_linear.m | false | *true* |
| grad/numerical/codes/log1px.m | false | *true* |
| grad/numerical/codes/jacobi_iter.m | false | *true* |
| grad/numerical/codes/quad_mc_1d.m | false | *true* |

### No New Tests




