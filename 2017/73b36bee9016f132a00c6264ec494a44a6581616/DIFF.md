## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/Calc3/exam3/Exam3Solutions.tex | false | *true* |
| work/APDE/hw/hw4.pdf | false | *true* |
| research/optcont/papers/isp1fd/REU2014_ISP_paper.tex | false | *true* |
| work/siam-chapter/summer_school/flyer-template.tex | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/papers/MISP-diff/main.pdf | false | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/optcont/papers/MISP-diff/main | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




