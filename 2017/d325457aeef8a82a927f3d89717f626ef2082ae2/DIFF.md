## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/APDE/hw/hw4.pdf | true | *false* |
| ugrad/atmpolllab/hw1.tex | false | *true* |
| work/training/maxima/main.tex | true | *false* |
| work/APDE/hw/hw13.pdf | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




