## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/projects/typing | false | *true* |
| work/alg/curr/1701-lec | false | *true* |
| work/projects/abdulla | false | *true* |
| work/projects/typing/fulton-siam-2016 | false | *true* |
| research/optcont/code-docs | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




