## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| grad/dissertation/res-num/collected-indep.tex | false | *true* |
| grad/dissertation/talk/main.tex | false | *true* |
|-----------------------|
### No New Tests

*****

## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| grad/dissertation/other/comp/comp-q-inc/nan/code/+lin/jacobi_test.m | false | *true* |
| grad/dissertation/other/comp/comp-q-inc/nan/code/+lin/lin_gauss_fp.m | false | *true* |
| grad/dissertation/other/comp/comp-q-inc/nan/code/+lin/lin_jacobi_fp.m | false | *true* |
| grad/dissertation/other/comp/comp-q-inc/nan/code/+lin/gauss_test.m | false | *true* |
| grad/dissertation/other/comp/comp-q-inc/nan/code/+lin/lin_gauss.m | false | *true* |
| grad/dissertation/other/comp/comp-q-inc/nan/code/+lin/lin_jacobi.m | false | *true* |
|-----------------------|
### No New Tests

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| /Users/jgoldfar/Documents/work/DiffEq/mma/Lab_1_Algebra_of_Matrices.m | false |
| /Users/jgoldfar/Documents/work/DiffEq/mma/Lab_3_ODE.m | false |
| /Users/jgoldfar/Documents/work/DiffEq/mma/Lab_2_Linear_Systems.m | false |
|-----------------|
*****

