## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| research/seminar/besov_spaces/main.tex | false | *true* |
| research/optcont/code-docs/tests.pdf | false | *true* |
| work/alg/curr/1012-lec/limits-intro-1-ex.tex | false | *true* |
| work/alg/curr/1012-lec/flashcards.tex | false | *true* |
| research/seminar/12.2.10_seminar.tex | false | *true* |
| research/talks/jmm_16/main.tex | false | *true* |
| research/seminar/14.02.03_seminar.tex | false | *true* |
| research/seminar/12.3.16_seminar.tex | false | *true* |
| work/alg/curr/1012-lec/limits-intro-1.tex | false | *true* |
| research/seminar/14.01.27_seminar.tex | false | *true* |
| research/seminar/15.09.02/main.tex | false | *true* |
|-----------------------|
### No New Tests

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| /Users/jgoldfar/Documents/research/chaos/DSCT-paper.tex | true |
| /Users/jgoldfar/Documents/research/chaos/feigenbaum/jl/doc/logmap-conv.tex | true |
| /Users/jgoldfar/Documents/notes/redo/stochastic.tex | true |
| /Users/jgoldfar/Documents/research/chaos/feigenbaum/jl/doc/logmap-polyroot.tex | true |
| /Users/jgoldfar/Documents/notes/stochastic.tex | true |
| /Users/jgoldfar/Documents/research/chaos/Feigenbaum-Frechet.tex | true |
|-----------------|
*****

## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

