## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/Calc3/exam3/Exam3Solutions.tex | true | *false* |
| work/siam-chapter/summer_school/flyer-template.tex | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/code-docs/alt-scheme | false | *true* |
| research/seminar | false | *true* |
| notes/independent/book | false | *true* |
| research/optcont/papers/conv-paper | false | *true* |
| research/seminar/13.9.20_seminar/sections | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| grad/dissertation/res-num/pde/bosc-1.nb | false |



