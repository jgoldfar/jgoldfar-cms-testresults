## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/siam-chapter/ayr12/minutes-10.19.12.tex | false | *true* |
| work/Calc3/quizzes/Quiz3makeup.tex | false | *true* |
| work/projects/conf-gen/mealtickets/mealtickets-xelatex.tex | false | *true* |
| ugrad/fp/tex/report.tex | false | *true* |
| work/Calc3/quizzes/Quiz1-01.tex | false | *true* |
| work/Calc3/133.tex | false | *true* |
| research/talks/ky_aug2011/poster.tex | false | *true* |
| work/Calc3/quizzes/Quiz5-E1.tex | false | *true* |
| work/Calc3/exam3/Problem19.tex | false | *true* |
| work/siam-chapter/ayr12/minutes-9.6.12.tex | false | *true* |
| work/projects/ugr/sp13/bpoggi/paper-linfinity.tex | false | *true* |
| work/Calc3/quizzes/Quiz4withSolution.tex | false | *true* |
| work/projects/conf-gen/accept_letter.tex | false | *true* |
| work/Calc3/quizzes/Quiz3-01.tex | false | *true* |
| work/projects/conf-gen/reg-form/reg-form.tex | false | *true* |
| work/Calc3/quizzes/Quiz6-01.tex | false | *true* |
| work/Calc3/quizzes/Quiz1-E1.tex | false | *true* |
| work/Calc3/quizzes/Quiz2-E1.tex | false | *true* |
| work/Calc3/quizzes/Quiz4Makeup.tex | false | *true* |
| work/siam-chapter/minutes-template.tex | false | *true* |
| work/Calc3/quizzes/Quiz4.tex | false | *true* |
| work/Calc3/quizzes/Quiz5-01.tex | false | *true* |
| work/Calc3/quizzes/Quiz2-01.tex | false | *true* |
| work/APDE/misc/tanxeqx.pdf | false | *true* |
| work/Calc3/quizzes/Quiz5.tex | false | *true* |
| work/Calc3/quizzes/Quiz1-02.tex | false | *true* |
| work/acalc/extra/selected_questions.tex | false | *true* |
| work/projects/abdulla/REU-2017-19.tex | false | *true* |
| work/GrpProj/Calc3/4/Project5.tex | false | *true* |
| work/acalc/extra/integrable_func.tex | false | *true* |
| ugrad/misc/climo_term.tex | false | *true* |
| work/projects/conf-gen/abs_accept_letter.tex | false | *true* |
| work/GrpProj/Calc3/4/Project4.tex | false | *true* |
| work/Calc3/quizzes/Quiz5-02.tex | false | *true* |
| research/optcont/ODE/paper-linfinity.tex | false | *true* |
| work/Calc1/labs_extended/lab11.tex | false | *true* |
| work/Calc3/quizzes/Quiz2-02.tex | false | *true* |
|-----------------------|
### New Tests
| File/Test | New |
|-----------------|
| /Users/jgoldfar/Documents/work/projects/abdulla/digraphs/digraph1-2km1-1.tex | false | 
| /Users/jgoldfar/Documents/work/Calc1/syl/calendar.pdf | true | 
| /Users/jgoldfar/Documents/work/Calc1/syl/syllabus.pdf | true | 
|-----------------|
### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| /Users/jgoldfar/Documents/work/Calc3/projectsFa16/3/Project3.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/3.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/1.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projectsSp17/1/Project1.tex | true |
| /Users/jgoldfar/Documents/work/Calc3/projectsFa16/2/Project2.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projectsSp17/4/Project5.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/3a.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/projects.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projectsSp17/4/Project4.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/5.tex | false |
| /Users/jgoldfar/Documents/work/Calc1/syl/syllabus.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projectsSp17/2/Project2.tex | false |
| /Users/jgoldfar/Documents/work/Calc1/syl/calendar.tex | true |
| /Users/jgoldfar/Documents/work/Calc3/projects/2.tex | false |
| /Users/jgoldfar/Documents/work/siam-chapter/summer_school/2013/pgflibraryvectorian.code.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/groups.tex | true |
| /Users/jgoldfar/Documents/work/Calc3/projects/4.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projectsSp17/3/Project3.tex | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/Rabbits.tex | false |
|-----------------|
*****

## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| /Users/jgoldfar/Documents/work/Calc3/projectsFa16/2/plotregion.m | false |
| /Users/jgoldfar/Documents/work/Calc3/projectsFa16/2/PlotBorders.m | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/plots.nb | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/Darts.nb | false |
| /Users/jgoldfar/Documents/work/Calc3/projects/proj3.nb | false |
|-----------------|
*****

