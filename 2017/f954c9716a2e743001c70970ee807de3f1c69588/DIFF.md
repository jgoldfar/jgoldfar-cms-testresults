## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| /Users/jgoldfar/Documents/research/misc/proofs.pdf | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| /Users/jgoldfar/Documents/research/misc/proof-secs/cauchy-seq-bounded.tex | false |
| /Users/jgoldfar/Documents/research/misc/proof-secs/signum-function-derivative.tex | false |
| /Users/jgoldfar/Documents/research/misc/proof-secs/heateq-point-source-solution.tex | false |
| /Users/jgoldfar/Documents/research/misc/proof-secs/symmetric-functions-intident.tex | false |
| /Users/jgoldfar/Documents/research/misc/proof-secs/determinant-boundary-straightening-map.tex | false |
| /Users/jgoldfar/Documents/research/misc/proofs.tex | true |
| /Users/jgoldfar/Documents/research/misc/proof-secs/tpl.tex | false |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




