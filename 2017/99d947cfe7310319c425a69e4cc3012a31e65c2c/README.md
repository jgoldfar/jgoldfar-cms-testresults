## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/review/ex-solns.tex | false | *true* |
| research/optcont/code-docs/fwd-pde.pdf | false | *true* |
| work/Calc3/exam3/Exam3Solutions.tex | false | *true* |
| research/optcont/code-docs/model-probs.pdf | false | *true* |
| work/Calc3/quizzes/Quiz3withSolution.tex | false | *true* |
| research/optcont/code-docs/fwd-ode.pdf | false | *true* |
| ugrad/misc/envsat_term.tex | false | *true* |
|-----------------------|
### New Tests
| File/Test | New |
|-----------------|
| /Users/jgoldfar/Documents/work/siam-chapter/ayr12/minutes-10.19.12-attendance.tex | false | 
| /Users/jgoldfar/Documents/work/siam-chapter/minutes-<date-short>-attendance.tex | false | 
| /Users/jgoldfar/Documents/work/siam-chapter/ayr12/minutes-10.19.12-actionitems.tex | false | 
| /Users/jgoldfar/Documents/work/siam-chapter/minutes-<date-short>-actionitems.tex | false | 
| /Users/jgoldfar/Documents/work/siam-chapter/ayr12/minutes-9.6.12-actionitems.tex | false | 
| /Users/jgoldfar/Documents/work/siam-chapter/ayr12/minutes-9.6.12-attendance.tex | false | 
|-----------------|
*****

## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |
|-----------------------|
### No New Tests

*****

