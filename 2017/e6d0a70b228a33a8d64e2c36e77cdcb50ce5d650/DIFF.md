## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/curr/1012-lec/all.tex | false | *true* |
| work/APDE/hw/hw4.pdf | true | *false* |
| work/training/maxima/main.tex | true | *false* |
| research/ndce/reu-paper/2015-main.tex | true | *false* |
| work/APDE/hw/hw13.pdf | true | *false* |
| work/siam-chapter/summer_school/flyer-template.tex | false | *true* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| grad/apdes/su11 | false | *true* |
| research/ndce/reu-paper | false | *true* |
| research/seminar/parts | false | *true* |
| research/ndce/ndce_paper/sections | false | *true* |
| notes/independent/book/lsu | false | *true* |
| notes/independent/book/besov | false | *true* |
| research/ndce/ndce_paper | false | *true* |
| research/optcont/ODE | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




