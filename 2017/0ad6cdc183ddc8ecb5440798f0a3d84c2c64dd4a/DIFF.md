## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/curr/full-calendar.tex | false | *true* |
| work/alg/curr/MTH1011-syllabus.tex | false | *true* |
| work/alg/curr/MTH1012-syllabus.tex | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| research/talks/siam_pd15/talk.pdf | true | 
| work/alg/mth1701-studentcontract.pdf | true | 
| work/alg/lab-test-policies.pdf | true | 
| work/alg/mathterms.pdf | true | 
| work/alg/abridged-calendar-tpl.pdf | true | 
| work/alg/full-calendar-tpl-7day.pdf | true | 
| work/alg/mth0111-studentcontract.pdf | true | 
| work/alg/peer-tutoring.pdf | true | 
| work/alg/mth0111-test-policies.pdf | true | 
| work/alg/mth1701-test-policies.pdf | true | 
| work/alg/full-calendar-tpl.pdf | true | 
| work/alg/advancement.pdf | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| work/alg/peer-tutoring.tex | true |
| work/alg/mth1701-studentcontract.tex | true |
| work/alg/abridged-calendar-tpl.tex | true |
| work/alg/lab-test-policies.tex | true |
| research/talks/siam_pd15/talk | false |
| work/alg/full-calendar-tpl.tex | true |
| work/alg/full-calendar-tpl-7day.tex | true |
| work/alg/mth1701-test-policies.tex | true |
| work/alg/mth0111-test-policies.tex | true |
| work/alg/mathterms.tex | false |
| work/siam-chapter/minutes-<date-short>-attendance.tex | false |
| work/alg/mth0111-studentcontract.tex | true |
| work/siam-chapter/minutes-<date-short>-actionitems.tex | false |
| work/alg/advancement.tex | true |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




