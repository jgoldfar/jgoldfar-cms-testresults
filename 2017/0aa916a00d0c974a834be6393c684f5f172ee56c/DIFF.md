## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/MCode-Constrained/report1.tex | false | 
| misc/not_mine/mth6330-final-fa17.tex | true | 



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/MCode-Constrained | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| misc/not_mine | true | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| research/optcont/MCode-Constrained/LICENSE | true | *false* |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




