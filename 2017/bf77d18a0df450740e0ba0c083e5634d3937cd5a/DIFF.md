## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| research/optcont/code-docs/fwd-pde.pdf | true | *false* |
| research/optcont/code-docs/model-probs.pdf | true | *false* |
| work/Calc3/quizzes/Quiz3withSolution.tex | true | *false* |
| work/APDE/hw/hw4.pdf | true | *false* |
| research/ndce/codes/emdiben/alg_desc.tex | true | *false* |
| research/talks/jmm_15a/talk.tex | true | *false* |
| work/training/maxima/main.tex | true | *false* |
| research/misc/fa14-npde-course/notes.tex | true | *false* |
| work/APDE/hw/hw13.pdf | true | *false* |
| research/optcont/code-docs/fwd-ode.pdf | true | *false* |
| work/siam-chapter/ayr13/genmeeting.tex | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| notes/independent/papers/emdiben | false | *true* |
| notes/independent/papers/oleinik_60 | false | *true* |
| notes/independent/book/holton | false | *true* |
| notes/independent/book/goldman | false | *true* |
| notes/independent/papers | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




