## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/APDE/advanced | false | *true* |
| research/seminar/minilec | false | *true* |
| notes/independent/papers/solonnikov64 | false | *true* |
| research/seminar/13.9.20_seminar | false | *true* |
| research/talks/siam_pd13 | false | *true* |
| work/APDE/hw | false | *true* |
| research/seminar/14.09.09_seminar | false | *true* |
| work/GrpProj/Calc3/4 | false | *true* |
| grad/seminar/dsct | false | *true* |
| work/APDE/hw/exercises | false | *true* |
| research/seminar/13.9.20_seminar/figures/tex | false | *true* |
| work/training/latex | false | *true* |
| misc/maths | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




