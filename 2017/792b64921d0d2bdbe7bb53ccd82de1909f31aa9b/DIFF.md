## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/alg/curr/1012-lec/all.tex | false | *true* |
| work/APDE/hw/hw4.tex | false | *true* |
| research/talks/jmm_15a/talk.tex | false | *true* |
| work/siam-chapter/summer_school/flyer-template.tex | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| /Users/jgoldfar/Documents/research/misc/proof-secs/determinant-boundary-straightening-map.tex | false | 
| /Users/jgoldfar/Documents/research/misc/proof-secs/heateq-point-source-solution.tex | false | 
| /Users/jgoldfar/Documents/research/misc/proof-secs/symmetric-functions-intident.tex | false | 
| /Users/jgoldfar/Documents/research/misc/proof-secs/tpl.tex | false | 
| /Users/jgoldfar/Documents/research/misc/proof-secs/signum-function-derivative.tex | false | 
| /Users/jgoldfar/Documents/research/misc/proof-secs/cauchy-seq-bounded.tex | false | 



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




