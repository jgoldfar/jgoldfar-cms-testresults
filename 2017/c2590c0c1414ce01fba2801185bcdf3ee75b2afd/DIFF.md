## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| research/optcont/ODE/ode-opt-extended.tex | false | *true* |
| research/optcont/ODE/ode-opt.tex | false | *true* |
| research/optcont/ODE/ode-opt-full.tex | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




