## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| misc/julia/SPSBase/doc/design.tex | true | 
| research/fda-pde/understanding-machine-learning.tex | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| misc/julia/SPS/doc/design.tex | true |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| misc/julia/SPSBase/doc | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| misc/julia/SPS/doc | true |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### New Tests
| File/Test | New |
|-----------------|
| misc/julia/SPSBase/README | true | 
| misc/julia/SPSInterface/LICENSE | true | 
| misc/julia/SPSRunner/README | true | 
| misc/julia/SPSBase/LICENSE | true | 
| misc/julia/SPSRunner/LICENSE | true | 
| misc/julia/SPSInterface/README | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| misc/julia/SPS/LICENSE | true |
| misc/julia/SPS/README | true |



## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




