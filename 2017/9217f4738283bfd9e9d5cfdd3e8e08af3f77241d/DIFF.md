## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/Calc3/exam3/Exam3Solutions.tex | false | *true* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




