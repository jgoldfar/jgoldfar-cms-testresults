## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/optcont/papers/gen-constrained/presentation.tex | false |
| research/optcont/papers/gen-constrained/REG-Update.tex | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




