## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/MISP-diff/main.pdf | false | *true* |
| work/APDE/hw/hw13.pdf | false | *true* |

### New Tests
| File/Test | New |
|-----------------|
| research/optcont/papers/MISP-diff/orig.pdf | true | 
| research/misc/fa14-npde-course/notes-dbl-deg.pdf | true | 
| research/misc/fa14-npde-course/notes.pdf | true | 
| research/optcont/papers/MISP-diff/main-disc.pdf | true | 

### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| research/misc/fa14-npde-course/notes-dbl-deg.tex | true |
| research/misc/fa14-npde-course/notes.tex | false |



## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| research/optcont/papers/diff-paper | false | *true* |
| research/optcont/MCode-Constrained | false | *true* |

### No New Tests




## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| research/misc/fa14-npde-course/p-lapl-shapeode.m | false | *true* |

### No New Tests




