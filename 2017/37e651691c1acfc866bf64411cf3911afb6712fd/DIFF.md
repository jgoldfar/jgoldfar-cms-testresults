## PDF Files Compile

| File/Test | Old | New |
|-----------------------|
| work/Calc3/exam3/Exam3Solutions.tex | true | *false* |
| work/siam-chapter/summer_school/flyer-template.tex | true | *false* |

### No New Tests




## Run Check Commands

| File/Test | Old | New |
|-----------------------|
| work/Calc1/syl | false | *true* |

### No New Tests


### Lost/Removed Tests in New Commit
| File/Test | Old |
|-----------------|
| misc/julia/CMSTest | true |



## Subrepos BasicFiles

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




## OSS Equiv Scripts

| File/Test | Old | New |
|-----------------------|
| *No changed test results.* |  |  |

### No New Tests




