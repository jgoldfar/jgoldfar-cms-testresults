## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| grad/dissertation/other/prev/sp13/yy.mm.dd_meeting_tpl.tex |
| work/Calc1/notes/3.5/main.tex |
| grad/dissertation/main.pdf |
| research/optcont/misc-notes/minimization.tex |
| grad/dissertation/showrefs.pdf |
| work/Calc1/notes/main.tex |
| grad/dissertation/res-num/collected-indep.tex |
| grad/dissertation/other/prev/sp14/yy.mm.dd_meeting_tpl.tex |



### New Tests
File/Test | Result
----------|----
work/alg/curr/0111-lec/review-ch2.pdf | true
work/alg/curr/0111-lec/review.pdf | true
work/alg/curr/1701-lec/week8.pdf | false
notes/lec-notes-tpl.pdf | true
grad/complex/hw4.pdf | false
work/alg/curr/1701-lec/week3.pdf | false
work/alg/curr/1701-lec/week7.pdf | false
notes/numerical.pdf | true
work/alg/curr/0111-lec/week15.pdf | true
work/alg/curr/1701-lec/review-ch8.pdf | true
work/alg/curr/0111-lec/week6.pdf | false
work/alg/curr/1701-lec/week1.pdf | true
work/alg/curr/1701-lec/week9.pdf | true
work/alg/curr/0111-lec/week7.pdf | true
notes/complex/complex.pdf | false
grad/complex/hw2.pdf | false
work/alg/curr/1701-lec/review-ch3.pdf | true
grad/complex/hw5.pdf | false
work/alg/curr/1012-lec/flashcards.pdf | true
work/alg/curr/1701-lec/review-ch5.pdf | true
notes/inv-prob-sp12.pdf | true
work/alg/curr/1701-lec/all.pdf | false
work/alg/curr/1012-lec/limits-intro-2.pdf | true
work/alg/curr/0111-lec/review-ch5.pdf | true
work/alg/curr/0111-lec/week13.pdf | true
work/alg/curr/0111-lec/week12.pdf | true
notes/appstat.pdf | true
notes/mathmeth.pdf | true
notes/models-sp09.pdf | true
notes/discmath.pdf | true
work/alg/curr/1701-lec/week15.pdf | true
work/alg/curr/1012-lec/halfangle-ec.pdf | true
work/alg/curr/0111-lec/week9.pdf | true
grad/complex/hw6.pdf | true
work/alg/curr/0111-lec/week5.pdf | false
work/alg/curr/1012-lec/limits-intro-2-ex.pdf | true
work/alg/curr/0111-lec/week2.pdf | true
work/alg/curr/0111-lec/week3.pdf | true
work/alg/curr/1701-lec/week13.pdf | false
grad/complex/hw9.pdf | true
work/alg/curr/0111-lec/week8.pdf | true
notes/stochastic.pdf | true
notes/dynmet.pdf | true
work/alg/curr/0111-lec/all.pdf | false
work/alg/curr/1701-lec/week2.pdf | true
work/alg/curr/0111-lec/week10.pdf | true
notes/mathstat.pdf | false
work/alg/curr/1701-lec/review-ch1-0111-also.pdf | false
work/alg/curr/1701-lec/review-ch4.pdf | true
work/alg/curr/1012-lec/all.pdf | false
work/alg/curr/0111-lec/review-ch6.pdf | true
notes/functional.pdf | true
work/alg/curr/0111-lec/review-ch4.pdf | true
work/alg/curr/1701-lec/week14.pdf | true
grad/complex/test1.pdf | false
notes/algebra.pdf | true
work/alg/curr/1701-lec/week10.pdf | false
grad/complex/test2.pdf | false
work/alg/curr/1701-lec/review-ch9.pdf | true
notes/introa.pdf | true
work/alg/curr/1701-lec/review-ch6.pdf | true
work/alg/curr/0111-lec/week4.pdf | false
work/alg/curr/0111-lec/week11.pdf | false
work/alg/curr/1701-lec/review-ch7.pdf | true
work/alg/curr/1701-lec/week4.pdf | false
work/alg/curr/1701-lec/week5.pdf | false
grad/complex/hw7.pdf | false
work/alg/curr/1701-lec/review-ch1.pdf | true
grad/complex/hw8.pdf | true
work/alg/curr/1701-lec/weektpl.pdf | false
notes/bvp.pdf | true
work/alg/curr/0111-lec/review-ch7.pdf | true
work/alg/curr/0111-lec/week14.pdf | true
work/alg/curr/0111-lec/week16.pdf | false
work/alg/curr/0111-lec/review-ch1-1701-also.pdf | true
work/alg/curr/1012-lec/limits-intro-1.pdf | false
grad/complex/hw1.pdf | false
work/alg/curr/1012-lec/notecard.pdf | true
work/alg/curr/1701-lec/week11.pdf | false
work/alg/curr/1701-lec/week16.pdf | true
work/alg/curr/1701-lec/week12.pdf | false
work/alg/curr/1011-lec/all.pdf | false
notes/odestheory.pdf | true
grad/complex/hw3.pdf | false
notes/realvariables.pdf | true
notes/finished/apdes.pdf | false
work/alg/curr/1701-lec/review.pdf | true
work/alg/curr/0111-lec/week1.pdf | true
work/alg/curr/1012-lec/limits-intro-1-ex.pdf | false
work/alg/curr/1701-lec/week6.pdf | false
work/alg/curr/0111-lec/review-ch3.pdf | true
work/alg/curr/0111-lec/review-ch1.pdf | true
notes/sobolevspaces.pdf | true
notes/linear.pdf | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
notes/models-sp09.tex | true
notes/finished/apdes.tex | true
work/alg/curr/1701-lec/week3.tex | true
work/alg/curr/0111-lec/review-ch2.tex | true
work/alg/curr/1701-lec/all.tex | true
notes/mathstat.tex | false
work/alg/curr/0111-lec/week12.tex | true
notes/complex/sec6.tex | false
work/alg/curr/0111-lec/week11.tex | true
work/alg/curr/1701-lec/week12.tex | true
grad/complex/hw5.tex | true
work/alg/curr/0111-lec/week8.tex | true
work/alg/curr/0111-lec/week6.tex | true
notes/appstat.tex | true
grad/complex/hw6.tex | true
notes/algebra.tex | true
work/alg/curr/0111-lec/review-ch6.tex | true
notes/inv-prob-sp12.tex | true
work/alg/curr/1012-lec/limits-intro-1-ex.tex | true
notes/sobolevspaces.tex | true
work/alg/curr/1701-lec/review-ch1-0111-also.tex | true
work/alg/curr/1012-lec/limits-intro-1.tex | true
work/alg/curr/1701-lec/week1.tex | true
notes/dynmet.tex | true
work/alg/curr/1701-lec/week15.tex | true
work/alg/curr/1012-lec/notecard.tex | true
notes/numerical.tex | true
work/alg/curr/1701-lec/review-ch1.tex | true
notes/linear.tex | true
work/alg/curr/0111-lec/week4.tex | true
notes/mathmeth.tex | true
work/alg/curr/1701-lec/review-ch9.tex | true
work/alg/curr/0111-lec/week13.tex | true
work/alg/curr/1701-lec/week9.tex | true
notes/discmath.tex | true
work/alg/curr/0111-lec/all.tex | true
work/alg/curr/0111-lec/review-ch3.tex | true
grad/complex/hw9.tex | true
grad/complex/hw7.tex | true
work/alg/curr/1701-lec/week13.tex | true
work/alg/curr/0111-lec/week7.tex | true
work/alg/curr/1701-lec/review.tex | true
notes/complex/sec7.tex | false
work/alg/curr/0111-lec/week5.tex | true
notes/functional.tex | true
work/alg/curr/1701-lec/week16.tex | true
notes/complex/sec1.tex | false
work/alg/curr/1701-lec/review-ch5.tex | true
work/alg/curr/0111-lec/week14.tex | true
notes/lec-notes-tpl.tex | true
work/alg/curr/0111-lec/week10.tex | true
grad/dissertation/proposal/inc/preamble.tex | false
work/alg/curr/1012-lec/all.tex | true
work/alg/curr/1011-lec/all.tex | true
work/alg/curr/1701-lec/review-ch7.tex | true
notes/odestheory.tex | true
work/alg/curr/1012-lec/limits-intro-2-ex.tex | true
work/alg/curr/0111-lec/review.tex | true
notes/bvp.tex | true
work/alg/curr/1701-lec/week5.tex | true
notes/introa.tex | true
work/alg/curr/0111-lec/review-ch1-1701-also.tex | true
grad/complex/test1.tex | true
grad/complex/test2.tex | true
notes/complex/sec8.tex | false
work/alg/curr/1701-lec/review-ch3.tex | true
notes/complex/complex.tex | false
work/alg/curr/1701-lec/review-ch6.tex | true
work/alg/curr/0111-lec/review-ch4.tex | true
work/alg/curr/0111-lec/week2.tex | true
work/alg/curr/0111-lec/week1.tex | true
grad/complex/hw8.tex | true
work/alg/curr/1012-lec/limits-intro-2.tex | true
work/alg/curr/1701-lec/week7.tex | true
work/alg/curr/0111-lec/week3.tex | true
notes/complex/sec5.tex | false
work/alg/curr/0111-lec/week9.tex | true
grad/complex/hw1.tex | true
work/alg/curr/1701-lec/week14.tex | true
notes/stochastic.tex | true
notes/realvariables.tex | true
work/alg/curr/1701-lec/weektpl.tex | true
work/alg/curr/0111-lec/review-ch1.tex | true
work/alg/curr/1701-lec/review-ch4.tex | true
work/alg/curr/1701-lec/week6.tex | true
work/alg/curr/0111-lec/review-ch5.tex | true
work/alg/curr/1701-lec/review-ch8.tex | true
work/alg/curr/1012-lec/flashcards.tex | true
work/alg/curr/1701-lec/week8.tex | true
grad/complex/hw2.tex | true
notes/complex/sec3.tex | false
work/alg/curr/0111-lec/week16.tex | true
work/alg/curr/1701-lec/week4.tex | true
work/alg/curr/0111-lec/week15.tex | true
notes/complex/sec2.tex | false
notes/complex/sec4.tex | false
grad/complex/hw4.tex | true
work/alg/curr/1701-lec/week10.tex | true
work/alg/curr/1012-lec/halfangle-ec.tex | true
work/alg/curr/0111-lec/review-ch7.tex | true
work/alg/curr/1701-lec/week2.tex | true
work/alg/curr/1701-lec/week11.tex | true
grad/complex/hw3.tex | true



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| research/talks/jmm_15 |
| work/Calc1/notes |
| notes/independent/book/holton |
| research/ndce/codes/gen-python |



### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/ISP-Ali | true



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



