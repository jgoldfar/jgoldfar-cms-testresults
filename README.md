Plot generation succeeded.
# CMS Test Results

This repository exists to organize integration test results run across the
[CMS repo](https://bitbucket.org/jgoldfar/jgoldfar-cms).

The test infrastructure currently runs only on a Linux platform, and is
organized using [CMSTest.jl](https://bitbucket.org/jgoldfar/cmstest.jl), a
purpose-built integration testing tool.

## Current Test Summary [(Details)](https://bitbucket.org/jgoldfar/jgoldfar-cms-testresults/src/Linux/29c395746cf7b7edbb86d3b0a01ea467bbbcafd0/)

Tests for commit id `29c395746cf7b7edbb86d3b0a01ea467bbbcafd0` from 2019-04-24T20:38:54.
This report run with Julia v0.7.0

![Test Trend Plot](trendPlot.svg)

1989.646595 seconds (1.17 M allocations: 76.081 MiB, 0.00% gc time)

Category | Pass | Fail | Error | Skip
---------|------|------|-------|-----
PDF Files Compile | 1088 | 9 | 0 | 223
Run Check Commands | 351 | 1 | 0 | 41
Subrepos BasicFiles | 68 | 1 | 0 | 1
OSS Equiv Scripts | 78 | 168 | 0 | 151
Total | 1585 | 179 | 0 | 416
