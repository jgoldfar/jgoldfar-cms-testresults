## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| research/misc/fa14-npde-course/notes-dbl-deg.pdf |
| work/alg/curr/MTH0111-calendar.tex |
| work/projects/reu-admin/notes/dsct/DSCT-Lecture4-print.tex |
| work/acalc/extra/ec_tpl.tex |
| work/acalc/extra/expectation_and_variance.tex |
| work/alg/mathterms.pdf |
| work/projects/conf-gen/reg-form/reg-form.tex |
| work/acalc/extra/selected_questions.tex |
| work/alg/curr/MTH1012-syllabus.tex |
| work/acalc/extra/integrable_func.tex |
| work/projects/reu-admin/notes/dsct/DSCT-Lecture4-print-loc.tex |
| work/SCUDEMPractice/problems.tex |
| work/acalc/extra/analysis_of_func.tex |
| grad/mth6300/abdulla_typed/YMC-2012/YMC-2012-Lecture.tex |
| work/acalc/extra/integral_properties.tex |
| work/acalc/extra/measure_thy_and_lebesgue.tex |
| work/acalc/extra/more_ode.tex |
| work/Calc1/assess/test1Alt.tex |
| work/projects/reu-admin/notes/dsct/DSCT-Lecture4.tex |
| work/acalc/extra/set_theory.tex |
| work/projects/conf-gen/certificates/certificates.tex |
| work/acalc/extra/series.tex |
| work/Calc1/glossary.pdf |
| work/acalc/extra/tricky_integrals.tex |
| work/committeeService/cEarl/compNotes.tex |
| work/acalc/extra/limit_calc.tex |
| work/projects/reu-admin/notes/isp/report/main-presentation.tex |
| work/alg/curr/MTH1701-syllabus.tex |
| work/projects/reu-admin/notes/isp/report/current-slides.tex |
| grad/dissertation/other/prev/sp14/14.02.19_meeting.tex |
| work/projects/reu-admin/notes/isp/report/first-energy-est-conv.tex |
| work/alg/curr/MTH1011-syllabus.tex |



### No New Tests


### No Lost/Removed Tests



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



