## PDF Files Compile: Regressions

|File/Test|
|----------|
| work/Calc1/notes/3.5/main.tex |
| work/Calc1/notes/main.tex |


## PDF Files Compile: Fixes

|File/Test|
|----------|
| research/seminar/minilec/lebesgue-diff-thm_seminar.tex |
| work/projects/ugr/sp13/bpoggi/dual-spaces.tex |
| work/projects/blog-drafts/generalized-jensens.tex |
| work/siam-chapter/trobbin.tex |
| research/optcont/code-docs/alt-scheme/PDE-CYL-sep.tex |
| research/optcont/papers/conv-a-paper/main.pdf |
| research/talks/siam_pd17/talk/main.tex |
| work/projects/blog-drafts/estimating-transportation-schedules.tex |
| research/seminar/14.01.27_seminar.tex |
| grad/seminar/isp/control-dist-param-1.tex |
| research/ndce/codes/cnps/alg_desc.tex |
| work/Calc1/labs_extended/lab22.tex |
| work/projects/reu-admin/notes/isp/report/first-energy-est.tex |
| research/ndce/ndce_paper/thm1-derivation.tex |
| grad/apdes/su11/hw1.pdf |
| work/Calc1/Sp10/complab3.tex |
| notes/independent/book/rudin-principles_of_math_analysis.tex |
| work/projects/blog-drafts/random-function-variates.tex |
| misc/not_mine/ia_tenali/rudin-c-not-orderable.tex |
| work/Calc1/labs_extended/lab20.tex |
| research/quant-bio/tpl.tex |
| work/siam-chapter/summer_school/2013/yrnotes/main.tex |
| misc/resume/res.pdf |
| work/DiffEq/proj/main.tex |
| grad/algebra/extra/ex1.tex |
| work/siam-chapter/ayr13/genmeeting.tex |
| research/quant-bio/schrodinger-no-diffusion/main.tex |
| work/siam-chapter/ayr13/reading-group.tex |
| research/seminar/ISP_convergence_talk/main.tex |
| grad/sobolevspaces/hw-revised/final-orig.tex |
| research/fda-pde/understanding-machine-learning.tex |
| work/projects/ugr/sp13/bpoggi/extremal_problems.tex |
| misc/not_mine/ia_tenali/ia-fa12-mt1.tex |
| notes/independent/papers/abdulla_king_00.tex |
| grad/seminar/isp/control-dist-param-5.tex |
| work/APDE/hw/hw13.pdf |
| work/projects/conf-gen/seas-announce.tex |
| work/APDE/codes/lab1-outline.tex |
| work/projects/reu-admin/notes/npde/report/fdiff.tex |
| work/Calc1/labs_extended/lab31.tex |
| research/seminar/bvp_of_math_phys/main.tex |
| work/siam-chapter/ayr13/reading-group/sir-10.10.13.tex |
| work/projects/blog-drafts/on-the-structure-of-superstable-logistic-orbits.tex |
| work/NumericalMethodsBook/main.tex |
| grad/dissertation/talk/main.tex |
| grad/sobolevspaces/exercises.tex |
| research/seminar/minilec/polynomial-dens.tex |
| grad/mth6300/func-approx.pdf |
| grad/pdes-sp13/final.tex |
| work/projects/reu-admin/notes/isp/lae-b0c0.tex |
| grad/sobolevspaces/hw1.tex |
| research/seminar/minilec/lax-milgram-thm_seminar.tex |
| work/APDE/codes/lab3-outline.tex |
| work/alg/lab-test-policies.pdf |
| ugrad/physoc/cruise1.tex |
| misc/travel/jmm-2017/sessions.tex |
| misc/env/tex-include/Templates/beamer.tex |
| work/MAC/OfficeHours.tex |
| research/seminar/FA_inf_calc/main.tex |
| work/Calc1/labs_extended/lab12.tex |
| work/tutoring/zachary/zachary_100112.tex |
| grad/stochastic/stochastic_probset3.tex |
| work/Calc1/labs_extended/lab15.tex |
| work/siam-chapter/summer_school/2013/yrnotes/intro-indep.tex |
| research/optcont/papers/diff-paper/main |
| grad/mth6230/test1.tex |
| work/Calc1/labs_extended/lab24.tex |
| work/Calc1/labs_extended/lab17.tex |
| work/MAC/MTH2001-Practice.tex |
| work/Calc1/labs_extended/lab16.tex |
| work/projects/blog-drafts/compactness-of-the-trace-map.tex |
| research/talks/nsf_grfp/proposal_draft3.tex |
| misc/maths/fact-est.tex |
| notes/independent/papers/oleinik_60/oleinik_60-problem.tex |
| notes/independent/leibniz_rule.tex |
| research/numpde/numbvp/BVP.jl/doc/impl-1.tex |
| work/Calc1/labs_extended/lab05.tex |
| notes/independent/book/friedman-pde-parabolic.tex |
| notes/independent/book/stefan-self-similar.tex |
| work/Calc1/labs_extended/tpl.tex |
| notes/independent/papers/oleinik_60/oleinik_60-kirchoff-cont.tex |
| research/ndce/codes/alg_desc_tpl.tex |
| work/Calc1/labs_extended/lab27.tex |
| grad/odestheory/nl-improper.tex |
| work/Calc1/labs_extended/lab04.tex |
| notes/independent/book/natanson-real_variables.tex |
| work/acalc/extra/non-standard.tex |
| grad/numpde/hw1.tex |
| grad/mth6230/exercises.tex |
| misc/env/tex-include/Templates/flashcards.tex |
| research/talks/jmm_17/main.tex |
| research/ndce/codes/weno-v2/doc/impl.tex |
| notes/independent/book/goldman/main.tex |
| work/MAC/great-mathematicians.tex |
| grad/apdes/sp13/hw1.tex |
| grad/stochastic/stochastic_test2.tex |
| research/ndce/reu-paper/current-slowdiff.tex |
| research/ndce/reu-paper/2015-main.tex |
| work/projects/blog-drafts/published/brief-ift.tex |
| work/Calc1/labs_extended/lab06.tex |
| notes/independent/book/holton/main.tex |
| work/Calc1/labs_extended/lab23.tex |
| notes/independent/papers/oleinik_60/oleinik_60.tex |
| work/Calc1/labs_extended/lab30.tex |
| grad/numpde/hw2.tex |
| grad/dissertation/other/prev/sp13/13.02.28_meeting.tex |
| work/Calc1/Sp10/complab1.tex |
| work/APDE/codes/lab2-outline.tex |
| work/Calc1/labs_extended/lab25.tex |
| grad/dissertation/plans.pdf |
| work/projects/reu-admin/notes/isp/report/g1-current.tex |
| grad/pdes/test1.tex |
| work/Calc1/labs_extended/lab02.tex |
| research/seminar/minilec/cauchys-integral-thm_seminar.tex |
| grad/stochastic/stochastic_probset.tex |
| work/projects/blog-drafts/neural-network-universal-approximation.tex |
| work/projects/blog-drafts/smooth-truncation-operators.tex |
| grad/introa/final-sp13.tex |
| grad/appstat/hw7.tex |
| grad/seminar/isp/control-dist-param-3.tex |
| notes/independent/zorich-cont-func.tex |
| work/Calc1/labs_extended/lab03.tex |
| grad/stochastic/stochastic_test1.tex |
| research/optcont/papers/nd-sphere-paper/main.tex |
| grad/stochastic/stochastic_probset1.tex |
| research/chaos/Feigenbaum-Frechet.tex |
| work/tutoring/zachary/zachary_200414.tex |
| grad/odestheory/midterm1-q2.tex |
| work/Calc1/labs_extended/lab29.tex |
| work/APDE/hw/hw4.pdf |
| misc/maths/geom-sum.tex |
| notes/independent/papers/solonnikov64/b233half.tex |
| work/siam-chapter/summer_school/2013/yrnotes/outro-indep.tex |
| research/seminar/13.9.20_seminar/thm1-derivation.tex |
| notes/independent/book/johnson-FEM.tex |
| research/optcont/papers/MISP-diff/main.pdf |
| work/siam-chapter/ayr13/reading-group/reading-group-future.tex |
| research/optcont/code-docs/alt-scheme/neumann-rect-explicit-scheme.tex |
| work/alg/review/exp-log/sol-exp-1.tex |
| research/seminar/seminar_tpl.tex |
| work/projects/blog-drafts/weyls-lemma.tex |
| research/talks/jmm_19/main.tex |
| misc/resume/covlet.pdf |
| grad/mathstat/hw/pset1.tex |
| misc/resume/cv.pdf |
| work/alg/curr/announcements.tex |
| research/chaos/feigenbaum/jl/doc/logmap-polyroot.tex |
| grad/odestheory/finaltest-fa12.tex |
| work/projects/ugr/fa12/problem-statement.tex |
| research/optcont/code-docs/alt-scheme/alt-timediscrete-sep.tex |
| work/siam-chapter/ayr13/reading-group/sir-10.24.13.tex |
| work/Calc1/labs_extended/lab26.tex |
| work/misc/tex-tutorial.tex |
| work/siam-chapter/summer_school/notes/all.tex |
| research/ndce/reu-paper/fdiff.tex |
| work/projects/reu-admin/notes/npde/report/current-slowdiff.tex |
| misc/maths/mollif.tex |
| research/seminar/SP19-Opt/main.tex |
| work/Calc1/labs_extended/lab01.tex |
| work/Calc1/labs_extended/lab10.tex |
| work/matlab-workshop/13.8.31-combined/Outline.tex |
| notes/independent/misc.tex |
| notes/independent/papers/goldman_03.tex |
| work/Calc1/labs_extended/lab09.tex |
| grad/seminar/isp/control-dist-param-1-ua.tex |
| misc/maths/complete-graph-num-nodes.tex |
| misc/julia/Traffic/doc.tex |
| work/projects/blog-drafts/bounded-laplacian-implies-all-second-derivatives.tex |
| research/misc/fa14-npde-course/notes.pdf |
| research/quant-bio/genetic-switch-demo/notes.tex |
| work/siam-chapter/officer-duties.tex |
| work/Calc1/labs_extended/lab13.tex |
| research/optcont/JCode/ISP/doc/figures/PrintDiag.tex |
| work/projects/placement/tips-and-tricks.tex |
| research/optcont/code-docs/alt-scheme/neumann-rect-implicit-scheme.tex |
| research/paramid/tpl.tex |
| work/Calc1/labs_extended/lab28.tex |
| work/siam-chapter/summer_school/notes/intro-indep.tex |
| work/projects/reu-admin/notes/tex/template-full.tex |
| grad/odestheory/midterm1-fa12.tex |
| work/siam-chapter/ayr12/minutes-9.6.12.tex |
| grad/pdes/test2.tex |
| work/acalc/recap.tex |
| grad/sobolevspaces/hw-revised/final.tex |
| work/projects/ugr/fa18/jgluck-init.tex |
| grad/appstat/hw8.tex |
| research/optcont/code-docs/alt-scheme/dirichlet-scheme.tex |
| misc/maths/math-ed.tex |
| research/talks/research_summary/main.tex |
| work/siam-chapter/minutes-template.tex |
| research/seminar/minilec/anisotropic_sobolev.tex |
| grad/dissertation/proposal/main.tex |
| misc/env/tex-include/Templates/amsart-triv.tex |
| research/optcont/code-docs/alt-scheme/neumann-onestep-scheme.tex |
| misc/not_mine/ia_tenali/ia-fa12-hw.tex |
| research/seminar/11.10.28_seminar.tex |
| research/seminar/14.02.03_seminar.tex |
| grad/seminar/isp/control-dist-param-2-ua.tex |
| work/Calc1/labs_extended/lab18.tex |
| misc/maths/law-maths.tex |
| research/optcont/papers/diff-c-paper/main.tex |
| research/numpde/numbvp/BVP.jl/doc/impl-2.tex |
| grad/seminar/isp/control-dist-param-4.tex |
| grad/dissertation/other/abdulla-paper-mainresults.tex |
| notes/independent/book/evans-sobolev-notes.tex |
| grad/seminar/isp/control-dist-param-2.tex |
| grad/appstat/hw9.tex |
| notes/independent/book/lsu/main.tex |
| research/talks/searcde16/main.tex |
| work/Calc1/labs_extended/lab07.tex |
| notes/independent/papers/emdiben/notes.tex |
| work/Calc1/labs_extended/lab14.tex |
| grad/dissertation/other/comp/comp-questions.tex |
| ugrad/misc/land_prop.tex |
| work/alg/review/newton-for-roots.tex |
| grad/pdes/final.tex |
| work/projects/typing/jrivera-duhamel/main.tex |
| work/Calc1/labs_extended/lab11.tex |
| work/projects/blog-drafts/tpl.tex |
| research/optcont/MISPCode/MISPTools/example/TikhonovSamarskiiResults.pdf |
| misc/env/tex-include/Templates/amsart-triv-noinc.tex |
| grad/dissertation/other/prev/sp13/13.03.07_meeting.tex |
| work/projects/reu-admin/notes/isp/report/g1-6.13.2014.tex |
| work/siam-chapter/ayr12/minutes-10.19.12.tex |
| work/APDE/hw/hw15.pdf |
| work/Calc1/labs_extended/lab21.tex |
| work/APDE/current/extra-projects.tex |
| research/numpde/doubly-degenerate.tex |
| work/projects/useR/useR.tex |
| work/acalc/extra/convex_facts.tex |
| research/optcont/MCode-Constrained/src/+model_fns/definedProblems.pdf |
| work/Calc1/labs_extended/lab08.tex |
| work/projects/reu-admin/notes/isp/report/g2-current.tex |



### New Tests
File/Test | Result
----------|----
grad/dissertation/proposal/inc/preamble.tex | false
research/optcont/ISP-Ali/src/notes.tex | false

### No Lost/Removed Tests



## Run Check Commands: Regressions

|File/Test|
|----------|
| work/Calc1/notes |
| work/siam-chapter |
| work/siam-chapter/ayr12 |


## Run Check Commands: Fixes

|File/Test|
|----------|
| research/optcont/papers/diff-c-paper |
| misc/env/tex-include |
| research/optcont/papers/MISP-diff |
| research/optcont/papers/nd-sphere-paper |
| research/optcont/papers/conv-a-paper |
| misc/env/tex-include/Templates |



### No New Tests


### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/optcont/ISP-Ali/src/precond.m | false
research/optcont/ISP-Ali/src/est_deriv.m | false
research/optcont/ISP-Ali/src/est_t_partial.m | false
research/optcont/ISP-Ali/src/test_all.m | false
research/optcont/ISP-Ali/src/est_x_partial.m | false
research/optcont/ISP-Ali/src/test_optimization.m | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/optcont/ISP-Ali/src/test.m | false



