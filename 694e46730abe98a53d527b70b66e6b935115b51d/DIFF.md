## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| work/alg/curr/1701-lec/week3.tex |
| work/alg/curr/0111-lec/week12.tex |
| work/alg/curr/0111-lec/week11.tex |
| work/alg/curr/1701-lec/week12.tex |
| work/alg/curr/1012-lec/limits-intro-1-ex.tex |
| work/alg/curr/1701-lec/week15.tex |
| work/alg/curr/1012-lec/notecard.tex |
| work/alg/curr/1701-lec/review-ch9.tex |
| work/alg/curr/1701-lec/week9.tex |
| work/alg/curr/0111-lec/all.tex |
| work/alg/curr/1701-lec/week13.tex |
| work/alg/curr/1701-lec/week16.tex |
| work/alg/curr/1701-lec/review-ch5.tex |
| work/alg/curr/1012-lec/limits-intro-2-ex.tex |
| work/alg/curr/1701-lec/week5.tex |
| work/alg/curr/1701-lec/review-ch3.tex |
| work/alg/curr/0111-lec/week1.tex |
| work/alg/curr/1701-lec/week7.tex |
| work/alg/curr/0111-lec/week3.tex |
| work/alg/curr/0111-lec/review-ch1.tex |
| work/alg/curr/1701-lec/week6.tex |
| work/alg/curr/1701-lec/review-ch8.tex |
| work/alg/curr/0111-lec/week16.tex |
| work/alg/curr/1701-lec/week4.tex |
| work/alg/curr/0111-lec/week15.tex |
| work/alg/curr/1701-lec/week10.tex |
| work/alg/curr/0111-lec/review-ch7.tex |
| work/alg/curr/1701-lec/week2.tex |
| work/alg/curr/0111-lec/review-ch2.tex |
| work/alg/curr/1701-lec/all.tex |
| work/alg/curr/0111-lec/week8.tex |
| work/alg/curr/0111-lec/week6.tex |
| work/alg/curr/0111-lec/review-ch6.tex |
| work/alg/curr/1701-lec/review-ch1-0111-also.tex |
| work/alg/curr/1012-lec/limits-intro-1.tex |
| work/alg/curr/1701-lec/week1.tex |
| work/alg/curr/1701-lec/review-ch1.tex |
| work/alg/curr/0111-lec/week4.tex |
| work/alg/curr/0111-lec/week13.tex |
| work/alg/curr/0111-lec/review-ch3.tex |
| work/alg/curr/0111-lec/week7.tex |
| work/alg/curr/1701-lec/review.tex |
| work/alg/curr/0111-lec/week5.tex |
| work/alg/curr/0111-lec/week14.tex |
| work/alg/curr/0111-lec/week10.tex |
| work/alg/curr/1012-lec/all.tex |
| work/alg/curr/1011-lec/all.tex |
| work/alg/curr/1701-lec/review-ch7.tex |
| work/alg/curr/0111-lec/review.tex |
| work/alg/curr/0111-lec/review-ch1-1701-also.tex |
| work/alg/curr/1701-lec/review-ch6.tex |
| work/alg/curr/0111-lec/review-ch4.tex |
| work/alg/curr/0111-lec/week2.tex |
| work/alg/curr/1012-lec/limits-intro-2.tex |
| work/alg/curr/0111-lec/week9.tex |
| work/alg/curr/1701-lec/week14.tex |
| work/alg/curr/1701-lec/weektpl.tex |
| work/alg/curr/1701-lec/review-ch4.tex |
| work/alg/curr/0111-lec/review-ch5.tex |
| work/alg/curr/1012-lec/flashcards.tex |
| work/alg/curr/1701-lec/week8.tex |
| work/alg/curr/1012-lec/halfangle-ec.tex |
| work/alg/curr/1701-lec/week11.tex |



### New Tests
File/Test | Result
----------|----
work/alg/review/dirMacros.tex | false

### No Lost/Removed Tests



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
work/alg/common | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/common | true



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
work/alg/common/README | true
work/alg/common/LICENSE | true

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/common/README | true
work/common/LICENSE | true



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/optcont/ISP-Ali/src/test_Adjoint.m | false

### No Lost/Removed Tests



