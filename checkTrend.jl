const TRT = include("TestResultTools.jl")

@static if VERSION >= v"0.7-"
    using Test
else
    using Base.Test
end

@testset "Check Trend" begin
    baseDir = isempty(ARGS) ? @__DIR__() : popfirst!(ARGS)
    (idLatest, summaryLatest), (idPrevious, summaryPrevious) = TRT.testTrendData(0, baseDir)

    @info "Testing $(idLatest) vs $(idPrevious)"

    passes1, fails1, errors1, skips1 = summaryLatest
    passes2, fails2, errors2, skips2 = summaryPrevious

    # Check that number of passing tests is nondecreasing
    @testset "More tests passing, category $k" for (k, v) in passes1
        if !haskey(passes2, k)
          @warn "Pass summary for $(idPrevious) malformed."
          continue
        end
        @test v >= passes2[k]
    end

    # Check that number of failing tests is nonincreasing
    @testset "Fewer tests failing, category $k" for (k, v) in fails1
        if !haskey(fails2, k)
          @warn "Fails sumary for $(idPrevious) malformed."
          continue
        end
        @test v <= fails2[k]
    end

    # Neither testset should have failures
    @testset "No test errors, category $k" for (k, v) in errors1
        if !haskey(errors2, k)
          @warn "Error summary for $(idPrevious) malformed."
          continue
        end
        @test v == 0 && errors2[k] == 0
    end
end # testset
