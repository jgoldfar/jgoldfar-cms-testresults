## PDF Files Compile: Regressions

|File/Test|
|----------|
| *None* |

## PDF Files Compile: Fixes

|File/Test|
|----------|
| work/projects/abdulla/CalcVarPDEOptimalStefanProblem.tex |
| research/optcont/papers/MISP-general/NonlinearParabolicPaper.pdf |



### New Tests
File/Test | Result
----------|----
research/quant-bio/schrodinger-no-diffusion/constant-b-field.tex | true
research/talks/ams_sec_sep17/main.pdf | true
work/misc/teaching-award-nomination-form.tex | true
research/quant-bio/rashad_thesis/main.tex | false

### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
research/quant-bio/schrodinger-no-diffusion/regularization.tex | true
research/talks/ams_sec_sep17/main.tex | false



## Run Check Commands: Regressions

|File/Test|
|----------|
| *None* |

## Run Check Commands: Fixes

|File/Test|
|----------|
| *None* |

### New Tests
File/Test | Result
----------|----
research/quant-bio/rashad-matlab | true

### No Lost/Removed Tests



## Subrepos BasicFiles: Regressions

|File/Test|
|----------|
| *None* |

## Subrepos BasicFiles: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



## OSS Equiv Scripts: Regressions

|File/Test|
|----------|
| *None* |

## OSS Equiv Scripts: Fixes

|File/Test|
|----------|
| *None* |

### No New Tests


### No Lost/Removed Tests



