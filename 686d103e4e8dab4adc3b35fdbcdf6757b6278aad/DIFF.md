## PDF Files Compile

File/Test | Old | New
----------|-----|----
work/Calc1/syl/syllabus.pdf | false | *true*
work/Calc1/syl/calendar.pdf | false | *true*
grad/stochastic/stochastic_probset.tex | false | *true*
misc/juliet/birth-cheatsheet.tex | false | *true*
grad/stochastic/stochastic_test1.tex | false | *true*
grad/stochastic/stochastic_test2.tex | false | *true*
grad/stochastic/stochastic_probset3.tex | false | *true*
grad/stochastic/stochastic_probset1.tex | false | *true*

### No New Tests


### Lost/Removed Tests in New Commit
File/Test | Old
----------|----
work/PutnamPrep/review-1985A3.inc.tex | false
work/PutnamPrepOther/review-2018A1.inc.tex | false
work/PutnamPrepOther/review-2018B5.tex | true
work/PutnamPrepOther/review-2018A3.tex | true
work/PutnamPrepOther/tpl.tex | true
work/PutnamPrep/review-1986A2.tex | true
work/PutnamPrepOther/review-2018A6.inc.tex | false
work/PutnamPrepOther/review-2018B4.tex | true
work/PutnamPrepOther/review-2018A5.tex | true
work/PutnamPrepOther/review-2018A4.tex | true
work/PutnamPrep/review-1985A3.tex | true
work/PutnamPrepOther/review-2018B3.tex | true
work/PutnamPrepOther/review-2018A4.inc.tex | false
work/PutnamPrepOther/review-2018A2.tex | true
work/PutnamPrepOther/review-2018B6.tex | true
work/PutnamPrepOther/review-2018B5.inc.tex | false
work/PutnamPrep/tpl.tex | true
work/PutnamPrepOther/review-2018A3.inc.tex | false
work/PutnamPrepOther/review-2018A5.inc.tex | false
work/PutnamPrepOther/review-2018B4.inc.tex | false
work/PutnamPrep/tpl.inc.tex | false
work/PutnamPrepOther/review-2018B6.inc.tex | false
work/PutnamPrep/review-1986A2.inc.tex | false
work/PutnamPrepOther/review-2018B1.inc.tex | false
work/PutnamPrepOther/review-2018A2.inc.tex | false
work/PutnamPrepOther/review-2018A6.tex | true
work/PutnamPrep/review-1986B1.inc.tex | false
work/PutnamPrepOther/review-2018A1.tex | true
work/PutnamPrepOther/review-2018B3.inc.tex | false
work/PutnamPrepOther/review-2018B2.tex | true
work/PutnamPrepOther/review-2018B2.inc.tex | false
work/PutnamPrepOther/review-2018B1.tex | true
work/PutnamPrepOther/tpl.inc.tex | false
work/PutnamPrep/review-1986B1.tex | true



## Run Check Commands

File/Test | Old | New
----------|-----|----
research/optcont/papers/nd-paper | false | *true*
work/projects/abdulla | true | *false*
research/seminar/day-to-day-FOSS | false | *true*
work/Calc1/syl | false | *true*
grad/dissertation | true | *false*

### No New Tests




## Subrepos BasicFiles

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




## OSS Equiv Scripts

File/Test | Old | New
----------|-----|----
*No changed test results.* |  | 

### No New Tests




